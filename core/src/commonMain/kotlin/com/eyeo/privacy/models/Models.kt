package com.eyeo.privacy.models

import io.ktor.http.*
import kotlinx.serialization.Serializable
import kotlin.js.JsExport


@JsExport
@Serializable
data class PrivacyShieldStatsData(
    val trackersBlocked: Int = 0,
    val cookiesBlocked: Int = 0,
    val popupsBlocked: Int = 0,
    val popupsShown: Int = 0
)

@JsExport
data class PrivacyShieldStats(
    val tabStats: PrivacyShieldStatsData,
    val globalStats: PrivacyShieldStatsData
)

object PrivacyShieldHeaders {
    val AcceptLanguage: String = HttpHeaders.AcceptLanguage
    val ContentType: String = HttpHeaders.ContentType
    val Cookie: String = HttpHeaders.Cookie
    const val DNT: String = "DNT"
    val Referrer: String = HttpHeaders.Referrer
    const val SecGPC: String = "Sec-GPC"
    val SetCookie: String = HttpHeaders.SetCookie
    val UserAgent: String = HttpHeaders.UserAgent
    const val XClientData: String = "X-Client-Data"
    val HttpHeadersToReduceFingerprint = setOf(
        "DPR",
        "Device-Memory",
        "Downlink",
        "ECT",
        "RTT",
        "Sec-CH-Prefers-Color-Scheme",
        "Sec-CH-UA",
        "Sec-CH-UA-Arch",
        "Sec-CH-UA-Bitness",
        "Sec-CH-UA-Full-Version",
        "Sec-CH-UA-Mobile",
        "Sec-CH-UA-Model",
        "Sec-CH-UA-Platform",
        "Sec-CH-UA-Platform-Version",
        "Sec-CH-UA-Full-Version-List",
        "Sec-CH-UA-WoW64"
    )
}
