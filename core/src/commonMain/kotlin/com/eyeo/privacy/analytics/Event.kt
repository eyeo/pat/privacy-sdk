package com.eyeo.privacy.analytics

import kotlin.js.JsExport
@JsExport
data class Event<T> internal constructor(val id: String, val value: T?) {
    fun hasValue() = value == null

    @Suppress("UNCHECKED_CAST")
    fun <T> valueAs(): T = value as T

    object Cmp {
        /**
         * A CMP popup was detected
         */
        const val CMP_SHOWN = "cmp_shown"
    }

    companion object
}

@JsExport
interface EventListener {
    fun onEvent(event: Event<*>)
}