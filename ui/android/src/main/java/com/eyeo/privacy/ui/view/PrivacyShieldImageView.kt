package com.eyeo.privacy.ui.view

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.graphics.drawable.DrawableCompat

class PrivacyShieldImageView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : AppCompatImageView(context, attrs, defStyleAttr), PrivacyShieldThemeHelper.ThemeListener,
    PrivacyShieldUIContext.UIContextAware {

    private val themeHelper = PrivacyShieldThemeHelper(context, false, this)

    override fun setPrivacyShieldUIContext(privacyShieldUIContext: PrivacyShieldUIContext) {
        themeHelper.setPrivacyShieldUIContext(privacyShieldUIContext)
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        themeHelper.onAttachedToWindow()
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        themeHelper.onDetachedFromWindow()
    }

    override fun onColorChange(color: Int) {
        DrawableCompat.setTint(this.drawable, color)
    }

}