package com.eyeo.privacy.ui.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewPropertyAnimator
import android.view.animation.AnticipateInterpolator
import android.view.animation.OvershootInterpolator
import android.widget.FrameLayout
import androidx.annotation.ColorInt
import androidx.core.graphics.drawable.DrawableCompat
import com.eyeo.pat.concurrent.Cancellable
import com.eyeo.privacy.PrivacyShieldProvider
import com.eyeo.privacy.ui.R
import com.eyeo.privacy.ui.databinding.PrivacyShieldViewButtonBinding

class PrivacyShieldButton @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr), PrivacyShieldProvider, PrivacyShieldUIContext.UIContextListener,
    PrivacyShieldUIContext.UIContextAwareImpl {

    private val binding: PrivacyShieldViewButtonBinding
    private var documentUrl: String? = null
    private var currentTabId: String? = null
    private var listeningJob: Cancellable? = null

    init {
        binding = PrivacyShieldViewButtonBinding.inflate(LayoutInflater.from(context), this, true)
        setupIcon(context, attrs)
    }

    private fun setupIcon(context: Context, attrs: AttributeSet?) {
        val psAttrs = context.theme.obtainStyledAttributes(
            attrs, R.styleable.PrivacyShieldButton, 0, 0)
        val iconBackground = psAttrs.getResourceId(
            R.styleable.PrivacyShieldButton_icon_background,
            R.drawable.ps_icon_background
        )
        val iconForeground = psAttrs.getResourceId(
            R.styleable.PrivacyShieldButton_icon_foreground,
            R.drawable.ps_icon_foreground
        )

        binding.psButtonIv.setImageResource(iconBackground)
        binding.psButtonIvFg.setImageResource(iconForeground)
    }

    override var uiContext: PrivacyShieldUIContext = PrivacyShieldUIContext.Default

    override fun getContextAwareChildren(): List<PrivacyShieldUIContext.UIContextAware> = listOf(
        binding.psButtonIvFg,
        binding.psButtonCountCvColor
    )

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        uiContext.registerListener(this)
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        listeningJob?.cancel()
        uiContext.unregisterListener(this)
    }

    override fun onTabChanged(tabId: String, documentUrl: String) {
        this.currentTabId = tabId
        this.documentUrl = documentUrl
        setCount(0)
        listeningJob?.cancel()
        listeningJob = stats?.listen(tabId) {
            setCount(it.tabStats.cookiesBlocked + it.tabStats.trackersBlocked)
        }
    }

    private fun setCount(count: Int) {
        val newScale = if (count == 0) 0f else 1.0f

        val counterContainer = binding.psButtonCountCvColor
        val viewTag = counterContainer.getTag(R.id.ps_button_tv) as Float? ?: 0f
        // we don't start animation if it's already targeting proper value
        if (viewTag != newScale) {
            counterContainer.setTag(R.id.ps_button_tv, newScale)
            val viewPropertyAnimator: ViewPropertyAnimator =
                counterContainer.animate().scaleX(newScale).scaleY(newScale)
                    .setDuration(200)
            if (newScale >= viewTag) {
                viewPropertyAnimator.interpolator = OvershootInterpolator()
            } else {
                viewPropertyAnimator.interpolator = AnticipateInterpolator()
            }
            viewPropertyAnimator.start()
        }

        if (count == 0 && binding.psButtonTv.getTag(R.id.ps_button_tv) == true) {
            counterContainer.visibility = View.VISIBLE
            counterContainer.getTag(R.id.ps_button_tv)
            counterContainer.animate().scaleX(1f).scaleY(1f).start()
        }

        if (count != 0) {
            binding.psButtonTv.text = count.toString()
        }
    }

    override fun setEnabled(enabled: Boolean) {
        super.setEnabled(enabled)
        alpha = if (enabled) 1.0f else 0.2f
    }
}