package com.eyeo.privacy.utils

import android.webkit.WebView

object UIExtension {

    fun WebView.getTabId() = Integer.toHexString(System.identityHashCode(this))

}