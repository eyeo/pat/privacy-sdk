package com.eyeo.privacy.service

import com.eyeo.privacy.service.AcceptLanguageHeader
import com.eyeo.privacy.service.UserAgentHeader
import kotlin.test.Test
import kotlin.test.assertEquals

class HeaderTests {

    @Test
    fun userAgentHeaderTest() {
        val ua =
            "prefix Mozilla/5.0 (Linux; Android 12, Pixel 3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.129 Mobile Safari/537.36 suffix"
        val uaExpected =
            "Mozilla/5.0 (Linux; Android 13) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Mobile Safari/537.36"
        val header = UserAgentHeader(ua)
        assertEquals(ua, header.getValue())
        header.reduce()
        assertEquals(uaExpected, header.getValue())
    }


    @Test
    fun acceptLanguageHeaderTest() {
        val fullHeader = "fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7"
        val smallHeader = "fr-FR"
        val reduceHeader = "fr-FR,fr;q=0.9"
        val header = AcceptLanguageHeader(fullHeader)
        assertEquals(fullHeader, header.getValue())
        header.reduce()
        assertEquals(reduceHeader, header.getValue())


        val header2 = AcceptLanguageHeader(smallHeader)
        header2.reduce()
        assertEquals(smallHeader, header2.getValue())

    }

}