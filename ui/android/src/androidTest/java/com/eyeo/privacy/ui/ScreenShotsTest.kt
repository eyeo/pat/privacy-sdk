package com.eyeo.privacy.ui

import android.content.Context
import android.content.Intent
import android.view.View
import android.widget.FrameLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.lifecycle.Lifecycle
import androidx.test.core.app.ActivityScenario.launch
import androidx.test.espresso.Espresso.onIdle
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import androidx.test.internal.runner.junit4.statement.UiThreadStatement
import androidx.test.platform.app.InstrumentationRegistry
import com.eyeo.pat.test.CaptureView.CAPTURE_SLEEP_DELAY
import com.eyeo.pat.test.CaptureView.SCREENSHOT_FOLDER
import com.eyeo.pat.test.CaptureView.THEME_LABELS
import com.eyeo.pat.test.CaptureView.THEME_MODES
import com.eyeo.pat.test.CaptureView.captureFragment
import com.eyeo.pat.test.CaptureView.captureView
import com.eyeo.pat.test.CaptureView.toDp
import com.eyeo.pat.test.FragmentScenario
import com.eyeo.pat.test.PatScreenCapture
import com.eyeo.privacy.models.PrivacyShieldStats
import com.eyeo.privacy.models.PrivacyShieldStatsData
import io.mockk.clearMocks
import io.mockk.every
import io.mockk.mockk
import com.eyeo.privacy.PrivacyShieldAndroid
import com.eyeo.privacy.PrivacyShieldCore
import com.eyeo.privacy.PrivacyShieldProvider
import com.eyeo.privacy.models.*
import com.eyeo.privacy.ui.tnc.PrivacyShieldTnCDialogFragment
import com.eyeo.privacy.ui.utils.showOnce
import com.eyeo.privacy.ui.view.*
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@MediumTest
@RunWith(AndroidJUnit4::class)
class ScreenShotsTest {

    private val context = InstrumentationRegistry.getInstrumentation().targetContext

    private val themeId = R.style.Theme_PrivacyShield_Settings_Primary
    private val activityContainerId = R.id.ps_activity_container
    private val cardViewStyle = com.google.android.material.R.attr.materialCardViewElevatedStyle
    private val viewWidth = 340.toDp()

    @Before
    fun setup() {
        PrivacyShieldAndroid.release()
        PrivacyShieldAndroid.setup(context)
        PrivacyShieldAndroid.get().privacy().editSettings().enable(true).apply()
    }

    @After
    fun release() {
        PrivacyShieldAndroid.release()
        PrivacyShieldProvider.testInstance = null
    }

    @Test
    fun capturePrivacyFragment() {
        captureFragment<PrivacyShieldSettingsFragment>(
            "settings_fragment",
            themeId,
            activityContainerId
        )
    }

    @Test
    fun captureStatsView() {
        captureView(context, "stats_view") {
            PrivacyShieldStatsView(it).let { statsView ->
                val privacyShieldStats =
                    PrivacyShieldStats(
                        PrivacyShieldStatsData(12, 8, 4),
                        PrivacyShieldStatsData(100000, 100000, 100)
                    )
                //tag::PrivacyShieldStatsView[]
                statsView.setStats(privacyShieldStats)
                //end::PrivacyShieldStatsView[]
                statsView
            }
        }
    }

    @Test
    fun captureBoardView() {
        captureView(context, "board_view") {
            PrivacyShieldPopupView(it).apply {
                PrivacyShieldUIContext.Default.setActiveTabUrl("0", "https://crumbs.org")
            }
        }
    }

    @Test
    fun captureButton() {
        val currentStats =
            PrivacyShieldStats(
                PrivacyShieldStatsData(50, 10, 15),
                PrivacyShieldStatsData(250, 250, 5)
            )


        val mockPrivacyShield = mockk<PrivacyShieldCore>(relaxed = true) {
            every { stats().listen(any(), any()) } answers {
                secondArg<(PrivacyShieldStats) -> Unit>()(currentStats)
                mockk(relaxed = true)
            }
        }
        PrivacyShieldProvider.testInstance = mockPrivacyShield
        val width = FrameLayout.LayoutParams.WRAP_CONTENT
        captureView(context, "button", width = width) {
            PrivacyShieldButton(it).apply {
                setPrivacyShieldUIContext(mockk(relaxed = true) {
                    every { registerListener(any()) } answers {
                        firstArg<PrivacyShieldUIContext.UIContextListener>().onProtectionUpdated(
                            PrivacyShieldUIContext.ProtectionStatus.ENABLED
                        )
                        mockk(relaxed = true)
                    }
                })
                onTabChanged("0", "https://www.google.com")
            }
        }

        clearMocks(mockPrivacyShield)
        captureView(context, "button_paused", width = width) {
            PrivacyShieldButton(it).apply {
                setPrivacyShieldUIContext(mockk(relaxed = true) {
                    every { registerListener(any()) } answers {
                        firstArg<PrivacyShieldUIContext.UIContextListener>().onProtectionUpdated(
                            PrivacyShieldUIContext.ProtectionStatus.PAUSED
                        )
                        mockk(relaxed = true)
                    }
                })
                onTabChanged("0", "https://www.google.com")
            }
        }

        captureView(context, "button_disabled", width = width) {
            PrivacyShieldButton(it).apply {
                setPrivacyShieldUIContext(mockk(relaxed = true) {
                    every { registerListener(any()) } answers {
                        firstArg<PrivacyShieldUIContext.UIContextListener>().onProtectionUpdated(
                            PrivacyShieldUIContext.ProtectionStatus.PAUSED_UNTIL_NEXT_SESSION
                        )
                        mockk(relaxed = true)
                    }
                })
                onTabChanged("0", "https://www.google.com")
            }
        }
    }

    @Test
    fun captureTnCDialogFragment() {
        PrivacyShieldProvider.testInstance = mockk(relaxed = true)

        for (x in THEME_MODES.indices) {
            UiThreadStatement.runOnUiThread { AppCompatDelegate.setDefaultNightMode(THEME_MODES[x]) }
            val intent = Intent(context, FragmentScenario.EmptyFragmentActivity::class.java).apply {
                putExtra(FragmentScenario.EmptyFragmentActivity.THEME_EXTRAS_BUNDLE_KEY, themeId)
                putExtra(
                    FragmentScenario.EmptyFragmentActivity.ACTIVITY_CONTAINER_ID_KEY,
                    activityContainerId
                )
            }
            val scenario = launch<FragmentScenario.EmptyFragmentActivity>(intent)
                .onActivity { activity ->
                    //tag::PrivacyShieldTnCDialogFragment[]
                    PrivacyShieldUI.get().preferences.termsAndConditionsDialog
                    //end::PrivacyShieldTnCDialogFragment[]
                    PrivacyShieldTnCDialogFragment.newInstance()
                        .showOnce(
                            activity.supportFragmentManager,
                            PrivacyShieldTnCDialogFragment.TAG
                        )
                }.moveToState(Lifecycle.State.RESUMED)
            onIdle()
            Thread.sleep(CAPTURE_SLEEP_DELAY)
            scenario.onActivity {
                val fragment =
                    it.supportFragmentManager.findFragmentByTag(PrivacyShieldTnCDialogFragment.TAG) as PrivacyShieldTnCDialogFragment
                PatScreenCapture.captureScreenshot(
                    view = fragment.dialog?.window?.decorView as View,
                    pictureName = "tnc_dialog_fragment_${THEME_LABELS[x]}",
                    childFolder = SCREENSHOT_FOLDER
                )
            }.moveToState(Lifecycle.State.DESTROYED)
        }
    }

    private fun captureView(
        context: Context,
        name: String,
        width: Int = viewWidth,
        builder: (AppCompatActivity) -> View
    ) {
        captureView(
            context = context,
            themeResId = themeId,
            activityId = activityContainerId,
            cardViewStyle = cardViewStyle,
            name = name,
            width = width,
            builder = builder
        )
    }
}