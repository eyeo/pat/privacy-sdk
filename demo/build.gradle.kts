import org.jetbrains.kotlin.incremental.deleteDirectoryContents
import java.io.FileInputStream
import java.util.*

@Suppress("DSL_SCOPE_VIOLATION")
plugins {
    alias(libs.plugins.android.application)
    alias(libs.plugins.kotlin.android)
}

android {
    namespace = "com.eyeo.privacy.webview.demo"
    compileSdk = libs.versions.android.target.sdk.get().toInt()

    defaultConfig {
        applicationId = "com.eyeo.privacy.webview.demo"
        minSdk = libs.versions.android.min.sdk.get().toInt()
        targetSdk = compileSdk
        versionCode = 1
        versionName = project.version.toString()
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    kotlinOptions.jvmTarget = JavaVersion.VERSION_1_8.toString()

    testOptions {
        animationsDisabled = true
    }

    val keystorePropertiesFile = file("keystore.properties")
    if (keystorePropertiesFile.exists()) {
        val keystoreProperties = Properties()
        keystoreProperties.load(FileInputStream(keystorePropertiesFile))

        signingConfigs {
            create("release") {
                keyAlias = keystoreProperties.getProperty("keyAlias")
                keyPassword = keystoreProperties.getProperty("storePassword")
                storeFile = file(keystoreProperties.getProperty("storeFile"))
                storePassword = keystoreProperties.getProperty("storePassword")
            }
        }
    }
    buildTypes {
        val demoSigningConfig =
            signingConfigs.findByName(if (keystorePropertiesFile.exists()) "release" else "debug")
        debug {
            signingConfig = demoSigningConfig
        }
        release {
            isMinifyEnabled = true
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
            signingConfig = demoSigningConfig
        }
    }

    packaging {
        resources.excludes.add("META-INF/LICENSE*")
        resources.excludes.add("META-INF/*.kotlin_module")
        resources.excludes.add("META-INF/AL2.0")
        resources.excludes.add("META-INF/LGPL2.1")
        resources.excludes.add("META-INF/licenses/ASM")
        jniLibs.excludes.add("win32-x86-64/*")
        jniLibs.excludes.add("win32-x86/*")
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
}

dependencies {
    implementation(project(":ui:android"))
    implementation(libs.pat.webview)

    implementation(libs.kotlin.stdlib)
    implementation(libs.androidx.core.ktx)
    implementation(libs.androidx.appcompat)
    implementation(libs.androidx.constraintlayout)
    implementation(libs.androidx.swiperefreshlayout)
    implementation(libs.androidx.cardview)
    implementation(libs.androidx.lifecycle)
    implementation(libs.google.material)
}
