package com.eyeo.privacy.service

import com.eyeo.pat.provider.StorageProvider
import com.russhwolf.settings.Settings
import io.mockk.MockKAnnotations
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import com.eyeo.privacy.models.PrivacyShieldStatsData
import org.junit.Before
import kotlin.test.Test
import kotlin.test.assertEquals

class StatsServiceTests {

    private lateinit var statsService: StatsService


    @MockK
    lateinit var storageSettings: Settings

    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxUnitFun = true)
        statsService = StatsService(StorageProvider(storageSettings))
    }

    @Test
    fun testListen() = runTest {
        val globalStats = mutableListOf<PrivacyShieldStatsData>()
        val tabStats = mutableListOf<PrivacyShieldStatsData>()

        val tabId1 = "1"
        val tabId2 = "2"
        val collectJob = launch(UnconfinedTestDispatcher(testScheduler)) {
            launch {
                statsService.listenStats(tabId1).map { it.globalStats }.toList(globalStats)
            }
            launch {
                statsService.listenStats(tabId1).map { it.tabStats }.toList(tabStats)
            }
        }
        delay(100)
        assertEquals(0, globalStats.last().trackersBlocked)
        assertEquals(0, globalStats.last().cookiesBlocked)
        assertEquals(0, tabStats.last().trackersBlocked)
        assertEquals(0, tabStats.last().cookiesBlocked)

        statsService.onTrackerBlocked(tabId1)
        delay(100)

        assertEquals(1, globalStats.last().trackersBlocked)
        assertEquals(0, globalStats.last().cookiesBlocked)
        assertEquals(1, tabStats.last().trackersBlocked)
        assertEquals(0, tabStats.last().cookiesBlocked)

        statsService.onTrackerBlocked(tabId2)
        statsService.onCookiesBlocked(tabId1)
        statsService.onCookiesBlocked(tabId2)
        delay(100)

        assertEquals(2, globalStats.last().trackersBlocked)
        assertEquals(2, globalStats.last().cookiesBlocked)
        assertEquals(1, tabStats.last().trackersBlocked)
        assertEquals(1, tabStats.last().cookiesBlocked)


        assertEquals(3, globalStats.size)
        assertEquals(3, tabStats.size)

        collectJob.cancel()


    }

}