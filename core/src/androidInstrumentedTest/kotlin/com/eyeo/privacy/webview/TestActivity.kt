package com.eyeo.privacy.webview

import android.app.Activity
import android.os.Bundle
import android.webkit.CookieManager
import android.webkit.WebSettings
import android.webkit.WebView
import com.eyeo.pat.PatLogger
import com.eyeo.pat.PatLoggerLevel
import com.eyeo.pat.webview.PatWebView
import com.eyeo.pat.webview.PatWebViewClient
import org.adblockplus.libadblockplus.android.AndroidBase64Processor
import org.adblockplus.libadblockplus.android.AndroidHttpClient
import org.adblockplus.libadblockplus.security.JavaSignatureVerifier
import org.adblockplus.libadblockplus.security.SignatureVerifier
import org.adblockplus.libadblockplus.sitekey.PublicKeyHolder
import org.adblockplus.libadblockplus.sitekey.PublicKeyHolderImpl
import org.adblockplus.libadblockplus.sitekey.SiteKeyVerifier
import org.adblockplus.libadblockplus.sitekey.SiteKeysConfiguration
import org.adblockplus.libadblockplus.util.Base64Processor

class TestActivity : Activity() {

    lateinit var webView: WebView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        PatLogger.logLevel = PatLoggerLevel.DEBUG

        val useAdblockWebView = intent.getBooleanExtra(ARG_ADBLOCK, true)
        val testUrl = intent.getStringExtra(ARG_URL) ?: "https://crumbs-benchmark.web.app"

        webView = if (useAdblockWebView) {
            PatWebView(this).apply {
                siteKeysConfiguration = this@TestActivity.getSiteKeysConfiguration()
            }

        } else {
            WebView(this)
        }

        WebView.setWebContentsDebuggingEnabled(true)
        CookieManager.getInstance().apply {
            setAcceptCookie(true)
            setAcceptThirdPartyCookies(webView, true)
            removeAllCookies { }
        }
        applicationContext.cacheDir.listFiles()?.forEach {
            it.deleteRecursively()
        }
        val webViewClient = PatWebViewClient()
        webView.webViewClient = webViewClient
        webViewClient.setupWebView(webView, false)
        webView.settings.apply {
            databaseEnabled = true
            domStorageEnabled = true
            mediaPlaybackRequiresUserGesture = true
            cacheMode = WebSettings.LOAD_NO_CACHE
            javaScriptEnabled = true
        }
        webView.keepScreenOn = true
        setContentView(webView)
        webView.loadDataWithBaseURL("", "<HTML><BODY><H3>Test</H3></BODY></HTML>","text/html","utf-8","")
        webView.postDelayed({
            webView.stopLoading()
            webView.loadUrl(testUrl)
        }, 200)

    }

    private fun getSiteKeysConfiguration(): SiteKeysConfiguration {
        val signatureVerifier: SignatureVerifier = JavaSignatureVerifier()
        val publicKeyHolder: PublicKeyHolder = PublicKeyHolderImpl()
        val httpClient = AndroidHttpClient(true)
        val base64Processor: Base64Processor = AndroidBase64Processor()
        val siteKeyVerifier =
            SiteKeyVerifier(signatureVerifier, publicKeyHolder, base64Processor)
        return SiteKeysConfiguration(
            signatureVerifier, publicKeyHolder, httpClient, siteKeyVerifier
        )
    }

    companion object {
        const val ARG_ADBLOCK = "UseAdblock"
        const val ARG_URL = "URL"
    }
}