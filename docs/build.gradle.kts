
tasks {
    val rubyDir = rootProject.file(".bin/ruby")
    val bundleFile = File(rubyDir, "bin/bundle")
    val installTask = register("installDocumentationDependencies") {
        val lockFile = java.io.File(buildDir, ".gemlock")
        inputs.file("Gemfile.lock")
        outputs.file(lockFile)

        doLast {
            val readLines = file("Gemfile.lock").readLines()
            var versionIndex = 0;
            readLines.forEachIndexed { index, s ->
                if (s.contains("BUNDLED WITH", false)) {
                    versionIndex = index + 1
                }
            }
            val version = readLines[versionIndex].trim()
            exec {
                commandLine("gem", "install", "bundler", "-v", version, "--install-dir", rubyDir.path)
            }
            exec {
                commandLine(bundleFile.path, "config", "set", "--local", rubyDir.path)
                commandLine(bundleFile.path, "install")
            }

            lockFile.parentFile.mkdirs()
            lockFile.createNewFile()
        }
    }

    val dokkaTask = project(":core").tasks.named("dokkaHtml")
    register<Exec>("startDocumentationServer") {
        commandLine(bundleFile.path, "exec", "jekyll", "serve")
        dependsOn(installTask, dokkaTask)
    }

    register<Exec>("buildDocumentation") {
        commandLine(bundleFile.path, "exec", "jekyll", "build", "-d", "../public")
        dependsOn(installTask, dokkaTask, project(":ui:android").tasks.named("updateScreenShots"))
    }

    rootProject.tasks.named("prepareBuildTools").get().dependsOn(installTask)

    register<Delete>("clean"){
        delete(".jekyll-cache", "build", "docs", "_site")
    }
}
