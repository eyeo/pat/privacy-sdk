package com.eyeo.privacy.webview

import android.content.Intent
import androidx.lifecycle.Lifecycle
import androidx.test.core.app.ActivityScenario
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import androidx.test.platform.app.InstrumentationRegistry
import com.eyeo.pat.test.PatScreenCapture
import com.eyeo.privacy.PrivacyShieldAndroid
import com.eyeo.privacy.PrivacyShieldProvider
import com.eyeo.privacy.models.FilterLevel
import com.eyeo.privacy.webview.WebTestUtils.retryTest
import org.junit.*
import org.junit.rules.TestName
import org.junit.runner.RunWith
import java.util.concurrent.CompletableFuture
import java.util.concurrent.TimeUnit


@MediumTest
@RunWith(AndroidJUnit4::class)
class CrumbsTestPages : PrivacyShieldProvider {

    @get:Rule
    var name = TestName()

    @Before
    fun init() {
        PrivacyShieldAndroid.release()
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        PrivacyShieldAndroid.setup(context)
    }

    @After
    fun release() {
        PrivacyShieldAndroid.release()
    }

    @Ignore("###EUCookieBar not taken into account")
    @Test
    fun testFilteringCmp() = retryTest {
        requirePrivacy.editSettings()
            .enable(false)
            .hideCookieConsentPopups(true)
            .apply()
        Assert.assertEquals(TestStatus.Failed, runTest(TEST_CMP_URL))
        requirePrivacy.editSettings()
            .enable(true)
            .hideCookieConsentPopups(false)
            .apply()
        Assert.assertEquals(TestStatus.Failed, runTest(TEST_CMP_URL))
        requirePrivacy.editSettings()
            .enable(true)
            .hideCookieConsentPopups(true)
            .apply()
        Thread.sleep(5000)
        Assert.assertEquals(TestStatus.Passed, runTest(TEST_CMP_URL))
    }

    @Test
    fun testCmpEventsListener() = retryTest {
        val initialStats = requireStats.getGlobal().popupsShown
        requirePrivacy.editSettings()
            .enable(false)
            .apply()
        runTest(TEST_CMP_UI_SHOWN_URL)
        Assert.assertEquals(requireStats.getGlobal().popupsShown, initialStats)

        requirePrivacy.editSettings()
            .enable(true)
            .apply()
        Assert.assertEquals(TestStatus.Passed, runTest(TEST_CMP_UI_SHOWN_URL))
        Assert.assertEquals(requireStats.getGlobal().popupsShown, initialStats + 1)
    }

    @Ignore("Allowlist rules don't work on webview ?allow=true is ignored")
    @Test
    fun testFilteringNetworkRequests() = retryTest {
        requirePrivacy.editSettings()
            .enable(false)
            .blockTrackers(true)
            .apply()
        Assert.assertEquals(TestStatus.Failed, runTest(TEST_NETWORK_REQUESTS_URL))
        requirePrivacy.editSettings()
            .enable(true)
            .blockTrackers(false)
            .apply()
        Assert.assertEquals(TestStatus.Passed, runTest(TEST_NETWORK_REQUESTS_URL))
        requirePrivacy.editSettings()
            .enable(true)
            .blockTrackers(true)
            .apply()
        Assert.assertEquals(TestStatus.Passed, runTest(TEST_NETWORK_REQUESTS_URL))
    }

    @Test
    fun testFilteringUTM() = retryTest {
        requirePrivacy.editSettings()
            .enable(false)
            .removeMarketingTrackingParameters(true)
            .apply()
        Assert.assertEquals(TestStatus.Failed, runTest(TEST_UTM_URL))
        requirePrivacy.editSettings()
            .enable(true)
            .removeMarketingTrackingParameters(false)
            .apply()
        Assert.assertEquals(TestStatus.Failed, runTest(TEST_UTM_URL))
        requirePrivacy.editSettings()
            .enable(true)
            .removeMarketingTrackingParameters(true)
            .apply()
        Assert.assertEquals(TestStatus.Passed, runTest(TEST_UTM_URL))
    }

    @Test
    fun testDomReferrer() = retryTest {
        requirePrivacy.editSettings()
            .enable(false)
            .hideReferrerHeader(true)
            .apply()
        Assert.assertEquals(TestStatus.Failed, runTest(TEST_REFERRER_URL))
        requirePrivacy.editSettings()
            .enable(true)
            .hideReferrerHeader(false)
            .apply()
        Assert.assertEquals(TestStatus.Failed, runTest(TEST_REFERRER_URL))
        requirePrivacy.editSettings()
            .enable(true)
            .hideReferrerHeader(true)
            .apply()
        Assert.assertEquals(TestStatus.Passed, runTest(TEST_REFERRER_URL))
    }

    @Test
    fun testDomDNT() = retryTest {
        requirePrivacy.editSettings()
            .enable(false)
            .enableDoNotTrack(true)
            .apply()
        Assert.assertEquals(TestStatus.Failed, runTest(TEST_DNT_URL))
        requirePrivacy.editSettings()
            .enable(true)
            .enableDoNotTrack(false)
            .apply()
        Assert.assertEquals(TestStatus.Failed, runTest(TEST_DNT_URL))
        requirePrivacy.editSettings()
            .enable(true)
            .enableDoNotTrack(true)
            .apply()
        Assert.assertEquals(TestStatus.Passed, runTest(TEST_DNT_URL))
    }

    @Test
    fun testDomGPC() = retryTest {
        requirePrivacy.editSettings()
            .enable(false)
            .enableGPC(true)
            .apply()
        Assert.assertEquals(TestStatus.Failed, runTest(TEST_GPC_URL))
        requirePrivacy.editSettings()
            .enable(true)
            .enableGPC(false)
            .apply()
        Assert.assertEquals(TestStatus.Failed, runTest(TEST_GPC_URL))
        requirePrivacy.editSettings()
            .enable(true)
            .enableGPC(true)
            .apply()
        Assert.assertEquals(TestStatus.Passed, runTest(TEST_GPC_URL))
    }

    @Test
    fun testSetHeaders() = retryTest {
        requirePrivacy.editSettings()
            .enable(false)
            .enableDoNotTrack(true)
            .enableGPC(true)
            .apply()
        Assert.assertEquals(TestStatus.Failed, runTest(TEST_SET_HEADERS_URL))
        requirePrivacy.editSettings()
            .enable(true)
            .enableDoNotTrack(false)
            .enableGPC(false)
            .apply()
        Assert.assertEquals(TestStatus.Failed, runTest(TEST_SET_HEADERS_URL))
        requirePrivacy.editSettings()
            .enable(true)
            .enableDoNotTrack(true)
            .enableGPC(true)
            .apply()
        Assert.assertEquals(TestStatus.Passed, runTest(TEST_SET_HEADERS_URL))
    }

    @Test
    fun testRemoveHeaders() = retryTest {
        requirePrivacy.editSettings()
            .enable(true)
            .enableFingerprintShield(true)
            .apply()
        Assert.assertEquals(TestStatus.Passed, runTest(TEST_REMOVED_HEADERS_URL))
    }

    @Test
    fun testFingerprintingNavigator() = retryTest {
        requirePrivacy.editSettings()
            .enable(false)
            .enableFingerprintShield(true)
            .apply()
        Assert.assertEquals(TestStatus.Failed, runTest(TEST_FINGERPRINTING_NAVIGATOR_URL))
        requirePrivacy.editSettings()
            .enable(true)
            .enableFingerprintShield(false)
            .apply()
        Assert.assertEquals(TestStatus.Failed, runTest(TEST_FINGERPRINTING_NAVIGATOR_URL))
        requirePrivacy.editSettings()
            .enable(true)
            .enableFingerprintShield(true)
            .apply()
        Assert.assertEquals(TestStatus.Passed, runTest(TEST_FINGERPRINTING_NAVIGATOR_URL))
    }

    @Test
    fun testCookieSandboxing() = retryTest {
        requirePrivacy.editSettings()
            .enable(false)
            .cookiesFilterLevel(FilterLevel.SANDBOX)
            .apply()
        Assert.assertEquals(TestStatus.Failed, runTest(TEST_COOKIES_SANDBOXING_URL))
        requirePrivacy.editSettings()
            .enable(true)
            .cookiesFilterLevel(FilterLevel.ALL)
            .apply()
        Assert.assertEquals(TestStatus.Failed, runTest(TEST_COOKIES_SANDBOXING_URL))
        requirePrivacy.editSettings()
            .enable(true)
            .cookiesFilterLevel(FilterLevel.SANDBOX)
            .apply()
        Assert.assertEquals(TestStatus.Passed, runTest(TEST_COOKIES_SANDBOXING_URL))
    }

    @Test
    fun testCookieRemoved() = retryTest {
        requirePrivacy.editSettings()
            .enable(false)
            .cookiesFilterLevel(FilterLevel.ALL)
            .apply()
        Assert.assertEquals(TestStatus.Failed, runTest(TEST_COOKIES_REMOVAL_URL))
        requirePrivacy.editSettings()
            .enable(true)
            .cookiesFilterLevel(FilterLevel.ALL)
            .apply()
        Assert.assertEquals(TestStatus.Passed, runTest(TEST_COOKIES_REMOVAL_URL))
    }

    private fun runTest(
        url: String
    ): TestStatus {
        val intent =
            Intent(InstrumentationRegistry.getInstrumentation().context, TestActivity::class.java)
        intent.putExtra(TestActivity.ARG_URL, url)
        intent.putExtra(TestActivity.ARG_ADBLOCK, true)

        val scenario = ActivityScenario.launch<TestActivity>(intent)
        try {
            scenario.moveToState(Lifecycle.State.RESUMED)
            InstrumentationRegistry.getInstrumentation().waitForIdleSync()

            var retry = 0
            while (retry < 15) {
                Thread.sleep(1000)
                val testResult = getTestResult(scenario)
                if (testResult == TestStatus.InProgress) {
                    retry++
                } else {
                    return testResult
                }
            }
            throw IllegalStateException("Can't get test result: TIMEOUT")
        } catch (e: Throwable) {
            captureScreenshot(name.methodName, scenario)
            throw e
        } finally {
            scenario.moveToState(Lifecycle.State.DESTROYED)
        }
    }


    private fun captureScreenshot(
        methodName: String,
        activityScenario: ActivityScenario<TestActivity>,
    ) {
        activityScenario.onActivity {
            PatScreenCapture.captureScreenshot(
                it,
                methodName,
                childFolder = SCREENSHOTS_FOLDER
            )
        }
        Thread.sleep(3000)
    }

    private fun getTestResult(scenario: ActivityScenario<TestActivity>): TestStatus {
        InstrumentationRegistry.getInstrumentation().waitForIdleSync()
        val future = CompletableFuture<List<String>>()
        scenario.onActivity { activity ->
            activity.webView.evaluateJavascript(
                "Array.from(document.getElementsByClassName(\"$TEST_RESULT_CLASS\")).map(value => value.getAttribute(\"data-result\"))"
            ) { result ->
                if (result.length < 3) {
                    future.complete(listOf())
                } else {
                    val list = result
                        .substring(1, result.length - 1)
                        .split(",")
                        .map { it.substring(1, it.length - 1) }
                    future.complete(list)
                }
            }
        }
        val status = future.get(3000, TimeUnit.MILLISECONDS).map { TestStatus.valueOf(it) }
        if (status.contains(TestStatus.Failed)) return TestStatus.Failed
        if (status.isEmpty() || status.contains(TestStatus.InProgress)) return TestStatus.InProgress
        return TestStatus.Passed
    }

    enum class TestStatus {
        InProgress,
        Passed,
        Failed
    }

    companion object {

        private val SCREENSHOTS_FOLDER =
            "screenshots/${CrumbsTestPages::class.java.name}"

        private const val TEST_RESULT_CLASS = "TestResult"
        private const val TEST_PAGE_URL = "https://test-pages.crumbs.org"
        private const val TEST_CMP_URL = "$TEST_PAGE_URL/filtering/cmp/"
        private const val TEST_CMP_UI_SHOWN_URL = "$TEST_PAGE_URL/filtering/cmpuishown"
        private const val TEST_NETWORK_REQUESTS_URL = "$TEST_PAGE_URL/filtering/network-requests/"
        private const val TEST_UTM_URL = "$TEST_PAGE_URL/filtering/utm?autoStart"
        private const val TEST_REFERRER_URL = "$TEST_PAGE_URL/dom/referrer/?autoStart"
        private const val TEST_DNT_URL = "$TEST_PAGE_URL/dom/dnt/"
        private const val TEST_GPC_URL = "$TEST_PAGE_URL/dom/gpc/"
        private const val TEST_SET_HEADERS_URL = "$TEST_PAGE_URL/headers/set-headers/"
        private const val TEST_REMOVED_HEADERS_URL = "$TEST_PAGE_URL/headers/removed-headers/"
        private const val TEST_FINGERPRINTING_NAVIGATOR_URL =
            "$TEST_PAGE_URL/fingerprinting/navigator/"
        private const val TEST_COOKIES_SANDBOXING_URL = "$TEST_PAGE_URL/cookies/sandboxing?step=1"
        private const val TEST_COOKIES_REMOVAL_URL = "$TEST_PAGE_URL/cookies/cookies-removal"
    }
}