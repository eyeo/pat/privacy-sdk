package com.eyeo.privacy.ui.view

import android.content.Context
import android.util.AttributeSet
import com.eyeo.privacy.ui.R
import com.google.android.material.card.MaterialCardView
import com.google.android.material.R as AndroidR

class PrivacyShieldCardView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null
) : MaterialCardView(context, attrs, AndroidR.attr.materialCardViewElevatedStyle),
    PrivacyShieldThemeHelper.ThemeListener, PrivacyShieldUIContext.UIContextAware {

    private val themeHelper: PrivacyShieldThemeHelper

    init {
        val psAttrs = context.theme.obtainStyledAttributes(
            attrs,
            R.styleable.PrivacyShieldCardView,
            0, 0
        )

        radius = psAttrs.getDimension(
            R.styleable.PrivacyShieldCardView_psRadius,
            context.resources.getDimension(R.dimen.ps_card_radius)
        )
        val lightTheme = psAttrs.getBoolean(R.styleable.PrivacyShieldCardView_psLightTheme, false)
        cardElevation = 0f
        psAttrs.recycle()
        themeHelper = PrivacyShieldThemeHelper(context, lightTheme, this)
    }

    override fun setPrivacyShieldUIContext(privacyShieldUIContext: PrivacyShieldUIContext) {
        themeHelper.setPrivacyShieldUIContext(privacyShieldUIContext)
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        themeHelper.onAttachedToWindow()
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        themeHelper.onDetachedFromWindow()
    }

    override fun onColorChange(color: Int) {
        setCardBackgroundColor(color)
    }

}