package com.eyeo.privacy.ui

import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.app.AppCompatDelegate.MODE_NIGHT_YES
import androidx.preference.Preference
import androidx.preference.SwitchPreferenceCompat
import androidx.test.core.app.ActivityScenario
import androidx.test.core.app.ActivityScenario.launch
import androidx.test.espresso.Espresso.onIdle
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import androidx.test.platform.app.InstrumentationRegistry
import com.eyeo.privacy.PrivacyShieldAndroid
import com.eyeo.privacy.models.FilterLevel
import com.eyeo.privacy.presenter.PrivacyPresenter
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith

@MediumTest
@RunWith(AndroidJUnit4::class)
class SettingsTest {

    @Test
    fun testSettingsActivity() {
        PrivacyShieldAndroid.release()
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        PrivacyShieldAndroid.setup(context)
        AppCompatDelegate.setDefaultNightMode(MODE_NIGHT_YES)

        //tag::settingsActivity[]
        var settingsIntent = PrivacyShieldSettingsActivity.createIntent(context)
        //end::settingsActivity[]

        launch<PrivacyShieldSettingsActivity>(settingsIntent).onActivity { activity ->
            val fragment =
                activity.supportFragmentManager.fragments.find { it is PrivacyShieldSettingsFragment }
            Assert.assertEquals(fragment?.javaClass, PrivacyShieldSettingsFragment::class.java)
        }

        onView(withText(R.string.ps_privacy_title)).check(matches(isDisplayed()))

        //tag::settingsActivityExtra[]
        settingsIntent = PrivacyShieldSettingsActivity.createIntent(
            context,
            highlightedFeature = context.getString(R.string.ps_key_de_amp),
            parentIntent = null, // up navigation
        )
        //end::settingsActivityExtra[]

        launch<PrivacyShieldSettingsActivity>(settingsIntent).onActivity { activity ->
            val fragment =
                activity.supportFragmentManager.fragments.find { it is PrivacyShieldSettingsFragment }
            Assert.assertEquals(fragment?.javaClass, PrivacyShieldSettingsFragment::class.java)
        }
        onIdle()
        onView(withText(R.string.ps_de_amp)).check(matches(isDisplayed()))
        PrivacyShieldAndroid.release()
    }

    @Test
    fun testSettings() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        PrivacyShieldAndroid.setup(context)
        val privacy = PrivacyShieldAndroid.get().privacy()
        privacy.editSettings().resetDefault().enable(true).apply()
        val settingsIntent = PrivacyShieldSettingsActivity.createIntent(context)
        val scenario = launch<PrivacyShieldSettingsActivity>(settingsIntent)
        onView(withText(R.string.ps_show_advanced_features)).perform(click())


        val entries = mapOf(

            R.string.ps_key_block_third_party_cookies to { value: Boolean ->
                //tag::blockThirdPartyCookies[]
                // enabled by default (sandbox)
                privacy.editSettings()
                    .blockThirdPartyCookies(value)
                    .cookiesFilterLevel(FilterLevel.SANDBOX)
                    .apply()
                //end::blockThirdPartyCookies[]
            },
            R.string.ps_key_hide_cookie_popups to { value: Boolean ->
                //tag::hideCookieConsentPopups[]
                // disabled by default (require the user consent in Europe)
                privacy.editSettings().hideCookieConsentPopups(value).apply()
                //end::hideCookieConsentPopups[]
            },
            R.string.ps_key_block_trackers to { value: Boolean ->
                //tag::blockTrackers[]
                // enabled by default
                privacy.editSettings().blockTrackers(value).apply()
                //end::blockTrackers[]
            },
            R.string.ps_key_hide_referrer_header to { value: Boolean ->
                //tag::hideReferrerHeader[]
                // enabled by default
                privacy.editSettings().hideReferrerHeader(value).apply()
                //end::hideReferrerHeader[]
            },
            R.string.ps_key_cname_cloaking_protection to { value: Boolean ->
                //tag::enableCNameCloakingProtection[]
                // enabled by default
                privacy.editSettings().enableCNameCloakingProtection(value).apply()
                //end::enableCNameCloakingProtection[]
            },
            R.string.ps_key_send_no_track_signal to { value: Boolean ->
                //tag::enableDoNotTrack[]
                // enabled by default
                privacy.editSettings().enableDoNotTrack(value).apply()
                //end::enableDoNotTrack[]
            },
            R.string.ps_key_gpc to { value: Boolean ->
                //tag::enableGPC[]
                // enabled by default
                privacy.editSettings().enableGPC(value).apply()
                //end::enableGPC[]
            },
            R.string.ps_key_remove_marketing_tracking_parameters to { value: Boolean ->
                //tag::removeMarketingTrackingParameters[]
                // enabled by default
                privacy.editSettings().removeMarketingTrackingParameters(value).apply()
                //end::removeMarketingTrackingParameters[]
            },
            R.string.ps_key_block_hyperlink_auditing to { value: Boolean ->
                //tag::blockHyperlinkAuditing[]
                // enabled by default
                privacy.editSettings().blockHyperlinkAuditing(value).apply()
                //end::blockHyperlinkAuditing[]
            },
            R.string.ps_key_fingerprint_shield to { value: Boolean ->
                //tag::enableFingerprintShield[]
                // enabled by default
                privacy.editSettings().enableFingerprintShield(value).apply()
                //end::enableFingerprintShield[]
            },
            R.string.ps_key_social_media_blocking to { value: Boolean ->
                //tag::blockSocialMediaIconsTracking[]
                // enabled by default
                privacy.editSettings().blockSocialMediaIconsTracking(value).apply()
                //end::blockSocialMediaIconsTracking[]
            },
            R.string.ps_key_de_amp to { value: Boolean ->
                //tag::enableDeAMP[]
                // disabled by default (may break website)
                privacy.editSettings().enableDeAMP(value).apply()
                //end::enableDeAMP[]
            },
        )

        entries.forEach { (key, setter) ->
            setter(false)
            Thread.sleep(150)
            checkValue(scenario, key, false)
            setter(true)
            Thread.sleep(150)
            checkValue(scenario, key, true)
        }


        PrivacyShieldAndroid.release()

    }

    @Test
    fun testHiddenSettings() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        PrivacyShieldAndroid.setup(context)
        val privacy = PrivacyShieldAndroid.get().privacy()
        privacy.editSettings().resetDefault().enable(true).apply()

        val features = arrayOf(
            R.string.ps_key_block_third_party_cookies,
            R.string.ps_key_hide_cookie_popups,
            R.string.ps_key_block_trackers,
            R.string.ps_key_block_third_party_cookies_dropdown,
            R.string.ps_key_hide_referrer_header,
            R.string.ps_key_cname_cloaking_protection,
            R.string.ps_key_send_no_track_signal,
            R.string.ps_key_gpc,
            R.string.ps_key_remove_marketing_tracking_parameters,
            R.string.ps_key_block_hyperlink_auditing,
            R.string.ps_key_fingerprint_shield,
            R.string.ps_key_social_media_blocking,
            R.string.ps_key_de_amp,
        )

        features.forEach { key ->
            launch<PrivacyShieldSettingsActivity>(
                PrivacyShieldSettingsActivity.createIntent(
                    context, hiddenFeatures = features.map { context.getString(it) }.toTypedArray()
                )
            ).apply {
                onView(withText(R.string.ps_show_advanced_features)).perform(click())
                this.onActivity {
                    val fragment =
                        it.supportFragmentManager.fragments.first() as PrivacyShieldSettingsFragment
                    assert(!fragment.findPreference<Preference>(context.getString(key))!!.isVisible)
                }
            }
        }

        launch<PrivacyShieldSettingsActivity>(
            PrivacyShieldSettingsActivity.createIntent(
                context, hiddenFeatures = features.map { context.getString(it) }.toTypedArray()
            )
        ).apply {
            onView(withText(R.string.ps_show_advanced_features)).perform(click())
            this.onActivity {
                val fragment =
                    it.supportFragmentManager.fragments.first() as PrivacyShieldSettingsFragment
                features.forEach { key ->
                    assert(!fragment.findPreference<Preference>(context.getString(key))!!.isVisible)
                }
            }
        }


        launch<PrivacyShieldSettingsActivity>(
            PrivacyShieldSettingsActivity.createIntent(
                context,
            )
        ).apply {
            onView(withText(R.string.ps_show_advanced_features)).perform(click())
            this.onActivity {
                val fragment =
                    it.supportFragmentManager.fragments.first() as PrivacyShieldSettingsFragment
                features.forEach { key ->
                    assert(fragment.findPreference<Preference>(context.getString(key))!!.isVisible)
                }
            }
        }

    }


    private fun checkValue(
        scenario: ActivityScenario<PrivacyShieldSettingsActivity>,
        key: Int,
        value: Boolean
    ) {
        scenario.onActivity { activity ->
            val fragment =
                activity.supportFragmentManager.fragments.find { it is PrivacyShieldSettingsFragment } as PrivacyShieldSettingsFragment
            val pref =
                fragment.findPreference<SwitchPreferenceCompat>(activity.getString(key))!!
            assert(pref.isChecked == value)
        }
    }


    data class SettingEntry(
        val text: Int,
        val tag: Int,
        val settingsCallback: (PrivacyPresenter.Editor, Boolean) -> PrivacyPresenter.Editor
    )


}