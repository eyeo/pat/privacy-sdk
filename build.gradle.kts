import com.eyeo.pat.PatExtension
import com.eyeo.pat.PatPlugin

@Suppress("DSL_SCOPE_VIOLATION")
plugins {
    alias(libs.plugins.kotlin.multiplatform) apply false
    alias(libs.plugins.kotlin.serialization) apply false
    alias(libs.plugins.kotlin.android) apply false
    alias(libs.plugins.android.library) apply false
    alias(libs.plugins.android.application) apply false
    alias(libs.plugins.download) apply false
    alias(libs.plugins.dokka) apply false
    alias(libs.plugins.npm.publish) apply false
    alias(libs.plugins.pat.plugin)
}

pat {
    ci {
        setup = true
    }
}

val privacyVersion = libs.versions.privacy.library.full.get()

allprojects {
    group = "com.eyeo.privacy"
    version = privacyVersion

    plugins.withType(PatPlugin::class) {
        extensions.getByType(PatExtension::class).publishing {
            gitlabProjectId = "48027330"
            repositoryUrl = "https://gitlab.com/eyeo/pat/privacy-shield-sdk"
        }
    }
}