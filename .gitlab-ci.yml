variables:
  FF_USE_FASTZIP: "true"
  CACHE_COMPRESSION_LEVEL: "fastest"

  BUILD_CACHE_PREFIX: "build-cache"
  TOOLS_CACHE_PREFIX: "tools-cache"
  CACHE_LOCK_PREFIX: "cache-lock"
  CACHE_DIR: $CI_BUILDS_DIR/../cache/$CI_PROJECT_PATH
stages:
  - dependencies
  - build
  - test
  - deploy
  - cleanup

default:
  image: registry.gitlab.com/eyeo/docker/crumbs-android-runner:android-x
  tags:
    - "pat-sdk"

workflow:
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - if: $CI_MERGE_REQUEST_IID
      when: never
    - when: always

.build-cache: &build-cache
  key: $BUILD_CACHE_PREFIX
  paths:
    - .gradle/
  policy: pull

.tools-cache: &tools-cache
  key: $TOOLS_CACHE_PREFIX
  paths:
    - .android/
    - .bin/
  policy: pull

.base-stage:
  interruptible: true
  before_script:
    - ./gradlew prepareCiFiles
    - source .ci/configure.sh
  cache:
    - <<: *build-cache
    - <<: *tools-cache

dependencies:
  stage: dependencies
  extends: .base-stage
  script:
    - |
      [[ -f .cache_exists ]] && .ci/prepare_cache.sh && echo "cache already exists" && exit 10
    - .ci/prepare_tools.sh
    - .ci/prepare_cache.sh
    - echo > .cache_exists
  allow_failure:
    exit_codes: 10
  cache:
    - <<: *build-cache
      policy: pull-push
      when: 'always'
    - <<: *tools-cache
      policy: pull-push
      when: 'on_success'
    - key: $CACHE_LOCK_PREFIX-$TOOLS_CACHE_PREFIX
      paths:
        - .cache_exists
      policy: pull-push

buildAndroid:
  stage: build
  extends: .base-stage
  script:
    - ./gradlew assemble assembleAndroidTest

buildJS:
  stage: build
  extends: .base-stage
  script:
    - ./gradlew jsBrowserProductionLibraryDistribution

allTests:
  stage: test
  extends: .base-stage
  script:
    - ./gradlew lint allTests
  artifacts:
    name: "tests_reports_${CI_PROJECT_NAME}_${CI_BUILD_REF_NAME}"
    reports:
      junit:
        - core/build/test-results/**/TEST-*.xml
    paths:
      - core/build/reports/
    expire_in: 1 week

androidConnectedTest:
  stage: test
  extends: .base-stage
  script:
    - .ci/start-emulator.sh -v
    - ./gradlew connectedAndroidTest
    - .ci/stop-emulator.sh
  artifacts:
    reports:
      junit: core/build/outputs/androidTest-results/connected/TEST-*.xml
    expire_in: 1 week

publishAndroid:
  stage: deploy
  extends: .base-stage
  script:
    - ./gradlew publishAllPublicationsToMavenRepository
  only:
    - main
    - develop
    - /^feat/shared/.*$/

publishJs:
  stage: deploy
  extends: .base-stage
  script:
    - ./gradlew publishJsPackageToGitlabRegistry
  only:
    - main
    - develop
    - /^feat/shared/.*$/

pages:
  stage: deploy
  extends: .base-stage
  script:
    - .ci/start-emulator.sh -v
    - ./gradlew buildDocumentation
    - .ci/stop-emulator.sh
  artifacts:
    paths:
      - public
    expire_in: 1 week
  rules:
    - if: $CI_COMMIT_BRANCH == "main"
      when: on_success
    - if: $PUBLISH_DOC
      when: on_success

cleanup:
  stage: cleanup
  extends: .base-stage
  script:
    - .ci/cleanup.sh $BUILD_CACHE_PREFIX $CACHE_DIR
    - .ci/cleanup.sh $TOOLS_CACHE_PREFIX $CACHE_DIR
    - .ci/cleanup.sh $CACHE_LOCK_PREFIX $CACHE_DIR
