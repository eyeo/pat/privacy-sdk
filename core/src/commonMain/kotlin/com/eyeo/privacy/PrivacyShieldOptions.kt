package com.eyeo.privacy

import kotlin.js.JsExport
import kotlin.jvm.JvmOverloads

@JsExport
data class PrivacyShieldOptions @JvmOverloads constructor(
    /**
     * Enable CHIPS support to sandbox cookies.
     * If false, a custom cookie sandboxing implementation will be used.
     * @see [link](https://developer.chrome.com/en/docs/privacy-sandbox/chips/]
     */
    var supportChips: Boolean = false,

    /**
     * Enable Storage Partitioning support.
     * If false, a custom localStorage sandboxing implementation will be used.
     * @see [link](https://developer.chrome.com/en/docs/privacy-sandbox/storage-partitioning/]
     */
    var supportStoragePartitioning: Boolean = false,

    /**
     * use this option to override the default fingerprint shield script embedded in the sdk
     */
    var fingerprintShieldScript: String? = null,
)