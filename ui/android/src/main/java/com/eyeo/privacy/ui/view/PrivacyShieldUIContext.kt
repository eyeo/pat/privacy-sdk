package com.eyeo.privacy.ui.view

import android.content.Context
import androidx.core.content.ContextCompat
import com.eyeo.pat.concurrent.Cancellable
import com.eyeo.pat.utils.UrlExtension.getUrlHost
import com.eyeo.privacy.PrivacyShieldAndroid
import com.eyeo.privacy.PrivacyShieldProvider
import com.eyeo.privacy.ui.R

class PrivacyShieldUIContext : PrivacyShieldProvider {

    private var listenJob: Cancellable? = null
    private var currentUrl: String = ""
    private var currentTabId: String = ""
    private var listeners = arrayListOf<UIContextListener>()
    private var currentStatus = ProtectionStatus.ENABLED

    init {
        updateStatus()
    }

    fun getProtectionStatus(): ProtectionStatus {
        return currentStatus
    }

    fun getCurrentUrl(): String {
        return currentUrl
    }

    fun setActiveTabUrl(tabId: String, url: String) {
        if (currentUrl != url || tabId != currentTabId) {
            for (i in listeners.size - 1 downTo 0) {
                listeners[i].onTabChanged(tabId, url)
            }
        }
        this.currentUrl = url
        this.currentTabId = tabId
        updateStatus()
    }

    fun registerListener(listener: UIContextListener) {
        listeners.add(listener)
        listener.onTabChanged(currentTabId, currentUrl)
        listener.onProtectionUpdated(currentStatus)
        updateListening()
    }

    fun unregisterListener(listener: UIContextListener): Boolean {
        val removed = listeners.remove(listener)
        if (removed) {
            updateListening()
        }
        return removed
    }

    private fun updateListening() {
        if (!PrivacyShieldAndroid.isInitialized()) return
        if (listeners.isEmpty()) {
            listenJob?.cancel()
            listenJob = null
        } else if (listenJob == null) {
            listenJob = requirePrivacy.listenSettings {
                updateStatus()
            }
        }
    }

    private fun updateStatus() {
        if (!PrivacyShieldAndroid.isInitialized()) return
        val lastStatus = currentStatus
        val isTemporaryAllowed =
            currentUrl.isNotEmpty() && requirePrivacy.getSettings().allowedDomainsThisSession.contains(
                currentUrl.getUrlHost()
            )
        val protectionEnabledForUrl = requirePrivacy.isProtectionEnabledForUrl(currentUrl)

        this.currentStatus = when {
            isTemporaryAllowed -> ProtectionStatus.PAUSED_UNTIL_NEXT_SESSION
            !protectionEnabledForUrl -> ProtectionStatus.PAUSED
            else -> ProtectionStatus.ENABLED
        }
        if (lastStatus != currentStatus) {
            for (i in listeners.size - 1 downTo 0) {
                listeners[i].onProtectionUpdated(currentStatus)
            }
        }
    }

    enum class ProtectionStatus {
        ENABLED,
        PAUSED,
        PAUSED_UNTIL_NEXT_SESSION;

        fun getColor(context: Context, lightTheme: Boolean): Int {

            val colorId = when (this) {
                ENABLED -> if (lightTheme) R.color.ps_green_light else R.color.ps_green
                PAUSED -> if (lightTheme) R.color.ps_orange_light else R.color.ps_orange
                PAUSED_UNTIL_NEXT_SESSION -> if (lightTheme) R.color.ps_yellow_light else R.color.ps_yellow
            }
            return ContextCompat.getColor(context, colorId)

        }

        fun getLabel(context: Context): String {
            return when (this) {
                ENABLED -> context.getString(R.string.ps_tracking_is_enabled)
                PAUSED -> context.getString(R.string.ps_tracking_is_disabled)
                PAUSED_UNTIL_NEXT_SESSION -> context.getString(R.string.ps_tracking_is_paused)
            }
        }
    }

    companion object {
        @JvmStatic
        val Default by lazy { PrivacyShieldUIContext() }
    }

    interface UIContextListener {
        fun onTabChanged(tabId: String, documentUrl: String) {}
        fun onProtectionUpdated(status: ProtectionStatus) {}
    }

    interface UIContextAware {
        fun setPrivacyShieldUIContext(privacyShieldUIContext: PrivacyShieldUIContext)
    }

    internal interface UIContextAwareImpl : UIContextAware {

        var uiContext: PrivacyShieldUIContext

        override fun setPrivacyShieldUIContext(privacyShieldUIContext: PrivacyShieldUIContext) {
            if (this is UIContextListener) {
                if (this.uiContext.unregisterListener(this)) {
                    privacyShieldUIContext.registerListener(this)
                }
            }
            this.uiContext = privacyShieldUIContext
            getContextAwareChildren().forEach {
                it.setPrivacyShieldUIContext(privacyShieldUIContext)
            }
        }

        fun getContextAwareChildren(): List<UIContextAware> = emptyList()
    }
}