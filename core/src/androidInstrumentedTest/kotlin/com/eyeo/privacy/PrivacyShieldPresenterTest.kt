package com.eyeo.privacy

import android.util.Log
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.eyeo.pat.provider.AndroidResourceProvider
import com.russhwolf.settings.SharedPreferencesSettings
import io.ktor.client.*
import io.ktor.client.engine.mock.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.setMain
import com.eyeo.privacy.service.AndroidDnsClient
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

@Suppress("unused")
@RunWith(AndroidJUnit4::class)
class PrivacyShieldPresenterTest {
    private val coroutineDispatcher = UnconfinedTestDispatcher()

    @Before
    fun init() {
        Dispatchers.setMain(coroutineDispatcher)
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        PrivacyShieldCore.setTestInstance(
            PrivacyShieldCore(
                SharedPreferencesSettings.Factory(context).create("privacy_shield_test"),
                AndroidResourceProvider(context),
                AndroidDnsClient(context),
                PrivacyShieldOptions()
            )
        )
    }

    @After
    fun release() {
        PrivacyShieldAndroid.get().privacy().editSettings()
            .resetDefault()
            .apply()
        PrivacyShieldAndroid.release()
    }

    @Test
    @Suppress("UNUSED_VARIABLE", "UNUSED_ANONYMOUS_PARAMETER") // for documentation purposes
    fun testPrivacy() {
        //tag::privacy[]
        val privacy = PrivacyShieldAndroid.get().privacy()
        //end::privacy[]
        assertNotNull(privacy)

        //tag::getPrivacySettings[]
        val privacySettings = privacy.getSettings()
        //end::getPrivacySettings[]
        assertNotNull(privacySettings)

        //tag::listenPrivacySettings[]
        val job = privacy.listenSettings { settings ->
            // update UI with privacySettings
        }
        // ...
        // call cancel when you want to unsubscribe
        job.cancel()
        //end::listenPrivacySettings[]

        privacy.editSettings()
            .resetDefault()
            .apply()

        val value = !privacy.getSettings().blockThirdPartyCookies
        //tag::editPrivacySettings[]
        privacy.editSettings()
            .blockThirdPartyCookies(value)
            .apply()
        //end::editPrivacySettings[]
        assertEquals(value, privacy.getSettings().blockThirdPartyCookies)

        val url = "https://www.google.com"

        //tag::checkPrivacyEnabled[]
        // privacy functionality global status
        val isPrivacyEnabled = privacy.getSettings().enabled

        // check privacy global status, domain allowedDomains and allowedDomainsThisSession settings
        val isPageProtected = privacy.isProtectionEnabledForUrl(url)
        //end::checkPrivacyEnabled[]
    }

    @Test
    fun testStats() {
        //tag::stats[]
        val statsPresenter = PrivacyShieldAndroid.get().stats()
        //end::stats[]
        assertNotNull(statsPresenter)

        val tabId = "0"
        //tag::statsGet[]
        val privacyShieldStats = statsPresenter.get(tabId = tabId)
        Log.d(
            "Stats",
            "${privacyShieldStats.globalStats.trackersBlocked} trackers blocked since the beginning"
        )
        Log.d(
            "Stats",
            "${privacyShieldStats.globalStats.popupsBlocked} popups blocked since the beginning"
        )
        //end::statsGet[]

        //tag::statsListen[]
        val job = statsPresenter.listen(tabId = tabId) { stats ->
            Log.d(
                "Stats",
                "${stats.tabStats.cookiesBlocked} cookies blocked so far on this page"
            )
        }
        //...
        // stop listening
        job.cancel()
        //end::statsListen[]

    }
}