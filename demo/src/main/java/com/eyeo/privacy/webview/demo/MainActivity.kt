package com.eyeo.privacy.webview.demo

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.webkit.*
import android.widget.EditText
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.eyeo.pat.webview.PatWebView
import com.eyeo.pat.webview.PatWebViewClient
import com.eyeo.privacy.PrivacyShieldProvider
import com.eyeo.privacy.analytics.Event
import com.eyeo.privacy.analytics.EventListener
import com.eyeo.privacy.models.TermsAndConditionsState
import com.eyeo.privacy.ui.PrivacyShieldSettingsActivity
import com.eyeo.privacy.ui.PrivacyShieldUI
import com.eyeo.privacy.ui.tnc.PrivacyShieldTnCDialogFragment
import com.eyeo.privacy.ui.utils.showOnce
import com.eyeo.privacy.ui.view.PrivacyShieldButton
import com.eyeo.privacy.ui.view.PrivacyShieldPopup
import com.eyeo.privacy.ui.view.PrivacyShieldUIContext
import com.google.android.material.snackbar.Snackbar
import org.adblockplus.libadblockplus.android.AndroidBase64Processor
import org.adblockplus.libadblockplus.android.AndroidHttpClient
import org.adblockplus.libadblockplus.android.webview.AdblockWebView
import org.adblockplus.libadblockplus.security.JavaSignatureVerifier
import org.adblockplus.libadblockplus.security.SignatureVerifier
import org.adblockplus.libadblockplus.sitekey.PublicKeyHolder
import org.adblockplus.libadblockplus.sitekey.PublicKeyHolderImpl
import org.adblockplus.libadblockplus.sitekey.SiteKeyVerifier
import org.adblockplus.libadblockplus.sitekey.SiteKeysConfiguration
import org.adblockplus.libadblockplus.util.Base64Processor
import timber.log.Timber


class MainActivity : AppCompatActivity(), AdblockWebView.EventsListener, EventListener,
    PrivacyShieldProvider {

    lateinit var navEt: EditText
    lateinit var webView: PatWebView
    lateinit var swipeRefreshLayout: SwipeRefreshLayout
    lateinit var progressBar: ProgressBar

    @Suppress("USELESS_CAST")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar?.hide()
        WebView.setWebContentsDebuggingEnabled(BuildConfig.DEBUG)
        webView = findViewById(R.id.main_webview)

        progressBar = findViewById(R.id.main_progressBar)
        swipeRefreshLayout = findViewById(R.id.main_swiperefresh)
        swipeRefreshLayout.setOnRefreshListener { webView.reload() }
        navEt = findViewById(R.id.main_url)
        navEt.setOnEditorActionListener { v, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_GO || actionId == EditorInfo.IME_ACTION_UNSPECIFIED) {
                var text = v.text.toString()
                if (!text.startsWith("http://") && !text.startsWith("https://")) {
                    text = "https://www.google.com/search?q=$text"
                }
                webView.loadUrl(text)
                v.clearFocus()
                val imm: InputMethodManager =
                    getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(v.windowToken, 0)
                true
            } else {
                false
            }
        }

        //tag::defaultContext[]
        val privacyShieldUIContext = PrivacyShieldUIContext.Default
        //end::defaultContext[]
        //tag::setCustomContext[]
        val privacyShieldBtn: PrivacyShieldButton = findViewById(R.id.main_ps_btn)
        privacyShieldBtn.setPrivacyShieldUIContext(privacyShieldUIContext)
        privacyShieldBtn.setOnClickListener {
            PrivacyShieldPopup(this@MainActivity).show(
                privacyShieldBtn,
                privacyShieldUIContext
            )
        }

        //end::setCustomContext[]
        webView.settings.apply {
            databaseEnabled = true
            domStorageEnabled = true
            mediaPlaybackRequiresUserGesture = true
            cacheMode = WebSettings.LOAD_NO_CACHE
        }
        // allow iframe to store cookie
        CookieManager.getInstance().setAcceptThirdPartyCookies(webView, true)

        webView.siteKeysConfiguration = getSiteKeysConfiguration()
        webView.webChromeClient = object : WebChromeClient() {
            override fun onProgressChanged(view: WebView?, newProgress: Int) {
                super.onProgressChanged(view, newProgress)
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                    progressBar.setProgress(newProgress, true)
                } else {
                    progressBar.progress = newProgress
                }
            }
        }
        //tag::client[]
        val patWebViewClient = PatWebViewClient()
        //Configure javascriptInterface and incognito mode for this WebView
        patWebViewClient.setupWebView(webView, incognito = false)
        webView.webViewClient = patWebViewClient
        //end::client[]

        //tag::listen[]
        patWebViewClient.setTabListener(object : PatWebViewClient.TabListener {
            override fun onActiveTabUrl(tabId: String, url: String) {
                //tag::updateContext[]
                //Update Privacy Shield default context to update UI components
                PrivacyShieldUIContext.Default.setActiveTabUrl(tabId, url)
                //end::updateContext[]
            }
        })
        //end::listen[]
        val nestedWebViewClient = object : WebViewClient() {
            override fun onPageStarted(view: WebView, url: String, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)
                navEt.setText(url)
                progressBar.progress = 0
                progressBar.visibility = View.VISIBLE
            }

            override fun onPageFinished(view: WebView, url: String) {
                swipeRefreshLayout.isRefreshing = false
                progressBar.progress = 100
                progressBar.visibility = View.GONE
            }

            override fun onReceivedError(
                view: WebView?, request: WebResourceRequest?, error: WebResourceError?
            ) {
                super.onReceivedError(view, request, error)
                progressBar.visibility = View.GONE
            }
        }
        //tag::nestedClient[]
        patWebViewClient.setWebViewClient(nestedWebViewClient)
        //end::nestedClient[]

        loadUrl(intent)

        if (PrivacyShieldUI.get().preferences.getTermsAndConditionsState() == TermsAndConditionsState.NONE) {
            PrivacyShieldTnCDialogFragment.newInstance(true)
                .showOnce(supportFragmentManager, PrivacyShieldTnCDialogFragment.TAG)
        }
    }

    override fun onResume() {
        super.onResume()
        requireAnalytics.addEventListener(this)
    }

    override fun onPause() {
        super.onPause()
        requireAnalytics.removeEventListener(this)
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        loadUrl(intent)
    }

    private fun loadUrl(loadIntent: Intent?) {
        if (loadIntent?.action == Intent.ACTION_VIEW && loadIntent.dataString != null) {
            webView.loadUrl(loadIntent.dataString!!)
        } else {
            webView.loadUrl("https://crumbs-benchmark.web.app/")
        }
    }

    override fun onBackPressed() {
        if (webView.canGoBack()) {
            webView.goBack()
        } else {
            finish()
        }
    }

    //TODO ask abp why this is required
    private fun getSiteKeysConfiguration(): SiteKeysConfiguration {
        val signatureVerifier: SignatureVerifier = JavaSignatureVerifier()
        val publicKeyHolder: PublicKeyHolder = PublicKeyHolderImpl()
        val httpClient = AndroidHttpClient(true)
        val base64Processor: Base64Processor = AndroidBase64Processor()
        val siteKeyVerifier = SiteKeyVerifier(signatureVerifier, publicKeyHolder, base64Processor)
        return SiteKeysConfiguration(
            signatureVerifier, publicKeyHolder, httpClient, siteKeyVerifier
        )
    }

    override fun onNavigation() {
        Timber.d("New page loaded")
    }

    override fun onResourceLoadingBlocked(info: AdblockWebView.EventsListener.BlockedResourceInfo?) {
        Timber.d("onResourceLoadingBlocked")
    }

    override fun onResourceLoadingAllowlisted(info: AdblockWebView.EventsListener.AllowlistedResourceInfo?) {
        Timber.d("onResourceLoadingAllowlisted")
    }

    override fun onEvent(event: Event<*>) {
        if (event.id == Event.Cmp.CMP_SHOWN) {
            Snackbar.make(
                webView,
                getText(R.string.cookie_popup_detected),
                Snackbar.LENGTH_LONG
            ).setAction(getText(com.eyeo.privacy.ui.R.string.ps_more)) {
                startActivity(
                    PrivacyShieldSettingsActivity.createIntent(
                        this, highlightedFeature = getString(
                            com.eyeo.privacy.ui.R.string.ps_key_hide_cookie_popups
                        )
                    )
                )
            }.show()
        }
    }

}