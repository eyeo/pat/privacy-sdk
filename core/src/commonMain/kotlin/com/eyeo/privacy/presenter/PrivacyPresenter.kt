package com.eyeo.privacy.presenter

import com.eyeo.pat.models.SettingsChangeListener
import com.eyeo.pat.provider.SettingsEditor
import com.eyeo.pat.provider.SettingsProvider
import com.eyeo.pat.utils.Coroutines.toCallback
import com.eyeo.pat.utils.UrlExtension.getUrlHost
import com.eyeo.pat.utils.UrlExtension.toUrl
import io.ktor.http.*
import com.eyeo.privacy.models.FilterLevel
import com.eyeo.privacy.models.PrivacySettings
import com.eyeo.privacy.service.PrivacyService
import kotlin.js.JsExport
import kotlin.js.JsName

/**
 * This presenter is the entry point to configure privacy feature
 */
@Suppress("unused", "NON_EXPORTABLE_TYPE")
@JsExport
class PrivacyPresenter internal constructor(private val privacyService: PrivacyService) {

    /**
     * check is the pass url is protected by privacy feature
     *
     * @param url to check
     *
     * @return true if the url is protected, false otherwise
     */
    fun isProtectionEnabledForUrl(url: String) = privacyService.isProtectionEnabled(url.toUrl())

    /**
     * get a snapshot of privacy settings
     *
     * @return privacy settings
     */
    fun getSettings(): PrivacySettings = privacyService.getSettings()


    /**
     * gets the settings provider
     *
     * @return privacy settings provider
     */
    fun getSettingsProvider(): SettingsProvider<PrivacySettings> = privacyService.getSettingsProvider()

    /**
     * create an editor to change current settings
     *
     * @return a new settings editor
     */
    fun editSettings() = Editor(privacyService.getSettingsProvider())

    /**
     * listen privacy settings changes
     *
     * @return a settings flow
     */
    @Suppress("NON_EXPORTABLE_TYPE")
    fun listenSettings() = privacyService.listenSettings()

    /**
     * @see listenSettings
     *
     * @param changeListener will be called on main thread each time privacy settings updated
     *
     * @return a cancellable object to end listening
     */
    @JsName("listenSettingsWithListener")
    fun listenSettings(changeListener: SettingsChangeListener<PrivacySettings>) =
        listenSettings { settings -> changeListener.onSettingsChanged(settings) }

    /**
     * @see listenSettings
     *
     * @param callback will be called on main thread each time privacy settings updated
     *
     * @return a cancellable object to end listening
     */
    @JsName("listenSettingsWithCallback")
    fun listenSettings(callback: (PrivacySettings) -> Unit) = listenSettings().toCallback(callback)

    /**
     * Check if a request is third party in the document context
     *
     * @param documentUrl document url
     * @param requestUrl request url
     * @return true is the request is third party, false otherwise
     */
    @Suppress("NON_EXPORTABLE_TYPE")
    fun isThirdPartyRequest(documentUrl: Url, requestUrl: Url) =
        privacyService.isThirdPartyRequest(documentUrl, requestUrl)

    /**
     * Returns the main domain corresponding to the document URL
     *
     * @param documentUrl document url
     * @return the main domain
     */
    fun getMainDomain(documentUrl: Url) = privacyService.getMainDomain(documentUrl)

    class Editor internal constructor(settingsProvider: SettingsProvider<PrivacySettings>) :
        SettingsEditor<PrivacySettings>(settingsProvider) {

        fun enable(enabled: Boolean) = this.apply {
            value = value.copy(enabled = enabled)
        }

        fun controlDomain(url: String) = apply {
            allowedDomains(value.allowedDomains.toHashSet().apply {
                url.getUrlHost()?.let {
                    remove(it)
                }
            })
            allowedDomainsThisSession(value.allowedDomainsThisSession.toHashSet().apply {
                url.getUrlHost()?.let {
                    remove(it)
                }
            })
        }

        fun allowDomain(url: String) = apply {
            allowedDomains(value.allowedDomains.toHashSet()
                .apply {
                    url.getUrlHost()?.let {
                        add(it)
                    }
                })
        }

        fun allowDomainThisSession(url: String) = apply {
            allowedDomainsThisSession(value.allowedDomainsThisSession.toHashSet()
                .apply {
                    url.getUrlHost()?.let {
                        add(it)
                    }
                })
        }

        fun resetAllowedDomainsThisSession() = apply {
            value = value.copy(allowedDomainsThisSession = setOf())
        }

        fun allowedDomains(@Suppress("NON_EXPORTABLE_TYPE") domains: Set<String>) = apply {
            value = value.copy(allowedDomains = domains)
        }

        fun allowedDomainsThisSession(@Suppress("NON_EXPORTABLE_TYPE") domains: Set<String>) =
            apply {
                value = value.copy(allowedDomainsThisSession = domains)
            }

        fun hideReferrerHeader(hideReferrerHeader: Boolean) = apply {
            value = value.copy(hideReferrerHeader = hideReferrerHeader)
        }

        fun hideCookieConsentPopups(hideCookieConsentPopups: Boolean) = apply {
            value = value.copy(hideCookieConsentPopups = hideCookieConsentPopups)
        }

        fun enableDoNotTrack(enableDoNotTrack: Boolean) = apply {
            value = value.copy(enableDoNotTrack = enableDoNotTrack)
        }

        fun enableGPC(enableGPC: Boolean) = apply {
            value = value.copy(enableGPC = enableGPC)
        }

        fun enableCNameCloakingProtection(enable: Boolean) = apply {
            value = value.copy(enableCNameCloakingProtection = enable)
        }

        fun removeMarketingTrackingParameters(removeMarketingTrackingParameters: Boolean) = apply {
            value =
                value.copy(removeMarketingTrackingParameters = removeMarketingTrackingParameters)
        }

        fun cookiesFilterLevel(cookiesFilterLevel: FilterLevel) = apply {
            value = value.copy(cookiesFilterLevel = cookiesFilterLevel)
        }

        fun blockTrackers(blockTrackers: Boolean) = apply {
            value = value.copy(blockTrackers = blockTrackers)
        }

        fun blockThirdPartyCookies(blockThirdPartyCookies: Boolean) = apply {
            value = value.copy(blockThirdPartyCookies = blockThirdPartyCookies)
        }

        fun blockHyperlinkAuditing(blockHyperlinkAuditing: Boolean) = apply {
            value = value.copy(blockHyperlinkAuditing = blockHyperlinkAuditing)
        }

        fun enableFingerprintShield(enable: Boolean) = apply {
            value = value.copy(enableFingerprintShield = enable)
        }

        fun blockSocialMediaIconsTracking(enable: Boolean) = apply {
            value = value.copy(blockSocialMediaIconsTracking = enable)
        }

        fun enableDeAMP(enable: Boolean) = apply {
            value = value.copy(deAMP = enable)
        }

        fun resetDefault() = apply {
            value = PrivacySettings()
        }
    }
}