package com.eyeo.privacy

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import org.junit.After
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import kotlin.test.assertTrue

@RunWith(AndroidJUnit4::class)
class PrivacyShieldAndroidTest {

    @Before
    fun init() = PrivacyShieldAndroid.release()

    @After
    fun release() = PrivacyShieldAndroid.release()

    @Test
    fun testSetup() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        //tag::setup[]
        PrivacyShieldAndroid.setup(context)
        //end::setup[]
        //tag::isInitialized[]
        val initialized = PrivacyShieldAndroid.isInitialized()
        //end::isInitialized[]
        assertTrue(initialized)
        //tag::instance[]
        val privacyShield = PrivacyShieldAndroid.get()
        //end::instance[]
        assertNotNull(privacyShield)
    }
}