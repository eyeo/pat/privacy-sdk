package com.eyeo.privacy

import com.eyeo.pat.browser.BrowserIntegration
import com.eyeo.pat.browser.BrowserPluginApi
import com.eyeo.privacy.PrivacyShieldCore.Companion.COOKIE_HIDING_FILTER_LIST
import com.eyeo.privacy.PrivacyShieldCore.Companion.PRIVACY_FILTER_LIST
import com.eyeo.privacy.PrivacyShieldCore.Companion.PRIVACY_FRIENDLY_LIST
import com.eyeo.privacy.PrivacyShieldCore.Companion.SOCIAL_FILTER_LIST
import com.eyeo.privacy.models.PrivacySettings
import com.eyeo.privacy.service.DomainService
import com.eyeo.privacy.service.PrivacyService
import io.ktor.http.Url
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.mockk
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Test
import java.util.concurrent.CyclicBarrier
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

@OptIn(DelicateCoroutinesApi::class)
class PrivacyShieldPluginTests {

    @RelaxedMockK
    private lateinit var privacyService: PrivacyService

    private lateinit var plugin: PrivacyShieldPlugin
    @RelaxedMockK
    private lateinit var domainService : DomainService

    private var settings: PrivacySettings = PrivacySettings()

    private var flow = MutableStateFlow(settings)

    var listenValue = listOf<String>()
    var barrier = CyclicBarrier(2)

    @Before
    fun setup() {
        Dispatchers.setMain(Dispatchers.Default)

        MockKAnnotations.init(this, relaxUnitFun = true)

        every { privacyService.getSettings() }.answers { settings }
        every { privacyService.listenSettings() }.returns(flow)

        plugin = PrivacyShieldPlugin(
            privacyService = privacyService,
            statsService = mockk(relaxed = true),
            headerService = mockk(relaxed = true),
            domainService = domainService,
            cmpService = mockk(relaxed = true),
            privacyShieldOptions = PrivacyShieldOptions(),
        )

        runBlocking {
            GlobalScope.launch(Executors.newSingleThreadExecutor().asCoroutineDispatcher()) {
                plugin.setup()
            }
        }
    }

    @After
    fun release() {
        listenValue = listOf()
        barrier.reset()
        settings = PrivacySettings()
        plugin.destroy()
    }


    @Test
    fun testFilterListSubscriptions() {
        applySettings(
            settings.copy(
                hideCookieConsentPopups = true,
                enabled = true
            )
        )
        BrowserIntegration.FilterEngine.setFilterEngineConfigListener(object :
            BrowserPluginApi.FilterEngineConfigListener {
            override fun setAllowedDomains(group: String, allowedDomains: List<String>) {
            }

            override fun setSubscriptions(group: String, subscriptionUrls: List<String>) {
                listenValue = subscriptionUrls
                barrier.await(1, TimeUnit.SECONDS)
            }
        })
        barrier.await(1, TimeUnit.SECONDS)
        barrier.reset()
        var expected = listOf(
            PRIVACY_FILTER_LIST,
            PRIVACY_FRIENDLY_LIST,
            COOKIE_HIDING_FILTER_LIST,
            SOCIAL_FILTER_LIST
        )
        assertEquals(expected, listenValue)

        applySettings(
            settings.copy(
                hideCookieConsentPopups = false,
                blockSocialMediaIconsTracking = false,
                enabled = true
            )
        )
        barrier.await(1, TimeUnit.SECONDS)
        barrier.reset()
        expected = listOf(
            PRIVACY_FILTER_LIST,
            PRIVACY_FRIENDLY_LIST,
        )
        assertEquals(expected, listenValue)

        applySettings(
            settings.copy(
                hideCookieConsentPopups = true,
                enabled = false
            )
        )
        barrier.await(1, TimeUnit.SECONDS)
        barrier.reset()
        expected = listOf()
        assertEquals(expected, listenValue)
    }


    @Test
    fun testAllowedDomain() {
        applySettings(
            settings.copy(
                enabled = true
            )
        )

        BrowserIntegration.FilterEngine.setFilterEngineConfigListener(object :
            BrowserPluginApi.FilterEngineConfigListener {
            override fun setAllowedDomains(group: String, allowedDomains: List<String>) {
                listenValue = allowedDomains
                barrier.await(1, TimeUnit.SECONDS)
            }

            override fun setSubscriptions(group: String, subscriptionUrls: List<String>) {
            }
        })
        barrier.await(100, TimeUnit.SECONDS)
        barrier.reset()

        assertEquals(emptyList(), listenValue)

        val expectedAllowedDomains = listOf("google.com")
        val expectedAllowedDomainsThisSession = listOf("amazon.com")

        applySettings(
            settings.copy(
                allowedDomains = expectedAllowedDomains.toSet(),
                allowedDomainsThisSession = expectedAllowedDomainsThisSession.toSet()
            )
        )

        barrier.await(1, TimeUnit.SECONDS)
        barrier.reset()

        // expect that the contents of both allow lists are considered
        val expected = expectedAllowedDomains + expectedAllowedDomainsThisSession
        assertEquals(expected, listenValue)

        applySettings(
            settings.copy(
                enabled = false
            )
        )

        barrier.await(1, TimeUnit.SECONDS)
        barrier.reset()

        assertEquals(emptyList(), listenValue)

    }

    @Test
    fun testShouldClearReferrer() {
        val docUrl = Url("https://google.com")
        val referrerUrl = Url("https://eviltracker.com")

        every { domainService.isThirdPartyRequest(any(), any(), any()) }.returns(true)
        every { privacyService.isProtectionEnabled(docUrl)}.returns(false)

        applySettings(settings.copy(enabled = false, hideReferrerHeader = true))
        assertFalse("Referrer should not be cleared") { plugin.shouldClearReferrer(docUrl, referrerUrl, isMainFrame = false) }

        every { privacyService.isProtectionEnabled(docUrl)}.returns(true)

        applySettings(settings.copy(enabled = true, hideReferrerHeader = false))
        assertFalse("Referrer should not be cleared") { plugin.shouldClearReferrer(docUrl, referrerUrl, isMainFrame = false) }

        applySettings(settings.copy(enabled = true, hideReferrerHeader = true))
        assertTrue("Referrer should be cleared") { plugin.shouldClearReferrer(docUrl, referrerUrl, isMainFrame = false) }
    }

    private fun applySettings(settings: PrivacySettings) {
        this.settings = settings
        flow.value = settings
    }
}