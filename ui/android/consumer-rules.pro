-keep class androidx.preference.SwitchPreferenceCompat
-keepclassmembers class androidx.preference.SwitchPreferenceCompat { *; }
-keep class com.google.android.material.bottomsheet.BottomSheetBehavior { *; }
-keep class androidx.preference.CheckBoxPreference

-keepclassmembers class * {
    @android.webkit.JavascriptInterface <methods>;
}

-keep class com.eyeo.privacy.ui.CrumbsInterestsFragment

#################   Crumbs   #################
-keepclassmembers class com.eyeo.privacy.* {
    @android.webkit.JavascriptInterface <methods>;
}