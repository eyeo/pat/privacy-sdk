package com.eyeo.privacy

import android.webkit.JavascriptInterface
import com.eyeo.privacy.service.StatsService
import com.eyeo.privacy.analytics.Event
import com.eyeo.privacy.presenter.AnalyticsPresenter

internal actual class CmpJsInterface internal actual constructor(private val statsService: StatsService,
                                                                 private val analyticsPresenter: AnalyticsPresenter) {
    @JavascriptInterface
    fun onCmpUiShown() {
        analyticsPresenter.notifyEvent(Event.Cmp.CMP_SHOWN)
        statsService.onPopupShown()
    }
}