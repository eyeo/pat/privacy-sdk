function addPrefix(prefix, key, delimiter = '##PS##') {
    return `${prefix}${delimiter}${key.trim()}`;
}

function removePrefix(prefix, key, delimiter = '##PS##') {
    var re = new RegExp(`${prefix}${delimiter}`, 'g');
    return key.replace(re, '').trim();
}

function getSymbol(key) {
    return Symbol.for(`crumbs:${key}`);
}

function createSecureProxy(func, handler) {
    var obj = new Proxy(func, handler);
    obj.toString = func.toString.bind(func);
    obj[getSymbol('toString')] = func.toString.bind(func); // store reference to original toString
    return obj;
}

function partitionCookie(supportChips, prefix, cookieString, delimiter = '##PS##') {
    // Firefox BUG: If server sends 2 Set-Cookie headers their values are combined here with \n as delimiter
    return cookieString.split('\n').map(function (v) {
        if (supportChips) {
            return cookieString + ";partitioned";
        } else {
            // Remove "Expires" and "Max-Age" attributes to make it a session cookie
            var cookieValue = v.replace(/;\s*expires=[^;]*/i, '');
            cookieValue = cookieValue.replace(/;\s*max-age=\d*/i, '');
            // add the prefix to the cookie name
            return addPrefix(prefix, cookieValue.trim(), delimiter);
        }
    }).join('\n');
}

function filterCookiesWithPrefix(prefix, cookieString, delimiter = '##PS##') {
    var cookies = cookieString.split(';').filter(function (cookie) {
        return cookie.trim().indexOf(prefix) === 0;
    });
    return {
        value: removePrefix(prefix, cookies.join(';'), delimiter),
        count: cookies.length,
    };
}

function setupCookiesOverwriteCode(blockCookies, supportChips, prefix) {
    // ================= Document.cookie ===============
    // @see https://developer.mozilla.org/en-US/docs/Web/API/Document/cookie
    try {
        var cookieDesc = Object.getOwnPropertyDescriptor(Document.prototype, 'cookie');
        var cookieGetProxy = {
            apply(target, thisArg, argArray) {
                var cookieList = Reflect.apply(target, thisArg, argArray);
                if (supportChips) {
                    return cookieList;
                }
                var { value } = filterCookiesWithPrefix(prefix, cookieList);
                return value;
            }
        };

        var cookieSetProxy = {
            apply(target, thisArg, argArray) {
                if (blockCookies) return;

                var cookieString = argArray[0];
                // safeguard in case the argument does not exist, or it's not string
                if (typeof cookieString !== 'string' || !cookieString) {
                    return Reflect.apply(target, thisArg, argArray);
                }

                var value = partitionCookie(supportChips, prefix, cookieString);
                return Reflect.apply(target, thisArg, [value]);
            }
        };

        Object.defineProperty(Document.prototype, 'cookie', {
            ...cookieDesc,
            get: createSecureProxy(cookieDesc.get, cookieGetProxy),
            set: createSecureProxy(cookieDesc.set, cookieSetProxy),
        });
    } catch (e) {
        // ignore error
    }

    // ==================== CookieStore =====================
    // @see https://developer.mozilla.org/en-US/docs/Web/API/CookieStore
    try {
        var cookieStorePrototype = window["CookieStore"].prototype;

        var cookieStoreSetDesc = Object.getOwnPropertyDescriptor(cookieStorePrototype, 'set');
        var cookieStoreSetProxy = {
            apply(target, thisArg, argArray) {
                if (blockCookies) return;

                var cookieName = argArray[0];
                if (argArray.length === 1) {
                    cookieName = argArray[0].name;
                }

                if (!supportChips) {
                    cookieName = addPrefix(prefix, cookieName);
                }

                // Method 1: await cookieStore.set(name, value)
                // do nothing as this will create a Session cookie
                if (argArray.length > 1) {
                    return Reflect.apply(target, thisArg, [cookieName, argArray[1]]);
                }

                // Method 2: await cookieStore.set(options)
                // if `options.expires` is not present, then it'll create a Session cookie
                var options = argArray[0];
                options.name = cookieName;

                if (options.expires !== undefined) {
                    delete options.expires;
                }
                if (supportChips) {
                    options.partitioned = true;
                }
                return Reflect.apply(target, thisArg, argArray);
            }
        }
        Object.defineProperty(cookieStorePrototype, 'set', {
            ...cookieStoreSetDesc,
            value: createSecureProxy(cookieStoreSetDesc.value, cookieStoreSetProxy),
        });

        var cookieStoreGetDesc = Object.getOwnPropertyDescriptor(cookieStorePrototype, 'get');
        var cookieStoreGetProxy = {
            async apply(target, thisArg, argArray) {
                if (!supportChips) {
                    var cookie;
                    if (typeof argArray[0] === 'string') {
                        cookie = await Reflect.apply(target, thisArg, [addPrefix(prefix, argArray[0])]);
                    } else {
                        argArray[0].name = addPrefix(prefix, argArray[0].name);
                        cookie = await Reflect.apply(target, thisArg, argArray[0]);
                    }

                    if (cookie) {
                        cookie.name = removePrefix(prefix, cookie.name);
                        return cookie;
                    }
                }
                return await Reflect.apply(target, thisArg, argArray);
            }
        }
        Object.defineProperty(cookieStorePrototype, 'get', {
            ...cookieStoreGetDesc,
            value: createSecureProxy(cookieStoreGetDesc.value, cookieStoreGetProxy),
        });

        var cookieStoreGetAllDesc = Object.getOwnPropertyDescriptor(cookieStorePrototype, 'getAll');
        var cookieStoreGetAllProxy = {
            async apply(target, thisArg, argArray) {
                if (!supportChips) {
                    if (argArray.length === 0) {
                        var cookies = await Reflect.apply(target, thisArg, argArray);
                        return cookies.filter(cookie => cookie.name.indexOf(prefix) === 0).map(cookie => {
                            cookie.name = removePrefix(prefix, cookie.name);
                            return cookie;
                        });
                    } else {
                        var cookieName = argArray[0];
                        if (typeof cookieName !== 'string') {
                            cookieName = argArray[0].name;
                        }
                        var prefixedCookies = await Reflect.apply(target, thisArg, [addPrefix(prefix, cookieName), argArray[1]]);
                        return prefixedCookies.map(cookie => {
                            cookie.name = cookieName;
                            return cookie;
                        });
                    }
                }
                return await Reflect.apply(target, thisArg, argArray);
            }
        }
        Object.defineProperty(cookieStorePrototype, 'getAll', {
            ...cookieStoreGetAllDesc,
            value: createSecureProxy(cookieStoreGetAllDesc.value, cookieStoreGetAllProxy),
        });
    } catch (e) {
        // ignore error
    }
}

function setupLocalStorageOverwriteCode(prefix) {
    var setItemDesc = Object.getOwnPropertyDescriptor(Storage.prototype, 'setItem');
    Object.defineProperty(Storage.prototype, 'setItem', {
        value(key, value) {
            if (localStorage === this && key !== undefined && key !== null) {
                return setItemDesc.value.call(localStorage, addPrefix(prefix, key), value);
            }
            return setItemDesc.value.call(this, key, value);
        },
        writable: false,
    });

    var getItemDesc = Object.getOwnPropertyDescriptor(Storage.prototype, 'getItem');
    Object.defineProperty(Storage.prototype, 'getItem', {
        value(key) {
            if (localStorage === this && key !== undefined && key !== null) {
                return getItemDesc.value.call(localStorage, addPrefix(prefix, key));
            }
            return getItemDesc.value.call(this, key);
        },
        writable: false,
    });

    var removeItemDesc = Object.getOwnPropertyDescriptor(Storage.prototype, 'removeItem');
    Object.defineProperty(Storage.prototype, 'removeItem', {
        value(key) {
            if (localStorage === this && key !== undefined && key !== null) {
                return removeItemDesc.value.call(localStorage, addPrefix(prefix, key));
            }
            return removeItemDesc.value.call(this, key);
        },
        writable: false,
    });
}

var blockAllCookies = '%BLOCK_COOKIES%' === 'true';
var prefix = '%SANDBOX_PREFIX%';
var supportChips = '%SUPPORT_CHIPS%' === 'true';
var supportStoragePartitioning = "%SUPPORT_STORAGE_PARTITIONING%" === 'true'
if (prefix) {
    setupCookiesOverwriteCode(blockAllCookies, supportChips, prefix);
    if (!supportStoragePartitioning) {
        setupLocalStorageOverwriteCode(prefix);
    }
}
