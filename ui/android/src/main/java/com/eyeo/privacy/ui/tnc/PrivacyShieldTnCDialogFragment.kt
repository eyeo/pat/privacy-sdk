package com.eyeo.privacy.ui.tnc

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.setFragmentResult
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.eyeo.privacy.PrivacyShieldProvider
import com.eyeo.privacy.analytics.Event
import com.eyeo.privacy.models.TermsAndConditionsState
import com.eyeo.privacy.ui.PrivacyShieldCoreExtension.ui
import com.eyeo.privacy.ui.R
import com.eyeo.privacy.ui.analytics.UiEvent.TermsAndConditions
import com.eyeo.privacy.ui.databinding.PrivacyShieldDialogActivationBinding

class PrivacyShieldTnCDialogFragment : DialogFragment(), PrivacyShieldProvider {

    private var enableAfterAcceptance = false
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        requireAnalytics.notifyEvent(Event.TermsAndConditions.OPEN_DIALOG)
        val binding = PrivacyShieldDialogActivationBinding.inflate(LayoutInflater.from(context))
        return MaterialAlertDialogBuilder(requireContext())
            .setView(binding.root)
            .setTitle(getString(R.string.ps_tnc_dialog_title))
            .setPositiveButton(getString(R.string.ps_tnc_dialog_accept)) { _, _ ->
                requirePrivacyShield.ui().preferences.setTermsAndConditionsState(TermsAndConditionsState.ACCEPTED)
                if (enableAfterAcceptance) {
                    requirePrivacy.editSettings().enable(true).apply()
                }
            }
            .setNeutralButton(getString(R.string.ps_tnc_dialog_later)) { _, _ ->
                setFragmentResult(
                    RESULT_KEY,
                    Bundle().apply { putBoolean(RESULT_VALUE_KEY, false) })
                requirePrivacyShield.ui().preferences.setTermsAndConditionsState(
                    TermsAndConditionsState.LATER)
            }
            .create()

    }

    companion object {
        fun newInstance(enableAfterAccept: Boolean = false): PrivacyShieldTnCDialogFragment {
            val dialog = PrivacyShieldTnCDialogFragment()
            dialog.enableAfterAcceptance = enableAfterAccept
            return dialog
        }

        const val RESULT_KEY = "com.eyeo.privacy.ui.tnc.PrivacyShieldTnCDialogFragment"
        const val RESULT_VALUE_KEY = "result"
        const val TAG = "PrivacyShieldTnCDialogFragment"
    }
}