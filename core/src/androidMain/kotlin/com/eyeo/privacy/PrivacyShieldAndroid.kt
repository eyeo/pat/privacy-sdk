package com.eyeo.privacy

import android.content.Context
import com.eyeo.pat.provider.AndroidResourceProvider
import com.russhwolf.settings.SharedPreferencesSettings
import com.eyeo.privacy.service.AndroidDnsClient

@Suppress("unused")
class PrivacyShieldAndroid {

    companion object {

        const val SETTINGS_NAME = "privacy_shield"

        @JvmStatic
        fun get() = PrivacyShieldCore.get()

        @JvmStatic
        @JvmOverloads
        fun setup(
            context: Context,
            options: PrivacyShieldOptions = PrivacyShieldOptions()
        ) {
            PrivacyShieldCore.createInstance(
                SharedPreferencesSettings.Factory(context).create(SETTINGS_NAME),
                AndroidResourceProvider(context),
                AndroidDnsClient(context),
                options,
            )
        }

        @JvmStatic
        fun release() = PrivacyShieldCore.destroy()

        @JvmStatic
        fun isInitialized() = PrivacyShieldCore.isInitialized()
    }
}