package com.eyeo.privacy.service

import com.eyeo.pat.browser.plugins.JavascriptPlugin
import com.eyeo.pat.models.RequestType
import com.eyeo.pat.provider.ResourcesProvider
import com.eyeo.pat.provider.SettingsProvider
import com.eyeo.privacy.CmpJsInterface
import com.eyeo.privacy.models.PrivacySettings
import com.eyeo.privacy.presenter.AnalyticsPresenter
import io.ktor.http.Url

internal class CmpService(
    private val privacySettings: SettingsProvider<PrivacySettings>,
    private val resourcesProvider: ResourcesProvider,
    private val statsService: StatsService,
    private val analyticsPresenter: AnalyticsPresenter
) {
    internal suspend fun createCmpJsBuilder(): JavascriptPlugin {
        return CmpJsBuilder(
            snippet = resourcesProvider.loadResource(CMP_SNIPPET_FILE)
        )
    }

    inner class CmpJsBuilder(private val snippet: String) : JavascriptPlugin {

        private val jsInterface = CmpJsInterface(statsService, analyticsPresenter)
        override fun createJavaScriptSnippet(
            tabId: String,
            documentUrl: Url,
            requestUrl: Url,
            requestType: RequestType
        ): String {
            val enabled = privacySettings.isEnabled()
            return if (enabled) snippet else ""
        }

        override fun createJsInterface(): Pair<String, Any> {
            return Pair(INTERFACE_NAME, jsInterface)
        }
    }

    companion object {
        private const val CMP_SNIPPET_FILE = "data/cmp_snippet.js"
        private const val INTERFACE_NAME = "PsCmpJsInterface"
    }
}