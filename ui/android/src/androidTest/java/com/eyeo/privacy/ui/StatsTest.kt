package com.eyeo.privacy.ui

import android.content.Intent
import android.widget.FrameLayout
import androidx.lifecycle.Lifecycle
import androidx.test.core.app.ActivityScenario.launch
import androidx.test.espresso.Espresso
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import androidx.test.platform.app.InstrumentationRegistry
import com.eyeo.pat.test.FragmentScenario
import com.eyeo.privacy.PrivacyShieldAndroid
import com.eyeo.privacy.ui.view.PrivacyShieldButton
import com.eyeo.privacy.ui.view.PrivacyShieldPopup
import com.eyeo.privacy.ui.view.PrivacyShieldUIContext
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@MediumTest
@RunWith(AndroidJUnit4::class)
class StatsTest {

    private val context = InstrumentationRegistry.getInstrumentation().targetContext

    @Before
    fun setup() {
        PrivacyShieldAndroid.release()
        PrivacyShieldAndroid.setup(context)
    }

    @After
    fun release(){
        PrivacyShieldAndroid.release()
    }

    @Test
    fun testPopup() {
        val intent = Intent(context, FragmentScenario.EmptyFragmentActivity::class.java).apply {
            putExtra(
                FragmentScenario.EmptyFragmentActivity.THEME_EXTRAS_BUNDLE_KEY,
                R.style.Theme_PrivacyShield_Settings_Primary
            )
            putExtra(
                FragmentScenario.EmptyFragmentActivity.ACTIVITY_CONTAINER_ID_KEY,
                R.id.ps_activity_container
            )
        }
        launch<FragmentScenario.EmptyFragmentActivity>(intent)
            .moveToState(Lifecycle.State.RESUMED)
            .onActivity { activity ->
                val psButton = PrivacyShieldButton(activity)
                activity.setContentView(FrameLayout(activity).apply {
                    addView(
                        psButton,
                        FrameLayout.LayoutParams(
                            FrameLayout.LayoutParams.WRAP_CONTENT,
                            FrameLayout.LayoutParams.WRAP_CONTENT
                        )
                    )
                })
                //tag::PrivacyShieldPopup[]
                PrivacyShieldPopup(activity)
                    .show(anchor = psButton, privacyShieldUIContext = PrivacyShieldUIContext.Default)
                //end::PrivacyShieldPopup[]
            }
        Espresso.onView(ViewMatchers.withId(R.id.ps_board_bottom))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }
}