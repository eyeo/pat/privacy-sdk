package com.eyeo.privacy.provider

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.eyeo.pat.provider.AndroidResourceProvider
import kotlinx.coroutines.runBlocking
import kotlinx.serialization.builtins.serializer
import com.eyeo.privacy.CoreBuildConfig
import org.junit.Assert.assertTrue
import org.junit.Test
import org.junit.runner.RunWith
import kotlin.test.assertNotNull
import kotlin.time.ExperimentalTime
import kotlin.time.measureTime


@RunWith(AndroidJUnit4::class)
class AndroidResourceProviderTests {

    @ExperimentalTime
    @Test
    fun testLoadDomains() = runBlocking {
        val time = measureTime {
            val appContext = InstrumentationRegistry.getInstrumentation().targetContext
            val script = AndroidResourceProvider(context = appContext).load(
                "data/fingerprint_shield.js",
                String.serializer()
            )
            assertTrue(script.isNotEmpty())
        }
        println("load fingerprint shield in $time ms")
    }

    @Test
    fun testConfig() {
        assertNotNull(CoreBuildConfig.version)
    }

}
