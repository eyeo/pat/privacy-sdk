package com.eyeo.privacy.ui.view

import android.content.Context
import android.content.res.Resources
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.View
import android.view.ViewGroup
import android.widget.PopupWindow
import com.eyeo.pat.utils.ContextExtension.getActivity
import com.eyeo.privacy.PrivacyShieldProvider
import com.eyeo.privacy.analytics.Event
import com.eyeo.privacy.ui.PrivacyShieldSettingsActivity
import com.eyeo.privacy.ui.R
import com.eyeo.privacy.ui.analytics.UiEvent.Popup

class PrivacyShieldPopup(context: Context) : PopupWindow(),
    PrivacyShieldPopupView.NavigationListener,
    PrivacyShieldProvider {

    private val popupView: PrivacyShieldPopupView

    init {
        isFocusable = true
        isOutsideTouchable = true
        setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        width = context.resources.getDimensionPixelSize(R.dimen.ps_popup_width)
        height = ViewGroup.LayoutParams.WRAP_CONTENT
        popupView = PrivacyShieldPopupView(context)
        contentView = popupView
        popupView.setClickListener(object : PrivacyShieldPopupView.ClickListener {
            override fun onSettingsButtonClick() {
                context.startActivity(
                    PrivacyShieldSettingsActivity.createIntent(
                        context,
                        parentIntent = context.getActivity()?.intent
                    )
                )
            }

        })
        animationStyle = R.style.PrivacyShieldPopupAnimation
    }

    @JvmOverloads
    fun show(
        anchor: View,
        privacyShieldUIContext: PrivacyShieldUIContext = PrivacyShieldUIContext.Default
    ) {
        val padding = (12 * Resources.getSystem().displayMetrics.density).toInt()
        popupView.setPrivacyShieldUIContext(privacyShieldUIContext)
        popupView.setNavigationListener(this)
        showAsDropDown(anchor, -padding, -padding)
        update()
        privacyShield?.analytics()?.notifyEvent(Event.Popup.OPEN_SCREEN)
    }

    override fun onLinkOpen() {
        dismiss()
    }

}