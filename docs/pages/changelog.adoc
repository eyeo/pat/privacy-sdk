---
layout: default
title: ChangeLog
nav_order: 7
description: "Privacy Shield ChangeLog"
permalink: /changelog
---
include::../../docs/config.adoc[]

== ChangeLog

[id=_1.0.5]
=== 1.0.5 - (16/02/2024)

==== Fixes and Improvements
- reuse domain provider from PAT-SDK

[id=_1.0.4]
=== 1.0.4 - (12/02/2024)

==== Fixes and Improvements
- Count blocked cookies when they are sandboxed

[id=_1.0.3]
=== 1.0.3 - (30/11/2023)

==== Fixes and Improvements
- Refactor Analytics

[id=_1.0.2]
=== 1.0.2 - (28/11/2023)

==== Features
- Add CMP popup counting functionality and include this in the statistics

[id=_1.0.1]
=== 1.0.1 - (24/10/2023)

==== Fixes and Improvements
- Add configurable icon for PrivacyShieldButton
- Add click listeners for PrivacyShieldPopup
- UI improvements(Android UI)
- Bugfix for inconsistent UI state after configuration change
- Bugfix in the localStorage sandboxing snippet logic
- Add local storage partitioning option to PrivacyShieldOptions
- Remove proxy related functionality
- Update dependencies

[id=_1.0.0]
=== 1.0.0 - (28/09/2023)
- Initial release

