import org.jetbrains.kotlin.incremental.deleteDirectoryContents

@Suppress("DSL_SCOPE_VIOLATION")
plugins {
    `maven-publish`
    alias(libs.plugins.kotlin.multiplatform)
    alias(libs.plugins.kotlin.serialization)
    alias(libs.plugins.android.library)
    alias(libs.plugins.download)
    alias(libs.plugins.dokka)
    alias(libs.plugins.npm.publish)
    alias(libs.plugins.pat.plugin)
}

group = "com.eyeo.privacy"
version = libs.versions.privacy.library.full.get()

pat {
    publishing {
        baseArtifactId = "privacy-shield-core"
    }
    buildConfig {
        packageName = "com.eyeo.privacy"
        configs {
            create("android")
            create("js")
        }
    }
}

kotlin {
    android {
        publishLibraryVariants = listOf("debug", "release")
        publishLibraryVariantsGroupedByFlavor = true
        compilations.all {
            kotlinOptions.jvmTarget = JavaVersion.VERSION_1_8.toString()
        }
    }
    js(IR) {
        useCommonJs()
        moduleName = "privacy-shield-core"
        browser {
            commonWebpackConfig {
                outputFileName = "privacy-shield-core.js"
            }
            testTask {
                useKarma {
                    useChromeHeadlessNoSandbox()
                }
            }
        }
        binaries.library()
    }
    sourceSets {
        all {
            languageSettings.apply {
                optIn("kotlinx.coroutines.ExperimentalCoroutinesApi")
                optIn("kotlinx.coroutines.FlowPreview")
                optIn("kotlin.js.ExperimentalJsExport")
            }
        }
        named("commonMain") {
            dependencies {
                implementation(kotlin("stdlib-common"))
                api(libs.pat.core)
                api(libs.pat.browser)
            }
        }
        named("commonTest") {
            dependencies {
                implementation(kotlin("test-common"))
                implementation(kotlin("test-annotations-common"))
            }
        }
        named("jsMain") {
            dependencies {
                implementation(npm("dateformat", "5.0.3"))
                implementation(libs.pat.web.ext)
            }
        }
        named("jsTest") {
            dependencies {
                implementation(kotlin("test-js"))
            }
        }
        named("androidMain") {
            dependencies {
                implementation(kotlin("stdlib"))
                implementation(libs.gms.ads.identifier)
            }
        }
        named("androidUnitTest") {
            dependencies {
                implementation(kotlin("test"))
                implementation(kotlin("test-junit"))
            }
        }
        named("androidInstrumentedTest") {
            dependencies {
                implementation(kotlin("test"))
                implementation(kotlin("test-junit"))
            }
        }
    }
}

android {
    compileSdk = libs.versions.android.target.sdk.get().toInt()

    defaultConfig {
        namespace = "com.eyeo.privacy"
        minSdk = libs.versions.android.min.sdk.get().toInt()
        multiDexEnabled = true
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        consumerProguardFiles("consumer-rules.pro")
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }

    packaging {
        resources.excludes.add("META-INF/LICENSE*")
        resources.excludes.add("META-INF/*.kotlin_module")
        resources.excludes.add("META-INF/AL2.0")
        resources.excludes.add("META-INF/LGPL2.1")
        resources.excludes.add("META-INF/licenses/ASM")
        jniLibs.excludes.add("win32-x86-64/*")
        jniLibs.excludes.add("win32-x86/*")
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    dependencies {
        testImplementation(libs.pat.test)
        androidTestImplementation(libs.pat.test)
        androidTestImplementation(libs.pat.webview)
    }

    testOptions {
        unitTests {
            isIncludeAndroidResources = true
        }
    }
}


tasks {

    val fetchFingerprintPackage =
        register<com.eyeo.pat.tasks.DownloadNpmPackage>("fetchFingerprintPackage") {
            version = libs.versions.fingerprint.shield.get()
            registry = "https://gitlab.com/api/v4/packages/npm/"
            packageName = "@eyeo/fingerprint-shield"
            outputFile = File(project.buildDir, "/tmp/npm/fingerprint-shield.tgz")
        }

    val copyFingerprintScript = register<Copy>("copyFingerprintScript") {
        dependsOn(fetchFingerprintPackage)
        from(tarTree(fetchFingerprintPackage.get().outputFile)) {
            include("package/dist/")
        }
        filesMatching("package/dist/**") {
            path = path.replace("package/dist/", "")
        }
        rename { it.replace("-", "_") }
        into(File("src/scripts"))
    }

    val androidDataFolder = "src/androidMain/res/raw"
    val copyAndroidMetaDataTask = register<Copy>("copyAndroidMetaData") {
        dependsOn(copyFingerprintScript)
        from("src/scripts") {
            exclude(".idea/**")
        }
        into(androidDataFolder)
        rename {
            "data_$it".split(".").first()
        }
    }

    getByName("preBuild").dependsOn(copyAndroidMetaDataTask)

    val jsDataFolder = "src/jsMain/resources/data"
    val copyJsMetaDataTask = register<Copy>("copyJsMetaData") {
        dependsOn(copyFingerprintScript)
        from("src/scripts")
        into(jsDataFolder)
    }

    named("jsProcessResources").configure {
        dependsOn(copyJsMetaDataTask)
    }

    getByName<Delete>("clean") {
        delete.add(jsDataFolder)
        delete.add(fileTree(androidDataFolder) {
            include("data_*")
        })
    }

    dokkaHtml {
        outputDirectory.set(rootProject.file("docs/docs"))
        doLast {
            rootProject.file("docs/docs/styles/logo-styles.css")
                .writeText(rootProject.file("docs/docs-styles.css").readText())
        }
    }

    val reportsDirectory = "$buildDir/reports/androidTests/connected"
    val deviceScreenShotsFolder =
        "/sdcard/Android/data/com.eyeo.privacy.test/files/Download/screenshots"
    val hostScreenshotsFolder = "$reportsDirectory/screenshots"

    val cleanScreenshotsTask by registering(Exec::class) {
        description = "cleans the screenshots from the device"
        commandLine(project.android.adbExecutable.toString(), "shell", "rm", "-r", deviceScreenShotsFolder)
        isIgnoreExitValue = true
    }

    val pullScreenshotsTask by registering {
        description =
            "copies screenshots from the device to the host"
        outputs.dir(hostScreenshotsFolder)
        doLast {
            val hostScreenshotsDir = File(hostScreenshotsFolder)
            hostScreenshotsDir.mkdirs()
            hostScreenshotsDir.deleteDirectoryContents()

            exec {
                commandLine("adb", "pull", deviceScreenShotsFolder)
                workingDir = File(reportsDirectory)
                isIgnoreExitValue = true
            }

        }
    }

    val embedScreenshotsTask by registering(EmbedScreenshotsTask::class) {
        description = "embeds failed tests screenshots in the JUnit HTML report"
        onlyIf { getByName("connectedDebugAndroidTest").state.failure != null }
        dependsOn(pullScreenshotsTask)
        inputDirectory = File(hostScreenshotsFolder)
    }

    afterEvaluate {
        tasks.getByName("connectedDebugAndroidTest") {
            dependsOn(cleanScreenshotsTask)
            finalizedBy(embedScreenshotsTask)
        }

        tasks.getByName("dokkaHtml") {
            dependsOn(tasks.getByName("generatePatConfigClass"))
        }
    }
}


