package com.eyeo.privacy.ui.view

import android.animation.ArgbEvaluator
import android.animation.ValueAnimator
import android.content.Context
import androidx.annotation.ColorInt
import com.eyeo.privacy.ui.R

class PrivacyShieldThemeHelper @JvmOverloads constructor(
    private val context: Context,
    private val lightTheme: Boolean,
    private val listener: ThemeListener,
    override var uiContext: PrivacyShieldUIContext = PrivacyShieldUIContext.Default,
) : PrivacyShieldUIContext.UIContextListener, PrivacyShieldUIContext.UIContextAwareImpl {

    private var colorAnimation: ValueAnimator? = null
    private var currentColor: Int = 0
        set(value) {
            field = value
            listener.onColorChange(value)
        }

    fun onAttachedToWindow() {
        uiContext.registerListener(this)
    }

    fun onDetachedFromWindow() {
        uiContext.unregisterListener(this)
        colorAnimation?.cancel()
    }

    interface ThemeListener {
        fun onColorChange(@ColorInt color: Int)
    }

    override fun onProtectionUpdated(status: PrivacyShieldUIContext.ProtectionStatus) {
        val color = status.getColor(context, lightTheme)
        if (color != currentColor) {
            colorAnimation?.cancel()
            colorAnimation =
                ValueAnimator.ofObject(ArgbEvaluator(), currentColor, color).apply {
                    duration =
                        context.resources.getInteger(R.integer.ps_play_button_animation_duration)
                            .toLong()
                    addUpdateListener { animator ->
                        currentColor = animator.animatedValue as Int
                    }
                    start()
                }
        }
    }
}
