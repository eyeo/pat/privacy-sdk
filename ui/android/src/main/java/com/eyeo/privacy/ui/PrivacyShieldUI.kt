package com.eyeo.privacy.ui

import android.content.Context
import androidx.fragment.app.FragmentManager
import com.eyeo.pat.utils.ContextExtension.getVersionCode
import com.eyeo.pat.utils.ContextExtension.getVersionName
import com.eyeo.privacy.PrivacyShieldAndroid
import com.eyeo.privacy.PrivacyShieldCore
import com.eyeo.privacy.models.TermsAndConditionsState
import io.ktor.http.*
import com.eyeo.privacy.ui.tnc.PrivacyShieldTnCDialogFragment
import com.eyeo.privacy.ui.utils.UIPreferences
import com.eyeo.privacy.ui.utils.showOnce
import com.eyeo.privacy.ui.view.PrivacyShieldUIContext

class PrivacyShieldUI {

    val preferences = UIPreferences(PrivacyShieldCore.storageProvider())

    fun defaultContext() = PrivacyShieldUIContext.Default

    fun createFeedbackFormUrl(context: Context, url: String?) =
        URLBuilder("https://crumbs.org/en/report-issue/").also {
            it.parameters.apply {
                append(
                    "version",
                    "${context.packageName}: ${context.getVersionName()} - ${context.getVersionCode()} / Privacy Shield: ${PrivacyShieldAndroid.get().version}"
                )
                url?.let {
                    append("url", url)
                }
            }
        }.buildString()

    fun openLink(context: Context, url: String, forceInsideApp: Boolean = true) =
        preferences.linkOpener?.openLink(
            context,
            url,
            if (forceInsideApp) context.packageName else null
        )

    /**
     * Sets the Privacy Shield enable state if terms and conditions were accepted else shows the TnC dialog
     * @param fragmentManager   used to show the TnC dialog
     * @param enable            the new state
     * @return true if the toggle was successful and false otherwise and the TnC dialog was shown
     */
    fun enablePrivacyShieldWithTnCDialog(
        fragmentManager: FragmentManager,
        enable: Boolean
    ): Boolean {
        return if (preferences.getTermsAndConditionsState() == TermsAndConditionsState.ACCEPTED) {
            PrivacyShieldAndroid.get().privacy().editSettings().enable(enable).apply()
            true
        } else {
            val dialogToShow =
                preferences.termsAndConditionsDialog
                    ?: PrivacyShieldTnCDialogFragment.newInstance(true)
            dialogToShow.showOnce(fragmentManager, PrivacyShieldTnCDialogFragment.TAG)
            false
        }
    }

    companion object {
        private var instance: PrivacyShieldUI? = null

        @JvmStatic
        fun get(): PrivacyShieldUI {
            if (instance == null) {
                instance = PrivacyShieldUI()
            }
            return instance!!
        }
    }
}

object PrivacyShieldCoreExtension {
    fun PrivacyShieldCore.ui() = PrivacyShieldUI.get()
}
