package com.eyeo.privacy.ui.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import com.eyeo.privacy.models.PrivacyShieldStats
import com.eyeo.privacy.ui.databinding.PrivacyShieldViewStatsBinding

class PrivacyShieldStatsView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr), PrivacyShieldUIContext.UIContextAwareImpl {

    private val binding: PrivacyShieldViewStatsBinding

    init {
        binding = PrivacyShieldViewStatsBinding.inflate(LayoutInflater.from(context), this, true)
    }

    override var uiContext: PrivacyShieldUIContext = PrivacyShieldUIContext.Default

    override fun getContextAwareChildren() = listOf<PrivacyShieldUIContext.UIContextAware>(
        binding.psStatsGlobalContainer,
        binding.psStatsTrackersContainer,
        binding.psStatsCmpContainer,
        binding.psStatsTrackersCounterContainer,
        binding.psStatsCmpCounterContainer,
        binding.psStatsCookiesContainer,
        binding.psStatsCookiesCounterContainer,
    )

    fun setStats(psStats: PrivacyShieldStats) {
        binding.psStatsCookiesCounterTv.text = "${psStats.tabStats.cookiesBlocked}"
        binding.psStatsCmpCounterTv.text = "${psStats.tabStats.popupsBlocked}"
        binding.psStatsTrackersCounterTv.text = "${psStats.tabStats.trackersBlocked}"
        var globalCount =
            psStats.globalStats.trackersBlocked + psStats.globalStats.cookiesBlocked + psStats.globalStats.popupsBlocked
        var extra = ""
        if (globalCount > 1000) {
            globalCount /= 1000
            extra = "k"
        }
        binding.psStatsGlobalCounterTv.text = "$globalCount$extra"
    }

}