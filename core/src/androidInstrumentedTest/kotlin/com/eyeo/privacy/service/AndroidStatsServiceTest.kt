package com.eyeo.privacy.service

import android.content.Context
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.eyeo.pat.provider.StorageProvider
import com.russhwolf.settings.SharedPreferencesSettings
import com.eyeo.privacy.models.PrivacyShieldStats
import kotlinx.coroutines.*
import org.junit.*
import org.junit.Assert.assertEquals
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class AndroidStatsServiceTest {

    private val appContext: Context = InstrumentationRegistry.getInstrumentation().targetContext

    @Before
    fun setup() {

    }

    @After
    fun release() {
    }

    @InternalCoroutinesApi
    @Test
    fun testStatsListening() = runBlocking {
        val service = StatsService(StorageProvider(SharedPreferencesSettings.Factory(appContext).create()))

        val result = arrayListOf<PrivacyShieldStats>()

        val job = async {
            service.listenStats("test").collect {
                result.add(it)
            }
        }

        delay(200)
        job.cancel()

        assertEquals(1, result.size)
        service.onCookiesBlocked("test")
        delay(200)

        assertEquals(1, result.size)

        val job2 = launch {
            service.listenStats("test").collect {
                result.add(it)
            }
        }
        delay(200)
        job2.cancel()

        assertEquals(2, result.size)
    }
}