package com.eyeo.privacy.ui.utils

import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager

/**
 * Will show the fragment once. Use this to avoid double dialogs when fast clicking.
 */
fun DialogFragment.showOnce(
    manager: FragmentManager,
    tag: String
) {
    if (manager.findFragmentByTag(tag) == null) {
        this.show(manager, tag)
    }
}