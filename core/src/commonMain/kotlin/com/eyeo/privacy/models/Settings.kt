package com.eyeo.privacy.models

import com.eyeo.pat.models.FeatureSettings
import kotlinx.serialization.Serializable
import kotlin.js.JsExport

@JsExport
@Serializable
data class PrivacySettings(
    override val enabled: Boolean = false,
    val hideReferrerHeader: Boolean = true,
    val hideCookieConsentPopups: Boolean = false,
    val enableDoNotTrack: Boolean = true,
    val enableGPC: Boolean = true,
    val enableCNameCloakingProtection: Boolean = true,
    val removeMarketingTrackingParameters: Boolean = true,
    val cookiesFilterLevel: FilterLevel = FilterLevel.SANDBOX,
    val blockTrackers: Boolean = true,
    val blockThirdPartyCookies: Boolean = true,
    val blockHyperlinkAuditing: Boolean = true,
    val enableFingerprintShield: Boolean = true,
    val blockSocialMediaIconsTracking: Boolean = true,
    val deAMP: Boolean = false,
    /**
     * Domains where privacy protection is disabled
     */
    @Suppress("NON_EXPORTABLE_TYPE")
    val allowedDomains: Set<String> = setOf(),
    /**
     * Domains where privacy protection is disabled for this session
     */
    @Suppress("NON_EXPORTABLE_TYPE")
    val allowedDomainsThisSession: Set<String> = setOf(),
) : FeatureSettings

@JsExport
@Serializable
enum class FilterLevel {
    SANDBOX,
    ALL
}

@JsExport
@Serializable
enum class TermsAndConditionsState {
    ACCEPTED, REJECTED, LATER, NONE
}