if (navigator.%PROPERTY_KEY% === undefined || navigator.%PROPERTY_KEY% === null) {
    Object.defineProperty(Navigator.prototype, '%PROPERTY_KEY%', {
        value: %PROPERTY_VALUE%,
        writable: false,
    });
}