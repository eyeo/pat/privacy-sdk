@Suppress("DSL_SCOPE_VIOLATION")
plugins {
    `maven-publish`
    alias(libs.plugins.android.library)
    alias(libs.plugins.kotlin.android)
    alias(libs.plugins.pat.plugin)
}

pat {
    publishing {
        baseArtifactId = "privacy-shield-ui-android"
    }
    buildConfig {
        packageName = "com.eyeo.privacy"
        configs {
            create("android")
        }
    }
}

android {
    compileSdk = libs.versions.android.target.sdk.get().toInt()
    defaultConfig {
        namespace = "com.eyeo.privacy.ui"
        minSdk = libs.versions.android.min.sdk.get().toInt()
        vectorDrawables.useSupportLibrary = true
        multiDexEnabled = true
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        consumerProguardFiles("consumer-rules.pro")
    }

    buildFeatures {
        buildConfig = false
        viewBinding = true
    }

    testOptions {
        animationsDisabled = true
    }

    packaging {
        resources.excludes.add("META-INF/LICENSE*")
        resources.excludes.add("META-INF/*.kotlin_module")
        resources.excludes.add("META-INF/AL2.0")
        resources.excludes.add("META-INF/LGPL2.1")
        resources.excludes.add("META-INF/licenses/ASM")
        jniLibs.excludes.add("win32-x86-64/*")
        jniLibs.excludes.add("win32-x86/*")
    }

    kotlinOptions.jvmTarget = JavaVersion.VERSION_1_8.toString()

}

dependencies {
    api(project(":core"))
    implementation(libs.kotlin.stdlib)
    implementation(libs.androidx.appcompat)
    implementation(libs.androidx.constraintlayout)
    implementation(libs.androidx.lifecycle)
    implementation(libs.androidx.preference.ktx)
    implementation(libs.google.flexbox)
    implementation(libs.google.material)

    androidTestImplementation(libs.pat.test)
    androidTestImplementation(libs.androidx.multidex)
}

val interestsPath = "res/raw/data_interests"

val connectedAndroidTest = "connectedAndroidTest"
val deviceScreenShotsFolder = "/sdcard/Android/data/com.eyeo.privacy.ui.test/files/Download/screenshots"
val cleanScreenShotsTask = tasks.register<Exec>("cleanScreenShots") {
    commandLine("adb", "shell", "rm", "-r", deviceScreenShotsFolder)
    isIgnoreExitValue = true
}

tasks.register("updateScreenShots") {
    dependsOn("connectedAndroidTest", cleanScreenShotsTask)
    doLast {
        val outFolder = rootProject.file("docs/assets")
        File(outFolder, "screenshots").deleteRecursively()
        exec {
            commandLine("adb", "pull", deviceScreenShotsFolder)
            workingDir = outFolder
        }
    }
}

afterEvaluate {
    tasks.getByName(connectedAndroidTest).dependsOn(cleanScreenShotsTask)
}
