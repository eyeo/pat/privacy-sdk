package com.eyeo.privacy.ui.analytics

import com.eyeo.privacy.analytics.Event


object UiEvent {
//tag::eventIds[]
    /**
     * PrivacyShieldSettingsActivity events
     */
    object SettingsKeys {
        /**
         * Privacy Shield settings activity has been opened
         */
        const val OPEN_SCREEN = "settings_open_screen"
    }

    object TermsAndConditionsKeys {
        /**
         * Privacy Shield accept terms dialog has been opened
         */
        const val OPEN_DIALOG = "terms_and_conditions_open_dialog"

        /**
         * Privacy Shield terms and conditions state has been changed
         * Event value type is TermsAndConditionsState enum
         */
        const val CHANGE_TNC_STATE = "terms_and_conditions_state_changed"
    }

    /**
     * PrivacyShieldPrivacyFragment events
     */
    object PrivacySettingsKeys {

        /**
         * Privacy settings fragment has been opened
         */
        const val OPEN_SCREEN = "privacy_settings_open_screen"

        /**
         * Toggle global privacy protection
         * Event value type is boolean
         */
        const val TOGGLE_FEATURE = "privacy_settings_toggle_feature"

        /**
         * Toggle trackers blocking feature
         * Event value type is boolean
         */
        const val TOGGLE_BLOCK_TRACKERS =
            "privacy_settings_toggle_block_trackers"

        /**
         * Toggle third party blocking feature
         * Event value type is boolean
         */
        const val TOGGLE_BLOCK_THIRD_PARTY_COOKIES =
            "privacy_settings_toggle_block_third_party_cookies"

        /**
         * Select third party cookies filter level
         * Event value type is FilterLevel
         */
        const val SELECT_COOKIES_FILTER_LEVEL = "privacy_settings_select_cookies_filter_level"

        /**
         * Toggle hide cookie consent popups option
         * Event value type is boolean
         */
        const val TOGGLE_HIDE_COOKIE_CONSENT_POPUPS = "privacy_settings_hide_cookie_consent_popups"

        /**
         * Toggle hide referrer header option
         * Event value type is boolean
         */
        const val TOGGLE_HIDE_REFERRER_HEADER = "privacy_settings_toggle_hide_referrer_header"

        /**
         * Toggle do not track signal
         * Event value type is boolean
         */
        const val TOGGLE_DO_NOT_TRACK = "privacy_settings_toggle_do_not_track"

        /**
         * Toggle GCP signal
         * Event value type is boolean
         */
        const val TOGGLE_ENABLE_GPC = "privacy_settings_toggle_enable_gcp"

        /**
         * Toggle cname cloaking protection
         * Event value type is boolean
         */
        const val TOGGLE_CNAME_CLOAKING_PROTECTION =
            "privacy_settings_toggle_cname_cloaking_protection"

        /**
         * Toggle remove marketing tracking parameters option
         * Event value type is boolean
         */
        const val TOGGLE_REMOVE_MARKETING_TRACKING_PARAMETERS =
            "privacy_settings_toggle_remove_marketing_tracking_parameters"

        /**
         * Toggle block hyperlink auditing option
         * Event value type is boolean
         */
        const val TOGGLE_BLOCK_HYPERLINK_AUDITING =
            "privacy_settings_toggle_block_hyperlink_auditing"

        /**
         * Toggle fingerprinting protection
         * Event value type is boolean
         */
        const val TOGGLE_ANTI_FINGERPRINTING =
            "privacy_settings_toggle_anti_fingerprinting"

        /**
         * Toggle the blocking of social media icons
         * Event value type is boolean
         */
        const val TOGGLE_BLOCK_SOCIAL_MEDIA_ICONS =
            "privacy_settings_toggle_block_social_media_icons"

        /**
         * Toggle the De-AMP feature
         * Event value type is boolean
         */
        const val TOGGLE_DE_AMP = "privacy_settings_toggle_de_amp"
    }

    /**
     * Privacy Shield Popup events
     */
    object PopupKeys {
        /**
         * Popup has been opened
         */
        const val OPEN_SCREEN = "popup_open_screen"

        /**
         * Settings button has been pressed
         */
        const val PRESS_SETTINGS = "popup_press_settings"

        /**
         * Feedback button has been pressed
         */
        const val PRESS_FEEDBACK = "popup_press_feedback"
    }
//end::eventIds[]

    val Event.Companion.Settings
        get() = SettingsKeys
    val Event.Companion.TermsAndConditions
        get() = TermsAndConditionsKeys
    val Event.Companion.PrivacySettings
        get() = PrivacySettingsKeys
    val Event.Companion.Popup
        get() = PopupKeys
}