package com.eyeo.privacy

@Suppress("unused")
interface PrivacyShieldProvider {

    val privacyShield: PrivacyShieldCore?
        get() = testInstance ?: if (PrivacyShieldCore.isInitialized()) PrivacyShieldCore.get() else null
    val requirePrivacyShield: PrivacyShieldCore
        get() = testInstance ?: PrivacyShieldCore.get()

    val privacy
        get() = privacyShield?.privacy()
    val requirePrivacy
        get() = requirePrivacyShield.privacy()

    val stats
        get() = privacyShield?.stats()
    val requireStats
        get() = requirePrivacyShield.stats()

    val analytics
        get() = privacyShield?.analytics()
    val requireAnalytics
        get() = requirePrivacyShield.analytics()

    companion object {
        /**
         * Visible for test only
         */
        var testInstance: PrivacyShieldCore? = null
    }

}