package com.eyeo.privacy.webview

object WebTestUtils {

    fun <T> retryTest(count: Int = 2, block: () -> T): T {
        return try {
            block.invoke()
        } catch (e: Throwable) {
            if (count <= 0) {
                throw e
            } else {
                retryTest(count - 1, block)
            }
        }
    }

}