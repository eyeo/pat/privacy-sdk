package com.eyeo.privacy.service

import com.eyeo.pat.models.RequestType
import com.eyeo.pat.utils.UrlExtension.toUrl
import com.eyeo.privacy.models.PrivacyShieldHeaders
import io.ktor.http.*
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.every
import io.mockk.impl.annotations.RelaxedMockK
import kotlinx.coroutines.runBlocking
import com.eyeo.privacy.models.PrivacySettings
import org.junit.Before
import kotlin.test.Test
import kotlin.test.assertEquals

class HeaderServiceTests {

    private lateinit var headerService: HeaderService

    @RelaxedMockK
    private lateinit var privacyService: PrivacyService

    @RelaxedMockK
    private lateinit var domainService: DomainService

    @RelaxedMockK
    private lateinit var statsService: StatsService

    lateinit var settings: PrivacySettings

    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxUnitFun = true)
        settings = PrivacySettings()

        headerService = HeaderService(
            privacyService,
            statsService,
            domainService,
            false
        )
    }

    @ExperimentalStdlibApi
    @Test
    fun testFilterSendReferrer() {
        coEvery { privacyService.getSettings() }.returns(
            settings.copy(
                enabled = true,
                hideReferrerHeader = true
            )
        )
        coEvery { privacyService.isProtectionEnabled(any()) }.answers { true }

        val docUrl = Url("https://www.google.fr/")
        val reqUrl = Url("https://crumbs.org")

        every {
            domainService.isThirdPartyRequest(
                match { it.host == docUrl.host },
                match { it.host == reqUrl.host },
                any()
            )
        }.answers { true }

        val headers = buildList {
            add(PrivacyShieldHeaders.Referrer to "${docUrl}test?param=1")
        }

        var result = headerService.filterSendHeaders(
            "",
            docUrl,
            reqUrl,
            RequestType.MAIN_FRAME,
            thirdPartyRequest = true,
            headers = headers
        )
        assertEquals(
            docUrl,
            Url(result.find { it.first == PrivacyShieldHeaders.Referrer }?.second!!),
            "Referrer has not been clip"
        )

        result = headerService.filterSendHeaders(
            "",
            docUrl,
            Url("http://crumbs.org"),
            RequestType.MAIN_FRAME,
            thirdPartyRequest = true,
            headers = headers
        )
        assertEquals(
            null,
            result.find { it.first == PrivacyShieldHeaders.Referrer },
            "Referrer has not been removed"
        )

        coEvery { privacyService.getSettings() }.returns(
            settings.copy(
                enabled = true,
                hideReferrerHeader = false
            )
        )
        result = headerService.filterSendHeaders(
            "",
            docUrl,
            reqUrl,
            RequestType.MAIN_FRAME,
            thirdPartyRequest = true,
            headers = headers
        )
        assertEquals(
            headers.first().second,
            result.find { it.first == PrivacyShieldHeaders.Referrer }?.second,
            "Referrer has been removed or modify"
        )
    }

    @Test
    fun testReferrerHeader() {
        val baseUrl = "https://www.google.fr/"
        val url = "${baseUrl}test?param=1"
        ReferrerHeader(url).apply {
            assertEquals(url, getValue())
            hidePath = true
            assertEquals(baseUrl, getValue())
        }

        val baseUrlWithoutSlash = "http://example.fr"
        val urlWithoutSlash = "$baseUrlWithoutSlash?test=ok"
        ReferrerHeader(urlWithoutSlash).apply {
            assertEquals(urlWithoutSlash, getValue())
            hidePath = true
            assertEquals(baseUrlWithoutSlash, getValue())
        }
    }

    @Test
    fun testMainDocumentReferrer() {

        coEvery { privacyService.getSettings() }.returns(
            settings.copy(
                enabled = true,
                hideReferrerHeader = true,
            )
        )
        coEvery { privacyService.isProtectionEnabled(any()) }.answers { true }

        val baseReferrerUrl = "https://www.amazon.com/"
        val referrerUrl = "${baseReferrerUrl}path/to/hide".toUrl()!!
        val pageUrl = "https://www.google.fr/".toUrl()!!

        every { domainService.isThirdPartyRequest(referrerUrl, pageUrl, any()) }.answers { true }

        val headers = listOf(
            PrivacyShieldHeaders.Referrer to referrerUrl.toString()
        )
        val newHeaders = headerService.filterSendHeaders(
            "0",
            pageUrl,
            pageUrl,
            RequestType.MAIN_FRAME,
            thirdPartyRequest = false,
            headers = headers
        )

        assertEquals(PrivacyShieldHeaders.Referrer, newHeaders.first().first)
        assertEquals(baseReferrerUrl, newHeaders.first().second)

    }


    @ExperimentalStdlibApi
    @Test
    fun testReduceFingerprintHttpHeaders() = runBlocking {
        coEvery { privacyService.getSettings() }.returns(
            settings.copy(
                enabled = true,
                enableFingerprintShield = true,
            )
        )
        coEvery { privacyService.isProtectionEnabled(any()) }.answers { true }

        val headers = buildList {
            PrivacyShieldHeaders.HttpHeadersToReduceFingerprint.forEach {
                add(it to "")
            }
        }

        val result = headerService.filterSendHeaders(
            "",
            Url("https://www.google.fr/"),
            Url("https://crumbs.org"),
            RequestType.MAIN_FRAME,
            true,
            headers = headers
        )
        assert(
            result.map { it.first }.intersect(PrivacyShieldHeaders.HttpHeadersToReduceFingerprint)
                .isEmpty()
        )
    }
}