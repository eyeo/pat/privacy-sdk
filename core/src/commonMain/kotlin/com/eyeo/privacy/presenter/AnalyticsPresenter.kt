package com.eyeo.privacy.presenter

import com.eyeo.privacy.analytics.Event
import com.eyeo.privacy.analytics.EventListener
import kotlin.js.JsExport

@JsExport
class AnalyticsPresenter {

    private val eventListeners = arrayListOf<EventListener>()
    fun addEventListener(listener: EventListener) {
        eventListeners.add(listener)
    }

    fun removeEventListener(listener: EventListener): Boolean {
        return eventListeners.remove(listener)
    }

    fun notifyEvent(id: String, value: Any? = null) {
        val event = Event(id, value)
        eventListeners.iterator().run {
            while (hasNext()) {
                next().onEvent(event)
            }
        }
    }
}