package com.eyeo.privacy.ui.view

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import com.eyeo.pat.PatLogger
import com.eyeo.pat.concurrent.Cancellable
import com.eyeo.privacy.PrivacyShieldProvider
import com.eyeo.privacy.analytics.Event
import com.eyeo.privacy.ui.PrivacyShieldCoreExtension.ui
import com.eyeo.privacy.ui.R
import com.eyeo.privacy.ui.analytics.UiEvent.Popup
import com.eyeo.privacy.ui.databinding.PrivacyShieldViewPopupBinding

@SuppressLint("ViewConstructor")
class PrivacyShieldPopupView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
    logoForegroundResId: Int = R.drawable.ps_icon_foreground,
    logoBackgroundResId: Int = R.drawable.ps_icon_background,
    titleResId: Int? = R.string.ps_privacy_title
) : FrameLayout(context, attrs, defStyleAttr), PrivacyShieldProvider,
    PrivacyShieldUIContext.UIContextListener,
    PrivacyShieldUIContext.UIContextAwareImpl {

    private val binding: PrivacyShieldViewPopupBinding
    private var navListener: NavigationListener? = null
    private lateinit var clickListener: ClickListener
    private var statsJob: Cancellable? = null
    override var uiContext: PrivacyShieldUIContext = PrivacyShieldUIContext.Default

    init {
        binding = PrivacyShieldViewPopupBinding.inflate(LayoutInflater.from(context), this, true)
        binding.psLogoForegroundIv.setImageResource(logoForegroundResId)
        binding.psLogoBackgroundIv.setImageResource(logoBackgroundResId)
        titleResId?.let {
            binding.psLogoTv.visibility = VISIBLE
            binding.psLogoTv.text = context.getString(it)
        } ?: { binding.psLogoTv.visibility = GONE }
        binding.psDialogIssue.setOnClickListener {
            requirePrivacyShield.ui().openLink(
                context,
                requirePrivacyShield.ui().createFeedbackFormUrl(context, uiContext.getCurrentUrl()),
            )
            navListener?.onLinkOpen()
            requireAnalytics.notifyEvent(Event.Popup.PRESS_FEEDBACK)
        }
        binding.psDialogSettings.setOnClickListener {
            clickListener.onSettingsButtonClick()
            navListener?.onLinkOpen()
            requireAnalytics.notifyEvent(Event.Popup.PRESS_SETTINGS)
        }
    }

    override fun getContextAwareChildren() = listOf<PrivacyShieldUIContext.UIContextAware>(
        binding.psBoardStats,
        binding.psBoardBottom,
        binding.psBoardPlayBtn,
        binding.psBoardOnOffContainer,
        binding.psBoardOnOffCard
    )

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        uiContext.registerListener(this)
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        uiContext.unregisterListener(this)
        this.statsJob?.cancel()
    }

    override fun onTabChanged(tabId: String, documentUrl: String) {
        this.statsJob?.cancel()
        this.statsJob = stats?.listen(tabId) {
            binding.psBoardStats.setStats(it)
        }
    }

    override fun onProtectionUpdated(status: PrivacyShieldUIContext.ProtectionStatus) {
        val text = status.getLabel(context)
        binding.psStatsOnOffTitle.text =
            context.getString(R.string.ps_tracking_protection_is, text)
    }

    fun setNavigationListener(listener: NavigationListener) {
        this.navListener = listener
    }

    fun setClickListener(listener: ClickListener) {
        this.clickListener = listener
    }

    interface NavigationListener {
        fun onLinkOpen()
    }

    interface ClickListener {
        fun onSettingsButtonClick()
    }
    companion object {
        private val logger = PatLogger("PrivacyShieldPopupView")
    }

}