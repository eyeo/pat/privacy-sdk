package com.eyeo.privacy.service

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.net.ConnectivityManager
import android.net.ConnectivityManager.NetworkCallback
import android.net.LinkProperties
import android.net.Network
import android.net.NetworkRequest
import android.os.Build
import com.eyeo.pat.PatLogger
import com.eyeo.pat.utils.ContextExtension.hasPermission
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.IOException
import java.io.InputStreamReader
import java.io.LineNumberReader
import java.lang.reflect.Method
import java.net.DatagramPacket
import java.net.DatagramSocket
import java.net.InetAddress

class AndroidDnsClient(context: Context) : DomainService.DnsClient {

    private var dnsResolver: DNSResolver = DNSResolver(context)

    override fun isAvailable(): Boolean {
        return true
    }

    override suspend fun performDnsRequest(requestData: ByteArray): ByteArray {
        return dnsResolver.getBestDNS()?.let { inetAddress ->
            udpCommunicate(inetAddress, requestData)
        } ?: throw IllegalStateException("No dns configured")
    }

    override suspend fun setup() {
        dnsResolver.setup()
    }

    @Suppress("BlockingMethodInNonBlockingContext")
    @Throws(IOException::class)
    private suspend fun udpCommunicate(inetAddress: InetAddress, question: ByteArray): ByteArray {
        return withContext(Dispatchers.IO) {
            var socket: DatagramSocket? = null
            try {
                socket = DatagramSocket()
                var packet = DatagramPacket(
                    question, question.size,
                    inetAddress, 53
                )
                socket.soTimeout = SOCKET_TIMEOUT
                socket.send(packet)
                packet = DatagramPacket(ByteArray(BUFFER_SIZE), BUFFER_SIZE)
                socket.receive(packet)
                packet.data
            } finally {
                socket?.close()
            }
        }
    }

    companion object {
        private const val SOCKET_TIMEOUT = 2 * 1000
        private const val BUFFER_SIZE = 1500
    }

    private class DNSResolver constructor(private val context: Context) {
        private val dnsServers = arrayListOf<InetAddress>()

        suspend fun setup() {
            withContext(Dispatchers.IO) {
                loadDefaultDns()
                loadDNSByReflection() || loadDNSByCommand() || listenDNS(context)
            }
        }

        fun getBestDNS(): InetAddress? {
            return dnsServers.lastOrNull()
        }

        private fun listenDNS(context: Context): Boolean {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP &&
                context.hasPermission(Manifest.permission.ACCESS_NETWORK_STATE)
            ) {
                try {
                    val connectivityManager =
                        context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                    val builder = NetworkRequest.Builder()
                    connectivityManager.registerNetworkCallback(builder.build(),
                        object : NetworkCallback() {
                            override fun onLinkPropertiesChanged(
                                network: Network,
                                linkProperties: LinkProperties
                            ) {
                                linkProperties.dnsServers.forEach {
                                    addServer(it)
                                }
                            }
                        })
                    return true
                } catch (e: Exception) {
                    // @see https://issuetracker.google.com/issues/175055271
                    PatLogger.w(TAG, "can't listen dns config", e)
                }
            }
            return false
        }

        private fun loadDNSByCommand(): Boolean {
            var success = false
            try {
                val process = Runtime.getRuntime().exec("getprop")
                val inputStream = process.inputStream
                val lnr = LineNumberReader(
                    InputStreamReader(inputStream)
                )
                var readLine: String?
                while (lnr.readLine().also { readLine = it } != null) {
                    val line = readLine!!

                    val split = line.indexOf("]: [")
                    if (split <= 1 || line.length - 1 <= split + 4) {
                        continue
                    }
                    val property = line.substring(1, split)
                    val value = line.substring(split + 4, line.length - 1)
                    if (property.endsWith(".dns") || property.endsWith(".dns1") ||
                        property.endsWith(".dns2") || property.endsWith(".dns3") ||
                        property.endsWith(".dns4")
                    ) {
                        addServer(InetAddress.getByName(value))
                        success = true
                    }
                }
            } catch (e: IOException) {
                PatLogger.w(TAG, "can't retrieve dns by command", e)
            }
            return success
        }

        @SuppressLint("PrivateApi")
        private fun loadDNSByReflection(): Boolean {
            var success = false
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
                try {
                    val cSystemProperties = Class.forName("android.os.SystemProperties")
                    val method: Method = cSystemProperties.getMethod(
                        "get",
                        String::class.java
                    )
                    for (propKey in arrayOf(
                        "net.dns1", "net.dns2", "net.dns3", "net.dns4"
                    )) {
                        val value = method.invoke(null, propKey) as String?
                        value?.takeIf { value.isNotEmpty() }
                            ?.let { InetAddress.getByName(value) }
                            ?.let {
                                addServer(it)
                                success = true
                            }
                    }
                } catch (e: Exception) {
                    PatLogger.w(TAG, "can't retrieve dns by reflection", e)
                }
            }
            return success
        }

        private fun loadDefaultDns() {
            try {
                addServer(InetAddress.getByName(DEFAULT_DNS))
            } catch (e: Exception) {
                PatLogger.w(TAG, "can't load default dns", e)
            }
        }

        private fun addServer(server: InetAddress) =
            dnsServers.find { it.hostAddress == server.hostAddress } ?: dnsServers.add(server)


        companion object {
            private const val TAG = "DNSResolver"
            private const val DEFAULT_DNS = "1.1.1.1"
        }
    }
}