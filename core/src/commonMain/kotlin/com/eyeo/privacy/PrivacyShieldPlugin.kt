package com.eyeo.privacy

import com.eyeo.pat.browser.BrowserPluginApi
import com.eyeo.pat.browser.plugins.*
import com.eyeo.pat.models.RequestType
import com.eyeo.pat.utils.UrlExtension.safeBuild
import com.eyeo.privacy.service.DomainService
import com.eyeo.privacy.service.HeaderService
import com.eyeo.privacy.service.PrivacyService
import com.eyeo.privacy.service.StatsService
import io.ktor.http.*
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import com.eyeo.privacy.service.*
import kotlin.coroutines.coroutineContext

@Suppress("unused")
class PrivacyShieldPlugin internal constructor(
    private val privacyService: PrivacyService,
    private val statsService: StatsService,
    private val headerService: HeaderService,
    private val domainService: DomainService,
    private val cmpService: CmpService,
    private val privacyShieldOptions: PrivacyShieldOptions,
) : HeadersPlugin, TabPlugin, RequestPlugin {


    private var privacyServiceJsBuilder: JavascriptPlugin? = null
    private var cmpServiceJsBuilder: JavascriptPlugin? = null

    suspend fun setup() {
        BrowserPluginApi.Tab.registerPlugin(this)
        BrowserPluginApi.Request.registerPlugin(this)
        BrowserPluginApi.Headers.registerPlugin(this)
        privacyServiceJsBuilder = privacyService.createJsBuilder(privacyShieldOptions.fingerprintShieldScript).also {
            BrowserPluginApi.Javascript.registerPlugin(it)
        }
        cmpServiceJsBuilder = cmpService.createCmpJsBuilder().also {
            BrowserPluginApi.Javascript.registerPlugin(it)
        }

        privacyService.listenSettings().map {
            val protectionEnabled = it.enabled
            listOfNotNull(
                if (protectionEnabled && it.blockTrackers) PrivacyShieldCore.PRIVACY_FILTER_LIST else null,
                if (protectionEnabled && it.blockTrackers) PrivacyShieldCore.PRIVACY_FRIENDLY_LIST else null,
                if (protectionEnabled && it.hideCookieConsentPopups) PrivacyShieldCore.COOKIE_HIDING_FILTER_LIST else null,
                if (protectionEnabled && it.blockSocialMediaIconsTracking) PrivacyShieldCore.SOCIAL_FILTER_LIST else null,
            )
        }
            .distinctUntilChanged()
            .onEach {
                BrowserPluginApi.FilterEngine.setSubscriptions("privacy_shield", it)
            }.launchIn(CoroutineScope(coroutineContext))

        privacyService.listenSettings()
            .map {
                if (it.enabled) it.allowedDomains.plus(it.allowedDomainsThisSession) else emptyList()
            }.distinctUntilChanged()
            .onEach {
                BrowserPluginApi.FilterEngine.setAllowedDomains("privacy_shield", it.toList())

            }.launchIn(CoroutineScope(coroutineContext))
    }

    fun destroy() {
        BrowserPluginApi.Tab.unregisterPlugin(this)
        BrowserPluginApi.Request.unregisterPlugin(this)
        BrowserPluginApi.Headers.unregisterPlugin(this)
        privacyServiceJsBuilder?.let {
            BrowserPluginApi.Javascript.unregisterPlugin(it)
            privacyServiceJsBuilder = null
        }
        cmpServiceJsBuilder?.let {
            BrowserPluginApi.Javascript.unregisterPlugin(it)
            cmpServiceJsBuilder = null
        }
    }

    /**
     * notify the engine that an event happened on a tab
     *
     * @param event the event
     */
    override fun onTabEvent(event: TabPlugin.Event) {
        event.apply {
            when (type) {
                TabPlugin.Type.Loading -> {
                    statsService.onPageChange(tabId)
                }

                TabPlugin.Type.ResourceBlocked -> {
                    if (PrivacyShieldCore.TRACKERS_SUBSCRIPTIONS.contains(filterListUrl))
                        statsService.onTrackerBlocked(tabId)
                    else if (PrivacyShieldCore.POPUPS_SUBSCRIPTIONS.contains(filterListUrl)) {
                        statsService.onPopupsBlocked(tabId)
                    }
                }

                else -> {}
            }
        }
    }

    /**
     * returns the rewritten request headers if filtering was needed
     *
     * @param tabId concerned tab id
     * @param documentUrl document url
     * @param requestUrl request url
     * @param requestType type of request
     * @param incognito the incognito state of the tab
     * @param headers request headers
     * @return headers the rewritten request headers
     */
    override fun rewriteRequestHeaders(
        tabId: String,
        documentUrl: Url,
        requestUrl: Url,
        requestType: RequestType,
        incognito: Boolean,
        headers: List<Pair<String, String>>
    ): List<Pair<String, String>> {
        val checkAlias = privacyService.getSettings().enableCNameCloakingProtection
        val thirdPartyRequest =
            domainService.isThirdPartyRequest(documentUrl, requestUrl, checkAlias)
        return headerService.filterSendHeaders(
            tabId = tabId,
            docUrl = documentUrl,
            reqUrl = requestUrl,
            requestType = requestType,
            thirdPartyRequest = thirdPartyRequest,
            headers = headers
        )
    }

    /**
     * returns the rewritten response headers if filtering was needed
     *
     * @param tabId concerned tab id
     * @param documentUrl document url
     * @param requestUrl request url
     * @param requestType type of request
     * @param incognito the incognito state of the tab
     * @param headers request headers
     * @return headers the rewritten response headers
     */
    override fun rewriteResponseHeaders(
        tabId: String,
        documentUrl: Url,
        requestUrl: Url,
        requestType: RequestType,
        incognito: Boolean,
        headers: List<Pair<String, String>>
    ): List<Pair<String, String>> {

        val checkAlias = privacyService.getSettings().enableCNameCloakingProtection
        val thirdPartyRequest =
            domainService.isThirdPartyRequest(documentUrl, requestUrl, checkAlias)
        return headerService.filterReceivedHeaders(
            tabId = tabId,
            docUrl = documentUrl,
            requestType = requestType,
            thirdPartyRequest = thirdPartyRequest,
            headers = headers
        )
    }


    /**
     * replace host by cname alias
     *
     * @param documentUrl main document url
     * @param requestUrl request url
     */
    override suspend fun rewriteFilteredUrl(documentUrl: Url, requestUrl: Url): Url {
        return if (!privacyService.isProtectionEnabled(documentUrl) ||
            !privacyService.getSettings().enableCNameCloakingProtection
        ) {
            requestUrl
        } else {
            domainService.retrieveHostAlias(requestUrl.host)?.let { host ->
                URLBuilder(requestUrl).apply {
                    this.host = host
                }.build()
            } ?: requestUrl
        }
    }

    /**
     * rewrite url to remove tracking data in it
     *
     * @param documentUrl document url
     * @param requestUrl url to rewrite
     * @return rewrite url (can be unchanged)
     */
    override fun rewriteRequestUrl(documentUrl: Url, requestUrl: Url): Url {
        var reqUrlBuilder = URLBuilder(requestUrl)

        if (privacyService.isProtectionEnabled(documentUrl)) {
            reqUrlBuilder = reqUrlBuilder
                .let { privacyService.removeTrackingParameters(it) }
                .let { privacyService.ampToCanonical(it) }
        }

        return reqUrlBuilder.safeBuild() ?: requestUrl
    }

    /**
     * check if a request should be blocked
     *
     * @param tabId concerned tab id
     * @param documentUrl document url
     * @param requestUrl request url
     * @param requestType type of request
     * @param headers request headers
     * @return true if the request should be blocked
     */
    override fun shouldBlockRequest(
        tabId: String,
        documentUrl: Url,
        requestUrl: Url,
        requestType: RequestType,
        headers: List<Pair<String, String>>
    ): Boolean {
        val blockRequest =
            privacyService.shouldBlockHyperlinkAuditing(documentUrl, requestType, headers)
        if (blockRequest) {
            statsService.onTrackerBlocked(tabId)
        }
        return blockRequest
    }

    /**
     * check if document.referrer should be cleared
     *
     * @param documentUrl document url
     * @param referrerUrl request url
     * @param isMainFrame type of request is main frame
     */
    override fun shouldClearReferrer(
        documentUrl: Url,
        referrerUrl: Url,
        isMainFrame: Boolean
    ): Boolean {
        return (!isMainFrame
                && privacyService.isProtectionEnabled(documentUrl)
                && privacyService.getSettings().hideReferrerHeader
                && domainService.isThirdPartyRequest(documentUrl, referrerUrl, false))
    }
}