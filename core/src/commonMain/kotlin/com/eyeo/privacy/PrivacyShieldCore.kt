package com.eyeo.privacy

import com.eyeo.pat.provider.MainDomainProvider
import com.eyeo.pat.provider.ResourcesProvider
import com.eyeo.pat.provider.SettingsProvider
import com.eyeo.pat.provider.StorageProvider
import com.eyeo.pat.utils.Coroutines
import com.eyeo.privacy.models.PrivacySettings
import com.eyeo.privacy.presenter.AnalyticsPresenter
import com.eyeo.privacy.presenter.PrivacyPresenter
import com.eyeo.privacy.presenter.StatsPresenter
import com.eyeo.privacy.service.CmpService
import com.eyeo.privacy.service.DomainService
import com.eyeo.privacy.service.HeaderService
import com.eyeo.privacy.service.PrivacyService
import com.eyeo.privacy.service.StatsService
import com.russhwolf.settings.Settings
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext
import kotlin.js.JsExport
import kotlin.jvm.JvmStatic

@JsExport
class PrivacyShieldCore internal constructor(
    settingsStorage: Settings,
    resourcesProvider: ResourcesProvider,
    dnsClient: DomainService.DnsClient,
    privacyShieldOptions: PrivacyShieldOptions
) {

    private val privacy: PrivacyPresenter
    private val stats: StatsPresenter
    private val analytics = AnalyticsPresenter()
    private val privacyShieldPlugin: PrivacyShieldPlugin
    private val storageProvider: StorageProvider
    private val coroutineContext: CoroutineContext = Coroutines.getSingleThreadContext()
    private val jobs: Job

    init {
        storageProvider = StorageProvider(settingsStorage)
        val privacySettings = SettingsProvider(
            PRIVACY_SETTINGS_KEY,
            storageProvider,
            PrivacySettings.serializer(),
            defaultBuilder = { PrivacySettings() }
        )

        val statsService = StatsService(storageProvider)
        val domainService = DomainService(MainDomainProvider(resourcesProvider), dnsClient)

        val privacyService = PrivacyService(
            privacySettings,
            resourcesProvider,
            domainService,
            privacyShieldOptions.supportChips,
            privacyShieldOptions.supportStoragePartitioning
        )

        val headerService =
            HeaderService(
                privacyService,
                statsService,
                domainService,
                privacyShieldOptions.supportChips,
            )

        val cmpService = CmpService(privacySettings, resourcesProvider, statsService, analytics)

        privacy = PrivacyPresenter(privacyService)
        stats = StatsPresenter(statsService)
        privacyShieldPlugin = PrivacyShieldPlugin(
            privacyService,
            statsService,
            headerService,
            domainService,
            cmpService,
            privacyShieldOptions
        )

        jobs = CoroutineScope(coroutineContext).launch {
            privacyShieldPlugin.setup()

            launch { domainService.setup() }
        }
    }

    fun privacy() = privacy

    fun stats() = stats

    fun analytics() = analytics

    val version = CoreBuildConfig.version

    private fun destroy() {
        jobs.cancel()
        privacyShieldPlugin.destroy()
    }

    companion object {

        private var instance: PrivacyShieldCore? = null

        /**
         * visible for test only
         */
        internal fun setTestInstance(testInstance: PrivacyShieldCore?) {
            instance = testInstance
        }

        @Suppress("NON_EXPORTABLE_TYPE")
        fun storageProvider() = get().storageProvider

        fun get(): PrivacyShieldCore {
            instance?.let {
                return it
            }
            throw IllegalStateException("PrivacyShield not initialized ! Setup PrivacyShield before any usage")
        }

        fun isInitialized(): Boolean = instance != null

        @Suppress("NON_EXPORTABLE_TYPE")
        fun createInstance(
            settings: Settings,
            resourcesProvider: ResourcesProvider,
            dnsClient: DomainService.DnsClient,
            privacyShieldOptions: PrivacyShieldOptions,
        ) {
            if (isInitialized()) throw IllegalStateException("Privacy Shield is already initialized")
            instance = PrivacyShieldCore(
                settingsStorage = settings,
                resourcesProvider = resourcesProvider,
                dnsClient = dnsClient,
                privacyShieldOptions = privacyShieldOptions
            )
        }

        fun destroy() {
            instance?.destroy()
            instance = null
        }

        internal const val CRUMBS_WEBSITE = "https://crumbs.org"
        internal const val PRIVACY_SETTINGS_KEY = "ps_privacy_settings"

        const val PRIVACY_FILTER_LIST =
            //tag::filterlist[]
            "https://easylist-downloads.adblockplus.org/easyprivacy.txt"
        //end::filterlist[]

        const val PRIVACY_FRIENDLY_LIST =
            //tag::filterlist[]
            "https://filterlist.crumbs.org/crumbs-privacy-friendly.txt"
        //end::filterlist[]

        const val COOKIE_HIDING_FILTER_LIST =
            //tag::filterlist[]
            "https://fanboy.co.nz/fanboy-cookiemonster.txt"
        //end::filterlist[]

        const val SOCIAL_FILTER_LIST =
            //tag::filterlist[]
            "https://easylist-downloads.adblockplus.org/fanboy-social.txt"
        //end::filterlist[]

        @JvmStatic
        val TRACKERS_SUBSCRIPTIONS =
            arrayOf(
                PRIVACY_FILTER_LIST,
                PRIVACY_FRIENDLY_LIST,
                SOCIAL_FILTER_LIST
            )

        @JvmStatic
        val POPUPS_SUBSCRIPTIONS = arrayOf(
            COOKIE_HIDING_FILTER_LIST,
        )
    }

}