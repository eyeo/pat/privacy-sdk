package com.eyeo.privacy.ui.utils

import androidx.fragment.app.DialogFragment
import com.eyeo.pat.provider.StorageProvider
import com.eyeo.pat.utils.CustomTabOpener
import com.eyeo.pat.utils.LinkOpener
import com.eyeo.privacy.PrivacyShieldCore
import com.eyeo.privacy.TncPreference
import com.eyeo.privacy.analytics.Event
import com.eyeo.privacy.models.TermsAndConditionsState
import com.eyeo.privacy.ui.PrivacyShieldSettingsFragment
import com.eyeo.privacy.ui.analytics.UiEvent.TermsAndConditions

class UIPreferences internal constructor(storageProvider: StorageProvider) {
    internal var linkOpener: LinkOpener? = CustomTabOpener()
    private val tncPreference = TncPreference(storageProvider)

    fun setTermsAndConditionsState(state: TermsAndConditionsState) {
        tncPreference.setTermsAndConditionsState(state)
        PrivacyShieldCore.get().analytics().notifyEvent(Event.TermsAndConditions.CHANGE_TNC_STATE, state)
    }

    fun getTermsAndConditionsState() = tncPreference.getTermsAndConditionsState()

    fun setLinkOpener(opener: LinkOpener?) {
        linkOpener = opener
    }

    /**
     * A custom terms and conditions dialog to be shown instead of the default one when the user tries to
     * enable Privacy Shield inside [PrivacyShieldSettingsFragment].
     */
    @JvmField
    var termsAndConditionsDialog: DialogFragment? = null

}