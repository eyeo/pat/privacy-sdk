package com.eyeo.privacy.service

import com.eyeo.pat.PatLogger
import com.eyeo.pat.provider.MainDomainProvider
import com.eyeo.pat.utils.SystemTimeProvider.Companion.getCurrentTimeMs
import com.eyeo.pat.utils.UrlExtension.isIPAddress
import io.ktor.http.URLBuilder
import io.ktor.http.Url
import io.ktor.utils.io.charsets.Charsets
import io.ktor.utils.io.core.String
import io.ktor.utils.io.core.toByteArray
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlin.collections.set
import kotlin.random.Random

class DomainService internal constructor(
    private val domainProvider: MainDomainProvider,
    private val dnsClient: DnsClient
) {

    internal val cachedAlias = hashMapOf<String, Alias>()

    internal suspend fun setup() {
        domainProvider.setup()
        withContext(Dispatchers.Default) {
            if (dnsClient.isAvailable()) {
                dnsClient.setup()
            }
        }
    }

    suspend fun retrieveHostAlias(host: String): String? {
        if (!dnsClient.isAvailable()) return null
        if (hasValidData(host)) return cachedAlias[host]?.alias
        val result = withContext(Dispatchers.Main.immediate) {
            val id = Random.nextInt(Short.MAX_VALUE.toInt())
            return@withContext try {
                val requestBytes = getMessageBytes(id.toShort(), host)
                val result = dnsClient.performDnsRequest(requestBytes)
                val response = parseDnsRecord(id.toShort(), result)
                val record =
                    response.find { it.type == RecordType.A }?.takeIf { it.source != host }
                val ttl = response.minOfOrNull { it.ttl } ?: RETRY_TTL
                Alias(record?.source, getCurrentTimeMs() + ttl * 1000)
            } catch (e: Exception) {
                PatLogger.w("AndroidDnsClient", "Dns error", e)
                null
            }
        }
        cachedAlias[host] = result ?: Alias(null, getCurrentTimeMs() + RETRY_TTL)
        return cachedAlias[host]?.alias
    }

    fun isThirdPartyRequest(documentUrl: Url, requestUrl: Url, checkAlias: Boolean): Boolean {
        val thirdPartyRequest = isThirdParty(documentUrl, requestUrl)
        if (thirdPartyRequest || !checkAlias) {
            return thirdPartyRequest
        }

        val documentAlias =
            cachedAlias[documentUrl.host]?.alias?.takeIf { it.isNotEmpty() }
                ?.let { URLBuilder(documentUrl).apply { host = it }.build() }
        val requestAlias =
            cachedAlias[requestUrl.host]?.alias?.takeIf { it.isNotEmpty() }
                ?.let { URLBuilder(documentUrl).apply { host = it }.build() }

        return if (documentAlias == null && requestAlias != null) {
            isThirdParty(documentUrl, requestAlias)
        } else if (!isThirdParty(documentUrl, requestAlias ?: requestUrl)) {
            false
        } else {
            isThirdParty(documentAlias ?: documentUrl, requestAlias ?: requestUrl)
        }

    }

    private fun isThirdParty(documentUrl: Url, requestUrl: Url): Boolean {
        if (documentUrl.host == requestUrl.host)
            return false

        if (documentUrl.isIPAddress() || requestUrl.isIPAddress())
            return true


        return domainProvider.getMainDomain(documentUrl) != domainProvider.getMainDomain(requestUrl)
    }

    internal fun getMainDomain(url: String?) = domainProvider.getMainDomain(url)
    internal fun getMainDomain(url: Url) = domainProvider.getMainDomain(url)

    private fun hasValidData(domain: String): Boolean {
        val alias = cachedAlias[domain]
        return (alias != null && alias.ttlEnd > getCurrentTimeMs())
    }

    interface DnsClient {
        fun isAvailable(): Boolean
        suspend fun performDnsRequest(requestData: ByteArray): ByteArray
        suspend fun setup() {}
    }

    internal enum class RecordType {
        CNAME,
        A
    }

    internal data class DnsRecord(
        val type: RecordType,
        val source: String,
        val value: String,
        val ttl: Int,
    )

    internal data class Alias(val alias: String?, val ttlEnd: Long)

    private class DnsRequest {

        private var data = byteArrayOf()

        fun writeByte(value: Int) {
            data += toByte(value)
        }

        fun toByte(value: Int) = (value and 0xff).toByte()

        fun writeShort(value: Int) {
            data += byteArrayOf(
                toByte(value shr 8),
                toByte(value),
            )
        }

        fun write(value: ByteArray) {
            data += value
        }

        fun writeDomain(domain: String) {
            for (s in domain.split(".").toTypedArray()) {
                val byteArray: ByteArray = s.toByteArray(charset = Charsets.UTF_8)
                writeByte(byteArray.size)
                write(byteArray)
            }
            writeByte(0)
        }

        fun getBytes() = data
    }

    private class DNSResponse constructor(private val data: ByteArray) {

        private var position = 0

        fun readByte(): Int {
            return data[position++].toInt() and 0xff
        }

        fun readShort(): Int {
            return readByte() shl 8 or readByte()
        }

        fun readInt(): Int {
            return (readByte() shl 24
                    or (readByte() shl 16)
                    or (readByte() shl 8)
                    or readByte())
        }

        fun readByteString(length: Int): String {
            val string = String(data, position, length, charset = Charsets.UTF_8)
            position += length
            return string
        }

        fun newDataView(): DNSResponse {
            return DNSResponse(data)
        }

        fun moveTo(pos: Int) {
            position = pos
        }

        fun skip(offset: Int) {
            position += offset
        }

        fun readName(previousOffset: ArrayList<Int> = arrayListOf()): String {
            val size = readByte()
            if (size and POINTER_BYTE == POINTER_BYTE) {
                val offset = size and POINTER_MASK shl 8 or readByte()
                if (previousOffset.contains(offset)) {
                    throw IllegalStateException("Cyclic offsets detected.")
                }
                val buffer = this.newDataView()
                buffer.moveTo(offset)
                previousOffset.add(offset)
                return buffer.readName(previousOffset)
            }
            if (size == 0) {
                return ""
            }
            val content = readByteString(size)
            val nextContent = readName(previousOffset)
            if (nextContent.isEmpty())
                return content
            return "$content.$nextContent"
        }

        fun readAnswers(count: Int): List<DnsRecord> {
            val result = arrayListOf<DnsRecord>()
            repeat(count) {
                val domain = readName()
                val type = readShort()
                //val dnsClass = readShort()
                skip(2)
                val ttl = readInt()

                val payloadSize = readShort()

                when (type) {
                    1 -> {
                        if (payloadSize == 4) {
                            val ip = "${readByte()}.${readByte()}.${readByte()}.${readByte()}"
                            result.add(DnsRecord(RecordType.A, domain, ip, ttl))
                        }
                    }
                    5 -> {
                        val target = readName()
                        result.add(DnsRecord(RecordType.CNAME, domain, target, ttl))
                    }
                    else -> {
                        skip(payloadSize)
                    }
                }
            }
            return result
        }

        fun readQuestions(count: Int): List<String> {
            val questions = arrayListOf<String>()
            repeat(count) {
                val name = readName()
                questions.add(name)
                // type
                // val qType = readShort()
                skip(2)
                // class
                // val qClass = readShort()
                skip(2)
            }
            return questions
        }

        companion object {
            private const val POINTER_BYTE = 0xc0
            private const val POINTER_MASK = 0x3f
        }
    }

    companion object {

        const val RETRY_TTL = 60 * 4

        private const val PUBLIC_SUFFIX_LIST_FILE = "data/public_suffix_list.dat"


        @Throws(IllegalStateException::class)
        internal fun parseDnsRecord(id: Short, bytes: ByteArray): List<DnsRecord> {
            DNSResponse(bytes).let { buffer ->

                val answerId = buffer.readShort()
                if (answerId != id.toInt()) {
                    throw IllegalStateException("Id doesn't match")
                }
                val header = buffer.readShort()
                val recursionDesired = (header shr 8) and 1 == 1
                val recursionAvailable = (header shr 7) and 1 == 1
                val resultCode = header and 0xF
                if (!(recursionAvailable && recursionDesired)) {
                    throw IllegalStateException("the dns server cant support recursion ")
                }

                when (resultCode) {
                    1 -> "Format error - The name server was unable to interpret the query."
                    2 -> "Server failure - The name server was unable to process this query due to a problem with the name server."
                    3 -> "Name Error - Meaningful only for responses from an authoritative name server, this code signifies that the domain name referenced in the query does not exist."
                    4 -> "Not Implemented - The name server does not support the requested kind of query."
                    5 -> "Refused - The name server refuses to perform the specified operation for policy reasons."
                    else -> null
                }?.let {
                    throw IllegalStateException(it)
                }

                val questionCount = buffer.readShort()
                val answerCount = buffer.readShort()

                // nameServer Count
                // val nameServer = buffer.readShort()
                buffer.skip(2)
                // additionalResourceRecordCount
                //val recordCount = buffer.readShort()
                buffer.skip(2)
                // ignore questions
                buffer.readQuestions(questionCount)

                return buffer.readAnswers(answerCount)
            }
        }

        internal fun getMessageBytes(id: Short, domain: String): ByteArray {
            return DnsRequest().apply {
                writeShort(id.toInt())
                // recursionDesired
                writeShort(1 shl 8)
                // questions count
                writeShort(1)
                // no  answer
                writeShort(0)
                // no nameServerRecords
                writeShort(0)
                // no  additionalResourceRecords
                writeShort(0)

                writeDomain(domain)
                // type CNAME
                writeShort(1)
                // class internet
                writeShort(1)
            }.getBytes()
        }
    }

}