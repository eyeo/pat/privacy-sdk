package com.eyeo.privacy.presenter

import com.eyeo.pat.concurrent.Cancellable
import com.eyeo.pat.concurrent.JobWrapper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.flow.cancellable
import kotlinx.coroutines.launch
import com.eyeo.privacy.models.PrivacyShieldStats
import com.eyeo.privacy.models.PrivacyShieldStatsData
import com.eyeo.privacy.service.StatsService
import kotlin.js.JsExport

/**
 * This presenter is the entry point to get statistics
 */
@Suppress("unused")
@JsExport
class StatsPresenter internal constructor(
    private val statsService: StatsService
) {

    fun listen(tabId: String, callback: (PrivacyShieldStats) -> Unit): Cancellable {
        return JobWrapper(MainScope().launch(Dispatchers.Main.immediate) {
            statsService.listenStats(tabId)
                .cancellable()
                .collect {
                    callback(it)
                }
        })
    }

    fun get(tabId: String): PrivacyShieldStats {
        return statsService.getCurrentStats(tabId)
    }

    fun getGlobal(): PrivacyShieldStatsData {
        return statsService.getGlobalStats()
    }

}