package com.eyeo.privacy.webview.demo

import android.app.Application
import com.eyeo.pat.PatLogger
import com.eyeo.pat.PatLoggerLevel
import com.eyeo.pat.browser.BrowserIntegration
import com.eyeo.pat.browser.plugins.HistoryListener
import com.eyeo.pat.browser.plugins.HistoryProvider
import com.eyeo.pat.browser.plugins.Visit
import com.eyeo.privacy.PrivacyShieldAndroid
import com.eyeo.privacy.PrivacyShieldCore
import com.eyeo.privacy.analytics.Event
import com.eyeo.privacy.analytics.EventListener
import com.eyeo.privacy.ui.analytics.UiEvent.PrivacySettings
import timber.log.Timber
import java.util.concurrent.TimeUnit

class DemoApp : Application() {

    override fun onCreate() {
        super.onCreate()

        PatLogger.logLevel = PatLoggerLevel.DEBUG
        Timber.plant(Timber.DebugTree())

        if (!PrivacyShieldAndroid.isInitialized()) {
            //tag::initialize[]
            PrivacyShieldAndroid.setup(applicationContext)
            //end::initialize[]
        }

        //tag::history[]
        BrowserIntegration.History.setHistoryProvider(fakeHistoryProvider())
        //end::history[]

        //tag::eventListener[]
        PrivacyShieldCore.get().analytics().addEventListener(object : EventListener {
            override fun onEvent(event: Event<*>) {
                Timber.d("Privacy Shield event: ${event.id}")
                when (event.id) {
                    Event.PrivacySettings.TOGGLE_FEATURE -> {
                        val isEnabled = event.valueAs<Boolean>()
                        if (isEnabled) {
                            Timber.d("Privacy protection is enabled")
                        }
                    }
                }
            }
        })
        //end::eventListener[]
    }

    private fun fakeHistoryProvider(): HistoryProvider {
        return object : HistoryProvider {
            override fun queryHistory(numberOfDays: Int, callback: HistoryListener) {
                val visits = arrayListOf<Visit>()
                for (i in 0..5) {
                    visits.add(
                        Visit(
                            "https://github.com/dialogflow/dialogflow-android-client/issues/57",
                            TimeUnit.DAYS.toMillis(i.toLong()).toDouble()
                        )
                    )
                }
                //should only count 1
                for (i in 0..5) {
                    visits.add(
                        Visit(
                            "http://www.cotation-immobiliere.fr/aspx/cotations/CotationPriceSearchCriteria.aspx",
                            TimeUnit.MINUTES.toMillis(i.toLong()).toDouble()
                        )
                    )
                }
                callback.onHistoryReady(visits.toTypedArray())
            }

        }

    }

}