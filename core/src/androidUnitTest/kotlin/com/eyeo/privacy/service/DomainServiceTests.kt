package com.eyeo.privacy.service

import com.eyeo.pat.provider.AndroidResourceProvider
import com.eyeo.pat.provider.MainDomainProvider
import io.ktor.http.*
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.RelaxedMockK
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import kotlin.test.Test

class DomainServiceTests {

    private lateinit var domainService: DomainService

    @RelaxedMockK
    internal lateinit var dnsClient: DomainService.DnsClient

    @RelaxedMockK
    internal lateinit var resourceProvider: AndroidResourceProvider

    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxUnitFun = true)

        coEvery {
            resourceProvider.loadResource(any())
        }.returns(publicSuffixList)

        domainService = DomainService(MainDomainProvider(resourceProvider), dnsClient)
        return runBlocking {
            domainService.setup()
        }
    }

    @Test
    fun testIsThirdParty() {
        // test cases : https://webkit.org/blog/11338/cname-cloaking-and-bounce-tracking-defense/

        val docUrl = Url("https://www.blog.com")
        val requestUrl = Url("https://track.blog.com")

        Assert.assertFalse(domainService.isThirdPartyRequest(docUrl, requestUrl, true))

        domainService.cachedAlias[requestUrl.host] = DomainService.Alias("other.blog.com", 0)
        Assert.assertFalse(domainService.isThirdPartyRequest(docUrl, requestUrl, true))

        domainService.cachedAlias[requestUrl.host] = DomainService.Alias("tracker.com", 0)
        Assert.assertTrue(domainService.isThirdPartyRequest(docUrl, requestUrl, true))

        domainService.cachedAlias.clear()
        domainService.cachedAlias[docUrl.host] = DomainService.Alias("abc123.edge.com", 0)
        Assert.assertFalse(domainService.isThirdPartyRequest(docUrl, requestUrl, true))

        domainService.cachedAlias[requestUrl.host] = DomainService.Alias("abc123.edge.com", 0)
        Assert.assertFalse(domainService.isThirdPartyRequest(docUrl, requestUrl, true))

        domainService.cachedAlias[requestUrl.host] = DomainService.Alias("other.blog.com", 0)
        Assert.assertFalse(domainService.isThirdPartyRequest(docUrl, requestUrl, true))

        domainService.cachedAlias[requestUrl.host] = DomainService.Alias("tracker.com", 0)
        Assert.assertTrue(domainService.isThirdPartyRequest(docUrl, requestUrl, true))

        Assert.assertFalse(domainService.isThirdPartyRequest(docUrl, requestUrl, false))

    }

    @Test
    fun testMainDomain() {
        Assert.assertEquals(domainService.getMainDomain("https://www.domain.fr"), "domain.fr")
        Assert.assertEquals(
            domainService.getMainDomain("https://domain.first.second"),
            "domain.first.second"
        )
        Assert.assertEquals(
            domainService.getMainDomain("https://subdomain.domain.first.second"),
            "domain.first.second"
        )
        Assert.assertEquals(
            domainService.getMainDomain("https://subDomain.domAin.FIRST.second"),
            "domain.first.second"
        )
        Assert.assertEquals(
            domainService.getMainDomain("https://subdomain.domain.with.three.parts"),
            "domain.with.three.parts"
        )
        Assert.assertEquals(
            domainService.getMainDomain("https://subdomain.domain.exclude.suffix"),
            "exclude.suffix"
        )
        Assert.assertEquals(
            domainService.getMainDomain("https://domain.any.wildcard.fr"),
            "domain.any.wildcard.fr"
        )
    }

    companion object {
        const val publicSuffixList = """
// fake test list
com
fr
first.second
with.three.parts
!exclude.suffix
*.wildcard.fr
"""
    }
}