package com.eyeo.privacy.service

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class DNSServiceTests {

    @Test
    fun testDNS() = runBlocking {
        val client = AndroidDnsClient(InstrumentationRegistry.getInstrumentation().context)
        client.setup()
        delay(500)
        val requestData = DomainService.getMessageBytes(100, "cname.crumbs-benchmark.sertook.com")
        val result = client.performDnsRequest(requestData)
        val records = DomainService.parseDnsRecord(100, result)

        Assert.assertEquals(
            "tracking.crumbs-benchmark.sertook.com",
            records.find { it.type == DomainService.RecordType.A }?.source
        )
    }

}