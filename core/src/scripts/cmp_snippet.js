if (typeof (PsCmpJsInterface) !== 'undefined') {

  function tcfApiExists() {
    return document.querySelector('iframe[name="__tcfapiLocator"]')
      && typeof window.__tcfapi !== 'undefined';
  }

  function setupListener() {
    try {
      window.__tcfapi('addEventListener', 2, (tcData, success) => {
        if (success && tcData.eventStatus === 'cmpuishown') {
          PsCmpJsInterface.onCmpUiShown();
        }
      });
    } catch(ignore) { }
  }

  function observeIframe() {
    const observer = new MutationObserver(function(mutationsList) {
      for (const mutation of mutationsList) {
          for (let node of mutation.addedNodes) {
              if (node.tagName === 'IFRAME' && node.name === '__tcfapiLocator') {
                  setupListener();
                  observer.disconnect();
              }
          }
      }
    });

    const targetNode = document;
    const config = { childList: true, subtree: true };
    observer.observe(targetNode, config);
  }

  document.addEventListener('DOMContentLoaded', function () {
    if (tcfApiExists()) {
      setupListener();
      return;
    }
    observeIframe();
  });
}