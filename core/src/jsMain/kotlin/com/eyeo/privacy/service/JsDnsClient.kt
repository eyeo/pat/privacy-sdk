package com.eyeo.privacy.service

import com.eyeo.privacy.service.DomainService

class JsDnsClient : DomainService.DnsClient {
    override fun isAvailable(): Boolean {
        return false
    }

    override suspend fun performDnsRequest(requestData: ByteArray): ByteArray {
        throw UnsupportedOperationException("Dns client not available in JS")
    }

    override suspend fun setup() {
        throw UnsupportedOperationException("Dns client not available in JS")
    }
}