package com.eyeo.privacy.webview

import android.content.Intent
import androidx.lifecycle.Lifecycle
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.web.model.Atoms.castOrDie
import androidx.test.espresso.web.model.Atoms.script
import androidx.test.espresso.web.sugar.Web.onWebView
import androidx.test.espresso.web.webdriver.DriverAtoms.findElement
import androidx.test.espresso.web.webdriver.DriverAtoms.webClick
import androidx.test.espresso.web.webdriver.Locator
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.eyeo.privacy.PrivacyShieldProvider
import com.eyeo.pat.test.PatScreenCapture
import com.eyeo.privacy.PrivacyShieldAndroid
import com.eyeo.privacy.models.FilterLevel
import com.eyeo.privacy.webview.WebTestUtils.retryTest
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestName
import org.junit.runner.RunWith
import java.util.concurrent.TimeUnit

@RunWith(AndroidJUnit4::class)
class CrumbsBenchmark : PrivacyShieldProvider {

    @get:Rule
    var name = TestName()

    @Before
    fun init() {
        PrivacyShieldAndroid.release()
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        PrivacyShieldAndroid.setup(context)
    }

    @After
    fun release() {
        PrivacyShieldAndroid.release()
    }

    @Test
    fun testCookies() = retryTest {
        requirePrivacy.editSettings()
            .enable(false)
            .blockThirdPartyCookies(true)
            .apply()
        runTestAndCheck(
            "privacy feature should be off",
            false,
            TEST_ID_COOKIE_SANDBOX
        )
        runTestAndCheck(
            "privacy feature should be off",
            false,
            TEST_ID_COOKIE_BLOCKED
        )
        runTestAndCheck(
            "privacy feature should be off",
            false,
            TEST_ID_COOKIE_SANDBOX_JS
        )
        runTestAndCheck(
            "privacy feature should be off",
            false,
            TEST_ID_STORAGE_SANDBOX_JS
        )
        requirePrivacy.editSettings()
            .enable(true)
            .blockThirdPartyCookies(false)
            .apply()
        runTestAndCheck(
            "privacy feature should be off",
            false,
            TEST_ID_COOKIE_SANDBOX
        )
        runTestAndCheck(
            "privacy feature should be off",
            false,
            TEST_ID_COOKIE_BLOCKED
        )
        runTestAndCheck(
            "privacy feature should be off",
            false,
            TEST_ID_COOKIE_SANDBOX_JS
        )
        runTestAndCheck(
            "privacy feature should be off",
            false,
            TEST_ID_STORAGE_SANDBOX_JS
        )
        requirePrivacy.editSettings()
            .enable(true)
            .blockThirdPartyCookies(true)
            .cookiesFilterLevel(FilterLevel.SANDBOX)
            .apply()
        runTestAndCheck("No cookies sandbox", true, TEST_ID_COOKIE_SANDBOX)
        runTestAndCheck(
            "Third party cookie should not be blocked",
            false,
            TEST_ID_COOKIE_BLOCKED
        )
        runTestAndCheck(
            "No cookies sandbox in iframe",
            true,
            TEST_ID_COOKIE_SANDBOX_JS, retry = 5
        )
        runTestAndCheck(
            "No local storage sandbox in iframe",
            true,
            TEST_ID_STORAGE_SANDBOX_JS, retry = 5
        )
        requirePrivacy.editSettings()
            .enable(true)
            .blockThirdPartyCookies(true)
            .cookiesFilterLevel(FilterLevel.ALL)
            .apply()
        runTestAndCheck("Cookies are not blocked", false, TEST_ID_COOKIE_SANDBOX)
        runTestAndCheck("Cookies are not blocked", true, TEST_ID_COOKIE_BLOCKED)
        runTestAndCheck("Cookies are not blocked", true, TEST_ID_COOKIE_SANDBOX_JS, retry = 5)
        runTestAndCheck("Cookies are not blocked", true, TEST_ID_STORAGE_SANDBOX_JS, retry = 5)

    }

    @Test
    fun testReferrer() = retryTest {
        requirePrivacy.editSettings()
            .enable(false)
            .hideReferrerHeader(true)
            .apply()
        runTestAndCheck(
            "referrer should not be blocked",
            false,
            TEST_ID_REFERRER
        )
        requirePrivacy.editSettings()
            .enable(true)
            .hideReferrerHeader(false)
            .apply()
        runTestAndCheck(
            "referrer should not be blocked",
            false,
            TEST_ID_REFERRER
        )
        requirePrivacy.editSettings()
            .enable(true)
            .hideReferrerHeader(true)
            .apply()
        runTestAndCheck(
            "referrer not hide",
            true,
            TEST_ID_REFERRER
        )
    }

    @Test
    fun testDNT() = retryTest {
        requirePrivacy.editSettings()
            .enable(false)
            .enableDoNotTrack(true)
            .apply()
        runTestAndCheck(
            "dnt should be off",
            false,
            TEST_ID_DNT
        )
        requirePrivacy.editSettings()
            .enable(true)
            .enableDoNotTrack(false)
            .apply()
        runTestAndCheck(
            "dnt should be off",
            false,
            TEST_ID_DNT
        )
        requirePrivacy.editSettings()
            .enable(true)
            .enableDoNotTrack(true)
            .apply()
        runTestAndCheck(
            "dnt not working",
            true,
            TEST_ID_DNT
        )
    }

    @Test
    fun testSecGPC() = retryTest {
        requirePrivacy.editSettings()
            .enable(true)
            .enableGPC(true)
            .apply()
        Thread.sleep(2000)
        runTestAndCheck(
            "SecGPC not working",
            true,
            TEST_ID_SEC_GPC
        )

        requirePrivacy.editSettings()
            .enable(false)
            .enableGPC(true)
            .apply()
        runTestAndCheck(
            "SecGPC should be off",
            false,
            TEST_ID_SEC_GPC
        )

        requirePrivacy.editSettings()
            .enable(true)
            .enableGPC(false)
            .apply()
        runTestAndCheck(
            "SecGPC should be off",
            false,
            TEST_ID_SEC_GPC
        )

    }

    @Test
    fun testClientData() = retryTest {
        requirePrivacy.editSettings()
            .enable(false)
            .apply()
        runTestAndCheck(
            "hide client data is not off",
            false,
            TEST_ID_CLIENT_DATA
        )
        requirePrivacy.editSettings()
            .enable(true)
            .apply()
        runTestAndCheck(
            "hide client data is not working",
            true,
            TEST_ID_CLIENT_DATA
        )
    }


    @Test
    fun testMarketingParams() = retryTest {
        requirePrivacy.editSettings()
            .enable(false)
            .removeMarketingTrackingParameters(true)
            .apply()
        runTestAndCheck(
            "params were removed",
            false,
            TEST_ID_MARKETING_PARAMS
        )
        requirePrivacy.editSettings()
            .enable(true)
            .removeMarketingTrackingParameters(false)
            .apply()
        runTestAndCheck(
            "params were removed",
            false,
            TEST_ID_MARKETING_PARAMS
        )
        requirePrivacy.editSettings()
            .enable(true)
            .removeMarketingTrackingParameters(true)
            .apply()
        runTestAndCheck(
            "params were not removed",
            true,
            TEST_ID_MARKETING_PARAMS
        )
    }

    @Test
    fun testBlockHyperlinkAuditing() = retryTest {
        requirePrivacy.editSettings()
            .enable(false)
            .blockHyperlinkAuditing(true)
            .apply()
        runTestAndCheck(
            "ping blocked",
            false,
            TEST_ID_HYPERLINK_AUDITING
        )
        requirePrivacy.editSettings()
            .enable(true)
            .blockHyperlinkAuditing(false)
            .apply()
        runTestAndCheck(
            "ping blocked",
            false,
            TEST_ID_HYPERLINK_AUDITING
        )
        requirePrivacy.editSettings()
            .enable(true)
            .blockHyperlinkAuditing(true)
            .apply()
        runTestAndCheck(
            "ping not blocked",
            true,
            TEST_ID_HYPERLINK_AUDITING
        )
    }

    @Test
    fun testCNAMECloaking() = retryTest {
        requirePrivacy.editSettings()
            .enable(false)
            .enableCNameCloakingProtection(true)
            .apply()
        runTestAndCheck(
            "cname cloaking should be off",
            false,
            TEST_ID_CNAME_CLOAKING, true
        )
        requirePrivacy.editSettings()
            .enable(true)
            .enableCNameCloakingProtection(false)
            .apply()
        runTestAndCheck(
            "cname cloaking should be off",
            false,
            TEST_ID_CNAME_CLOAKING, true
        )
        requirePrivacy.editSettings()
            .enable(true)
            .enableCNameCloakingProtection(true)
            .apply()
        Thread.sleep(3000)
        runTestAndCheck(
            "cname cloaking detection is not working",
            true,
            TEST_ID_CNAME_CLOAKING, true
        )

    }

    private fun runTestAndCheck(
        message: String,
        expectedValue: Boolean,
        testId: String,
        useAdblockWebView: Boolean = false,
        retry: Int = 3
    ) {
        repeat(retry) {
            val result = runTest(testId, message, useAdblockWebView)

            if (result == expectedValue)
                return
        }
        Assert.fail(message)
    }

    private fun runTest(
        testId: String,
        message: String,
        useAdblockWebView: Boolean = true
    ): Boolean {
        val intent =
            Intent(InstrumentationRegistry.getInstrumentation().context, TestActivity::class.java)
        intent.putExtra(TestActivity.ARG_ADBLOCK, useAdblockWebView)

        val activityScenario = ActivityScenario.launch<TestActivity>(intent)
        return activityScenario.let { scenario ->
            try {
                scenario.moveToState(Lifecycle.State.RESUMED)
                InstrumentationRegistry.getInstrumentation().waitForIdleSync()
                Thread.sleep(500)
                pressButton(testId, "ResetButton")
                Thread.sleep(100)
                pressButton(testId, "AutoButton")
                var retry = 0
                while (retry < 15) {
                    Thread.sleep(1000)
                    getTestResult(testId)?.also {
                        scenario.moveToState(Lifecycle.State.DESTROYED)
                        return@let it
                    }
                    retry++
                }
                throw IllegalStateException("Can't get test result: TIMEOUT")
            } catch (e: Throwable) {
                captureScreenshot(activityScenario, message)
                scenario.moveToState(Lifecycle.State.DESTROYED)
                throw e
            }
        }
    }

    private fun captureScreenshot(
        activityScenario: ActivityScenario<TestActivity>,
        message: String
    ) {
        activityScenario.onActivity { activity ->
            val screenshotName = name.methodName + "_" +
                    message.split(" ")
                        .joinToString(separator = "") { word -> word.replaceFirstChar { it.uppercase() } }
            PatScreenCapture.captureScreenshot(activity, screenshotName, childFolder = SCREENSHOTS_FOLDER)
        }
        Thread.sleep(3000)
    }

    private fun getTestResult(testId: String): Boolean? {
        InstrumentationRegistry.getInstrumentation().waitForIdleSync()
        val classes = onWebView()
            .perform(
                script(
                    "return document.getElementsByClassName(\"${testId}\")[0].className",
                    castOrDie(String::class.java)
                )
            ).get()

        return classes?.takeIf { it.contains("TestFinished") }?.contains("TestSuccess")
    }

    private fun pressButton(testId: String, buttonClass: String) {
        onWebView()
            .withTimeout(20, TimeUnit.SECONDS)
            .withElement(
                findElement(Locator.CSS_SELECTOR, ".$testId .$buttonClass"),
            ).perform(webClick())
            .get()
        Thread.sleep(200)
    }

    companion object {

        private val SCREENSHOTS_FOLDER =
            "screenshots/${CrumbsBenchmark::class.java.name}"

        private const val TEST_ID_COOKIE_SANDBOX = "THC_SANDBOX"
        private const val TEST_ID_COOKIE_SANDBOX_JS = "THC_SANDBOX_JS"
        private const val TEST_ID_STORAGE_SANDBOX_JS = "STORAGE_SANDBOX_JS"
        private const val TEST_ID_COOKIE_BLOCKED = "THC_BLOCKED"
        private const val TEST_ID_REFERRER = "referrerPathCrop"
        private const val TEST_ID_DNT = "DNT"
        private const val TEST_ID_SEC_GPC = "SecGPC"
        private const val TEST_ID_CLIENT_DATA = "X-Client-Data"
        private const val TEST_ID_MARKETING_PARAMS = "marketingParameters"
        private const val TEST_ID_HYPERLINK_AUDITING = "HYPERLINK"
        private const val TEST_ID_CNAME_CLOAKING = "cnameCloakingTracker"
    }
}