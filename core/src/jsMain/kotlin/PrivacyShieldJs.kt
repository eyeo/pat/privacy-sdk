import com.eyeo.pat.provider.JsResourcesProvider
import com.russhwolf.settings.Settings
import com.eyeo.privacy.PrivacyShieldCore
import com.eyeo.privacy.PrivacyShieldOptions
import com.eyeo.privacy.service.JsDnsClient
import kotlin.js.Promise

@JsExport
object PrivacyShieldJs {
    fun get() = PrivacyShieldCore.get()

    fun setup(
        options: PrivacyShieldOptions,
        @Suppress("NON_EXPORTABLE_TYPE") settingsStorage: Settings
    ) {
        // prevent crash in ktor with condition (window === undefined)
        js("globalThis.window = undefined")
        PrivacyShieldCore.createInstance(
            settingsStorage,
            JsResourcesProvider(),
            JsDnsClient(),
            options,
        )
    }
}

@JsExport
@Suppress("NON_EXPORTABLE_TYPE")
object Cast {
    fun stringArrayToSet(interests: Array<String>) = interests.toSet()

    fun stringSetToArray(interests: Set<String>) = interests.toTypedArray()
}

@JsExport
class CallbackWrapper<T> {
    private var hasResult = false
    private var result: T? = null
    private var resolve: ((T) -> Unit)? = null
    fun callback(result: T) {
        hasResult = true
        this.result = result
        resolve?.invoke(result)
        resolve = null
    }

    fun toPromise(): Promise<T> {
        if (hasResult) return Promise.resolve(result!!)
        return Promise { resolve, _ ->
            this.resolve = resolve
        }
    }
}