package com.eyeo.privacy.service

import com.eyeo.pat.provider.ResourcesProvider
import com.eyeo.pat.provider.SettingsProvider
import io.ktor.http.*
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.RelaxedMockK
import com.eyeo.privacy.models.PrivacySettings
import org.junit.Assert
import org.junit.Before
import kotlin.test.Test

class PrivacyServiceTests {

    private lateinit var privacyService: PrivacyService

    @RelaxedMockK
    internal lateinit var settings: SettingsProvider<PrivacySettings>

    @RelaxedMockK
    internal lateinit var resourcesProvider: ResourcesProvider

    @RelaxedMockK
    internal lateinit var domainService: DomainService

    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxUnitFun = true)
        privacyService = PrivacyService(
            settings,
            resourcesProvider,
            domainService,
            supportChips = false,
            supportStoragePartitioning = false
        )
    }

    @Test
    fun removeTrackingParametersTest() {

        coEvery {
            settings.get()
        }.returns(PrivacySettings(enabled = true, removeMarketingTrackingParameters = false))

        URLBuilder().takeFrom(fullQueryParam.build()).also {
            privacyService.removeTrackingParameters(it)
            Assert.assertEquals(it.buildString(), fullQueryParam.buildString())
        }

        coEvery {
            settings.get()
        }.returns(PrivacySettings(enabled = true, removeMarketingTrackingParameters = true))

        URLBuilder().takeFrom(fullQueryParam.build()).also {
            privacyService.removeTrackingParameters(it)
            Assert.assertEquals(it.parameters.entries().size, 0)
            Assert.assertNotEquals(it.buildString(), fullQueryParam.buildString())
        }

        URLBuilder().takeFrom(emptyQueryParam.build()).also {
            privacyService.removeTrackingParameters(it)
            Assert.assertEquals(it.buildString(), emptyQueryParam.buildString())
        }

        URLBuilder().takeFrom(conditionalQueryParam1.build()).also {
            privacyService.removeTrackingParameters(it)
            Assert.assertEquals(it.buildString(), conditionalQueryParam1.buildString())
        }

        URLBuilder().takeFrom(conditionalQueryParam2.build()).also {
            privacyService.removeTrackingParameters(it)
            Assert.assertEquals(it.buildString(), conditionalQueryParam2.buildString())
        }

        URLBuilder().takeFrom(conditionalQueryParam3.build()).also {
            privacyService.removeTrackingParameters(it)
            Assert.assertEquals(it.parameters.entries().size, 0)
            Assert.assertNotEquals(it.buildString(), conditionalQueryParam3.buildString())
        }
    }

    @Test
    fun ampToCanonicalTest() {
        coEvery {
            settings.get()
        }.returns(PrivacySettings(enabled = true, deAMP = true))
        ampToNonAmp.forEach {
            Assert.assertEquals(
                privacyService.ampToCanonical(URLBuilder(it.key)).buildString(), it.value
            )
        }
    }

    companion object {
        val emptyQueryParam = URLBuilder("https://google.com/?normal_param=1")
        val fullQueryParam =
            URLBuilder(host = "google.com", parameters = ParametersBuilder().apply {
                PrivacyService.TRACKING_URL_PARAMS.forEach {
                    append(it, "1")
                }
            }.build())
        val conditionalQueryParam1 =
            URLBuilder("https://google.com?mkt_tok=trackingValue&mkt_unsubscribe=1")
        val conditionalQueryParam2 =
            URLBuilder("https://google.com/Unsubscribe?mkt_tok=trackingValue")
        val conditionalQueryParam3 = URLBuilder("https://google.com?mkt_tok=value")

        val ampToNonAmp = mapOf(
            "https://www-dummy-org.cdn.ampproject.org/c/s/www.dummy.org/amp/" to "https://www.dummy.org",
            "https://www.google.com/amp/s/www.dummy.org/a-b-c/amp/" to "https://www.dummy.org/a-b-c",
            "https://dummy-org.cdn.ampproject.org/c/s/dummy.org/a/amp" to "https://dummy.org/a",
            "https://www-dummy-org.cdn.ampproject.org/c/s/www.dummy.org/?outputType=amp-type" to "https://www.dummy.org",
            "https://www-dummy-org.cdn.ampproject.org/c/s/www.dummy.org/abc.amp" to "https://www.dummy.org/abc",
            "https://www-dummy-org.cdn.ampproject.org/c/s/www.dummy.org/amp/a/b/c" to "https://www.dummy.org/a/b/c",
            "https://dummy-org.cdn.ampproject.org/c/s/dummy.org/blog/?amp" to "https://dummy.org/blog",
            "https://www-dummy-org.cdn.ampproject.org/c/s/www.dummy.org/a/b/amp/c.html" to "https://www.dummy.org/a/b/c.html",
            "https://www.google.com/amp/s/www.dummy.org/a/b.amp" to "https://www.dummy.org/a/b",
            "https://www-dummy-org.cdn.ampproject.org/c/s/www.dummy.org/a/b/c.amp.html" to "https://www.dummy.org/a/b/c.html",
            "https://amp-dummy-org.cdn.ampproject.org/c/s/amp.dummy.org/a.html" to "https://dummy.org/a.html",
            "https://cdn.ampproject.org/preconnect.gif?crossorigin0.000" to "https://cdn.ampproject.org/preconnect.gif?crossorigin0.000"
        )
    }

}