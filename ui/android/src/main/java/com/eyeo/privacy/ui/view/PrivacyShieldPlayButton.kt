package com.eyeo.privacy.ui.view

import android.content.Context
import android.graphics.drawable.Animatable
import android.graphics.drawable.Drawable
import android.os.Handler
import android.os.Looper
import android.util.AttributeSet
import androidx.annotation.DrawableRes
import androidx.appcompat.widget.AppCompatImageButton
import androidx.core.content.ContextCompat
import com.eyeo.privacy.PrivacyShieldProvider
import com.eyeo.privacy.ui.R

class PrivacyShieldPlayButton @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : AppCompatImageButton(context, attrs, defStyleAttr), PrivacyShieldProvider,
    PrivacyShieldUIContext.UIContextListener, PrivacyShieldUIContext.UIContextAwareImpl {

    private val clickListeners = mutableListOf<ClickListener>()

    private val maximumAnimationDuration by lazy {
        context.resources.getInteger(R.integer.ps_play_button_animation_duration).toLong()
    }

    private var currentMode: Mode = Mode.PLAYPAUSE
        set(value) {
            field = value
            setImageDrawable(field)
        }

    private val postHandler = Handler(Looper.getMainLooper())

    private val animationEndRunnable: Runnable = Runnable {
        val oppositeMode = currentMode.getOppositeMode()
        currentMode = oppositeMode
    }

    override var uiContext: PrivacyShieldUIContext = PrivacyShieldUIContext.Default

    init {
        currentMode = uiContext.getProtectionStatus().toMode()
        addClickListener(ClickListenerImpl(uiContext))
        setOnClickListener {
            val copy = ArrayList(clickListeners)
            copy.forEach { it.onClick() }
        }
        setOnLongClickListener {
            val copy = ArrayList(clickListeners)
            copy.forEach { it.onLongClick() }
            true
        }
    }

    fun addClickListener(listener: ClickListener) = clickListeners.add(listener)
    fun removeClickListener(listener: ClickListener) = clickListeners.remove(listener)

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        uiContext.registerListener(this)
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        uiContext.unregisterListener(this)
    }

    override fun onProtectionUpdated(status: PrivacyShieldUIContext.ProtectionStatus) {
        val newMode = status.toMode()
        if ((this.drawable.asAnimatable().isRunning && this.currentMode.getOppositeMode() != newMode) || (!this.drawable.asAnimatable().isRunning && this.currentMode != newMode)) {

            if (this.drawable.asAnimatable().isRunning) {
                postHandler.removeCallbacks(animationEndRunnable)
            }
            this.currentMode = newMode.getOppositeMode()
            this.drawable.asAnimatable().start()

            postHandler.postDelayed(animationEndRunnable, maximumAnimationDuration)
        }
    }

    private fun setImageDrawable(mode: Mode) {
        val animatedVector = ContextCompat.getDrawable(context, mode.drawableRes)
        this.setImageDrawable(animatedVector)
    }

    private fun Drawable.asAnimatable() = (this as Animatable)

    sealed class Mode(@DrawableRes val drawableRes: Int) {
        object PAUSEPLAY : Mode(R.drawable.ps_play_to_pause_animation)
        object PLAYPAUSE : Mode(R.drawable.ps_pause_to_play_animation)
        //object STOPPLAY : Mode(R.drawable.ps_play_to_stop_animation)
        //object PLAYSTOP : Mode(R.drawable.ps_stop_to_play_animation)
    }

    private val opposites = mapOf(
        Mode.PLAYPAUSE to Mode.PAUSEPLAY,
        Mode.PAUSEPLAY to Mode.PLAYPAUSE,
        //Mode.PLAYSTOP to Mode.STOPPLAY,
        //Mode.STOPPLAY to Mode.PLAYSTOP
    )

    private fun Mode.getOppositeMode() = opposites[this]!!

    private fun PrivacyShieldUIContext.ProtectionStatus.toMode(): Mode {
        return when (this) {
            PrivacyShieldUIContext.ProtectionStatus.ENABLED -> Mode.PLAYPAUSE
            PrivacyShieldUIContext.ProtectionStatus.PAUSED -> Mode.PAUSEPLAY
            PrivacyShieldUIContext.ProtectionStatus.PAUSED_UNTIL_NEXT_SESSION -> Mode.PAUSEPLAY
        }
    }
    interface ClickListener {
        fun onClick()
        fun onLongClick()
    }

    class ClickListenerImpl(private val uiContext: PrivacyShieldUIContext) :
        ClickListener,
        PrivacyShieldProvider {
        override fun onClick() {
            val url = uiContext.getCurrentUrl()
            privacy?.apply {
                val editor = editSettings()
                when (uiContext.getProtectionStatus()) {
                    PrivacyShieldUIContext.ProtectionStatus.ENABLED -> {
                        // use allowDomainThisSession instead to resume tracking protection
                        // in the next session instead of persisting the pause
                        editor.allowDomain(url)
                    }

                    PrivacyShieldUIContext.ProtectionStatus.PAUSED,
                    PrivacyShieldUIContext.ProtectionStatus.PAUSED_UNTIL_NEXT_SESSION -> {
                        editor.controlDomain(url)
                    }
                }.apply()
            }
        }

        override fun onLongClick() {
            val url = uiContext.getCurrentUrl()
            privacy?.apply {
                editSettings().allowDomainThisSession(url).apply()
            }
        }
    }
}