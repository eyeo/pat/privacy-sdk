package com.eyeo.privacy.service

import com.eyeo.pat.PatLogger
import com.eyeo.pat.models.RequestType
import com.eyeo.pat.utils.UrlExtension.toUrl
import com.eyeo.pat.utils.UrlExtension.toUrlBuilder
import com.eyeo.privacy.models.PrivacyShieldHeaders
import com.eyeo.privacy.models.FilterLevel
import io.ktor.http.*
import io.ktor.util.date.*

internal class HeaderService(
    private val privacyService: PrivacyService,
    private val statsService: StatsService,
    private val domainService: DomainService,
    private val supportCHIPS: Boolean,
) {
    private val logger = PatLogger("HeaderService")

    fun filterSendHeaders(
        tabId: String,
        docUrl: Url,
        reqUrl: Url,
        @Suppress("UNUSED_PARAMETER") requestType: RequestType,
        thirdPartyRequest: Boolean,
        headers: List<Pair<String, String>>
    ): List<Pair<String, String>> {
        val settings = privacyService.getSettings()
        val requestHeaders = parseHeaders(headers)
        requestHeaders.filterIsInstance<UserAgentHeader>().forEach {
            if (settings.enableFingerprintShield) {
                it.reduce()
            }
        }
        requestHeaders.filterIsInstance<AcceptLanguageHeader>().forEach {
            if (settings.enableFingerprintShield) {
                it.reduce()
            }
        }
        val protectionEnabledForDocument = privacyService.isProtectionEnabled(docUrl)
        if (protectionEnabledForDocument) {

            if (settings.blockThirdPartyCookies) {
                val cookiePrefix = "${domainService.getMainDomain(docUrl)}$COOKIE_SEPARATOR"
                requestHeaders.filterIsInstance<CookieHeader>().forEach { cookieHeader ->

                    val cookieCount = cookieHeader.cookies.size
                    when (settings.cookiesFilterLevel) {
                        FilterLevel.ALL -> {
                            if (thirdPartyRequest) {
                                cookieHeader.cookies.clear()
                            }
                        }
                        FilterLevel.SANDBOX -> {
                            if (!supportCHIPS) {
                                if (!thirdPartyRequest) {
                                    cookieHeader.cookies.removeAll {
                                        it.name.contains(
                                            COOKIE_SEPARATOR
                                        )
                                    }
                                } else {
                                    val newCookies = cookieHeader.cookies
                                        .filter { it.name.startsWith(cookiePrefix) }
                                        .map { it.copy(name = it.name.substring(cookiePrefix.length)) }
                                    cookieHeader.cookies.clear()
                                    cookieHeader.cookies.addAll(newCookies)
                                }


                            }
                        }
                    }
                    repeat(cookieCount - cookieHeader.cookies.size) {
                        statsService.onCookiesBlocked(tabId)
                    }
                }
            }

            if (settings.hideReferrerHeader) {
                if (!reqUrl.protocol.isSecure()) {
                    requestHeaders.removeAll {
                        it.getName().equals(PrivacyShieldHeaders.Referrer, ignoreCase = true)
                    }
                } else {
                    requestHeaders.filterIsInstance<ReferrerHeader>().forEach { referrer ->
                        referrer.getValue().toUrl()?.let { referrerUrl ->
                            referrer.hidePath =
                                domainService.isThirdPartyRequest(referrerUrl, reqUrl, true)
                        }
                    }
                }
            }

            requestHeaders.removeAll {
                it.getName().equals(PrivacyShieldHeaders.XClientData, ignoreCase = true)
            }

            if (settings.enableDoNotTrack) {
                requestHeaders.removeAll {
                    it.getName().equals(PrivacyShieldHeaders.DNT, ignoreCase = true)
                }
                requestHeaders.add(SimpleHeader(PrivacyShieldHeaders.DNT, "1"))
            }

            if (settings.enableGPC) {
                requestHeaders.removeAll {
                    it.getName().equals(PrivacyShieldHeaders.SecGPC, ignoreCase = true)
                }
                requestHeaders.add(SimpleHeader(PrivacyShieldHeaders.SecGPC, "1"))
            }
            if (settings.enableFingerprintShield) {
                requestHeaders.removeAll {
                    PrivacyShieldHeaders.HttpHeadersToReduceFingerprint.contains(it.getName())
                }
            }
        }

        if (!protectionEnabledForDocument || !settings.blockThirdPartyCookies) {
            //remove sandbox cookie when protection is disabled
            requestHeaders.filterIsInstance<CookieHeader>().forEach { cookieHeader ->
                cookieHeader.cookies.removeAll { it.name.contains(COOKIE_SEPARATOR) }
            }
        }

        return renderHeaders(requestHeaders)
    }

    fun filterReceivedHeaders(
        tabId: String,
        docUrl: Url,
        requestType: RequestType,
        thirdPartyRequest: Boolean,
        headers: List<Pair<String, String>>
    ): List<Pair<String, String>> {
        val settings = privacyService.getSettings()
        val protectionEnabled = privacyService.isProtectionEnabled(docUrl)
        val blockCookies =
            protectionEnabled && settings.blockThirdPartyCookies
        
        if (!blockCookies) {
            return headers
        }

        val requestHeaders = parseHeaders(headers)
        if (blockCookies) {
            val cookiePrefix = "${domainService.getMainDomain(docUrl)}$COOKIE_SEPARATOR"
            requestHeaders.filterIsInstance<SetCookieHeader>()
                .forEach { setCookieHeader ->
                    when (settings.cookiesFilterLevel) {
                        FilterLevel.ALL -> {
                            if (thirdPartyRequest) {
                                setCookieHeader.forceExpire = true
                                statsService.onCookiesBlocked(tabId)
                            }
                        }
                        FilterLevel.SANDBOX -> {
                            if (thirdPartyRequest) {
                                if (supportCHIPS) {
                                    setCookieHeader.cookie = setCookieHeader.cookie.copy(
                                        extensions = setCookieHeader.cookie.extensions.toMutableMap()
                                            .apply {
                                                put("Partitioned", null)
                                            },
                                        secure = true,
                                        path = "/",
                                    )
                                } else {
                                    setCookieHeader.cookie =
                                            //prefix and convert to session cookie
                                        setCookieHeader.cookie.copy(
                                            name = cookiePrefix + setCookieHeader.cookie.name,
                                            maxAge = 0,
                                            expires = null
                                        )
                                }
                                statsService.onCookiesBlocked(tabId)
                            }
                        }
                    }
                }
        }

        if (protectionEnabled && requestType.isFrameRequest()) {
            // make sure we don't allow FLoC on any website
            requestHeaders.add(FLoCHeader.instance)
        }
        return renderHeaders(requestHeaders)
    }

    private fun parseHeaders(headers: List<Pair<String, String>>): MutableList<Header> {
        return headers.map { parseHeader(it.first, it.second) }.toMutableList()
    }

    private fun renderHeaders(headers: List<Header>): List<Pair<String, String>> {
        return headers.map { renderHeader(it) }.toList()
    }

    private fun parseHeader(name: String, value: String): Header {
        if (value.isEmpty())
            return SimpleHeader(name, value)
        return try {
            when (name.lowercase()) {
                PrivacyShieldHeaders.UserAgent.lowercase() -> UserAgentHeader(value)
                PrivacyShieldHeaders.AcceptLanguage.lowercase() -> AcceptLanguageHeader(value)
                PrivacyShieldHeaders.Referrer.lowercase() -> ReferrerHeader(value)
                PrivacyShieldHeaders.Cookie.lowercase() -> CookieHeader(value)
                PrivacyShieldHeaders.SetCookie.lowercase() -> SetCookieHeader(value)
                else -> SimpleHeader(name, value)
            }
        } catch (e: Exception) {
            logger.w("Can't parse header: $name=$value", e)
            SimpleHeader(name, value)
        }
    }

    private fun renderHeader(header: Header): Pair<String, String> =
        Pair(header.getName(), header.getValue())

    companion object {
        private const val COOKIE_SEPARATOR = "##PS##"
    }

}


abstract class Header(private val key: String) {
    fun getName(): String = key
    abstract fun getValue(): String
}

open class SimpleHeader(name: String, private val value: String) : Header(name) {
    override fun getValue() = value
}

internal class CookieHeader(value: String) : Header(PrivacyShieldHeaders.Cookie) {
    val cookies: MutableList<Cookie> =
        parseClientCookiesHeader(value).map {
            Cookie(
                name = it.key,
                value = it.value,
                encoding = CookieEncoding.RAW
            )
        }
            .toMutableList()

    override fun getValue(): String {
        return renderCookies()
    }

    private fun renderCookies(): String = buildString {
        cookies.forEachIndexed { index, item ->
            append(item.name)
            append('=')
            append(item.value)
            if (index < cookies.size - 1) {
                append("; ")
            }
        }
    }
}

internal class UserAgentHeader(private var value: String) : Header(PrivacyShieldHeaders.UserAgent) {

    override fun getValue() = value

    fun reduce() {
        val chromeUAs =
            Regex(
                "Mozilla/5\\.0 \\(((?<platform>Lin|Win|Mac|X11; C|X11; L)+[^)]+)\\) " +
                        "AppleWebKit/537.36 \\(KHTML, like Gecko\\)(?: Version/\\d*.\\d*)? " +
                        "Chrome/(?<major>\\d+)[\\d.]+(?<mobile>[ Mobile]*) Safari/537\\.36"
            )

        chromeUAs.find(value)?.let {
            val (_, platform, major, mobile) = it.destructured

            val unifiedPlatform = mapOf(
                "Lin" to "Linux; Android 13",
                "Win" to "Windows NT 10.0; Win64; x64",
                "Mac" to "Macintosh; Intel Mac OS X 10_15_7",
                "X11; C" to "X11; CrOS x86_64",
                "X11; L" to "X11; Linux x86_64",
            )
            val reducedUA =
                "Mozilla/5.0 (${unifiedPlatform[platform]}) " +
                        "AppleWebKit/537.36 (KHTML, like Gecko) " +
                        "Chrome/${major}.0.0.0${mobile} Safari/537.36"
            value = reducedUA
        }
    }
}


class AcceptLanguageHeader(private var value: String) :
    Header(PrivacyShieldHeaders.AcceptLanguage) {

    override fun getValue() = value

    fun reduce() {
        value = value.split(",").take(2).joinToString(",")
    }
}


internal class SetCookieHeader(value: String) : Header(PrivacyShieldHeaders.SetCookie) {

    var cookie: Cookie = parseServerSetCookieHeader(value)
    var forceExpire: Boolean = false

    override fun getValue(): String {
        val returnCookie = if (forceExpire) {
            cookie.copy(maxAge = 0)
        } else {
            cookie
        }
        var value = with(returnCookie) {
            renderSetCookieHeader(
                name,
                value,
                encoding,
                maxAge,
                expires,
                domain,
                path,
                secure,
                httpOnly,
                extensions
            )
        }
        if (forceExpire) {
            value = "$value; Max-Age=0"
        }
        return value
    }

    private fun renderSetCookieHeader(
        name: String, value: String,
        encoding: CookieEncoding = CookieEncoding.URI_ENCODING,
        maxAge: Int = 0, expires: GMTDate? = null, domain: String? = null,
        path: String? = null,
        secure: Boolean = false, httpOnly: Boolean = false,
        extensions: Map<String, String?> = emptyMap(),
        includeEncoding: Boolean = true
    ): String = (
            listOf(
                cookiePartUnencoded(name, value),
                cookiePartUnencoded("Max-Age", if (maxAge > 0) maxAge else null),
                cookiePartUnencoded("Expires", expires?.toHttpDate()),
                cookiePartUnencoded("Domain", domain),
                cookiePartUnencoded("Path", path),
                cookiePartFlag("Secure", secure),
                cookiePartFlag("HttpOnly", httpOnly)
            )
                    + extensions.map { cookiePartExt(it.key, it.value) }
                    + if (includeEncoding) cookiePartExt(
                "\$x-enc",
                encoding.name
            ) else ""
            ).filter { it.isNotEmpty() }
        .joinToString("; ")

    private fun parseServerSetCookieHeader(cookiesHeader: String): Cookie {
        val asMap = parseClientCookiesHeader(cookiesHeader, false)
        val first = asMap.entries.first { !it.key.startsWith("$") }
        val encoding =
            asMap["\$x-enc"]?.let { CookieEncoding.valueOf(it) } ?: CookieEncoding.RAW
        val loweredMap = asMap.mapKeys { it.key.lowercase() }

        return Cookie(
            name = first.key,
            value = first.value,
            encoding = encoding,
            maxAge = loweredMap["max-age"]?.toInt() ?: 0,
            expires = loweredMap["expires"]?.fromCookieToGmtDate(),
            domain = loweredMap["domain"],
            path = loweredMap["path"],
            secure = "secure" in loweredMap,
            httpOnly = "httponly" in loweredMap,
            extensions = asMap.filterKeys {
                !loweredPartNames.contains(it.lowercase()) && it != first.key
            }
        )
    }

    @Suppress("NOTHING_TO_INLINE")
    private inline fun cookiePartUnencoded(name: String, value: Any?) =
        if (value != null) "$name=$value" else ""


    @Suppress("NOTHING_TO_INLINE")
    private inline fun cookiePartFlag(name: String, value: Boolean) =
        if (value) name else ""

    @Suppress("NOTHING_TO_INLINE")
    private inline fun cookiePartExt(name: String, value: String?) =
        if (value == null) cookiePartFlag(name, true) else cookiePartUnencoded(name, value)

    companion object {
        private val loweredPartNames =
            setOf("max-age", "expires", "domain", "path", "secure", "httponly", "\$x-enc")
    }
}

internal class ReferrerHeader(value: String) : SimpleHeader(PrivacyShieldHeaders.Referrer, value) {

    var hidePath = false

    override fun getValue(): String {
        val value = super.getValue()
        return if (hidePath) {
            value.toUrlBuilder()?.let {
                if (it.encodedPath.isNotEmpty()) {
                    it.encodedPath = "/"
                }
                it.parameters.clear()
                it.buildString()
            } ?: value
        } else {
            value
        }
    }

}

internal class FLoCHeader private constructor() : SimpleHeader(key, value) {
    companion object {
        val instance = FLoCHeader()
        private const val key = "permissions-policy"
        private const val value = "interest-cohort=()"
    }
}