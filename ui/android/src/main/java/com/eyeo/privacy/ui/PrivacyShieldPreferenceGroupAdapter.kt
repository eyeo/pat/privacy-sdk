package com.eyeo.privacy.ui

import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.graphics.Color
import android.view.ViewGroup
import android.view.animation.DecelerateInterpolator
import android.widget.TextView
import androidx.core.animation.doOnEnd
import androidx.core.graphics.ColorUtils
import androidx.preference.PreferenceGroupAdapter
import androidx.preference.PreferenceScreen
import androidx.preference.PreferenceViewHolder
import androidx.vectordrawable.graphics.drawable.ArgbEvaluator
import com.eyeo.privacy.ui.utils.themeColor

@SuppressLint("RestrictedApi")
internal class PrivacyShieldPreferenceGroupAdapter(
    preferenceScreen: PreferenceScreen,
    private val highlightPosition: Int?
) :
    PreferenceGroupAdapter(preferenceScreen) {

    private var highlightRequested = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PreferenceViewHolder {
        return super.onCreateViewHolder(parent, viewType).apply {
            // Fix vertical scroll bar showing in summary when an item is recycled
            findViewById(android.R.id.summary)?.let { it as? TextView }?.apply {
                maxLines = Integer.MAX_VALUE
                isVerticalScrollBarEnabled = false
            }
        }
    }

    override fun onBindViewHolder(holder: PreferenceViewHolder, position: Int) {
        super.onBindViewHolder(holder, position)
        if (position == highlightPosition && !highlightRequested) {
            highlightRequested = true

            val baseColor = Color.TRANSPARENT
            val highlightColor = ColorUtils.setAlphaComponent(
                holder.itemView.context.themeColor(androidx.appcompat.R.attr.colorAccent),
                50
            )
            val fadeInLoop =
                ValueAnimator.ofObject(ArgbEvaluator(), baseColor, highlightColor)
            fadeInLoop.apply {
                interpolator = DecelerateInterpolator()
                duration = 500
                repeatMode = ValueAnimator.RESTART
                repeatCount = 2
                addUpdateListener {
                    holder.itemView.setBackgroundColor(it.animatedValue as Int)
                }
                doOnEnd {
                    holder.itemView.setBackgroundColor(baseColor)
                }
            }
            fadeInLoop.start()
        }
    }
}