Privacy SDK
=================

This repository contains the generic Privacy Core code that's shared between  
platforms along with an Android UI library showcased inside of a demo app.

## [Documentation](https://eyeo.gitlab.io/pat/privacy-shield-sdk)

To use Privacy SDK please check the [user documentation](https://eyeo.gitlab.io/pat/privacy-shield-sdk)
You can also check api details thought the [Kotlin documentation](https://eyeo.gitlab.io/pat/privacy-shield-sdk/docs).
Privacy SDK [ChangeLog](https://eyeo.gitlab.io/pat/privacy-shield-sdk/changeLog) is available in the documentation.

## Requirements

In order to run the unit test suite or build the project you need a working java environment.
This project has been build with kotlin multi-platform.

## Developer commands

|Command|Description|
|--|--|
|`./gradlew build`| assemble and test this project |
|`./gradlew allTests`| runs tests for all targets and create aggregated report |
|`./gradlew publishAndroidPublicationToMavenLocal`| publish android library to maven local  |
|`cd ./docs && bundle exec jekyll serve --livereload`| work on documentation locally |
|`./gradlew updateScreenShots`| run android instrumentation tests to update documentation screenshots|

## CI

Artifacts are uploaded on the [maven repository](https://gitlab.com/api/v4/groups/10262605/-/packages/maven) with the following version name:
- main => X.X.X
- develop => X.X.X-SNAPSHOT
- feat/shared/branch_name => X.X.X-branch-name-SNAPSHOT

During development, when it is required to share a feature between repositories depending on each other, consider using `feat/shared/branch_name` for all repositories until their merge to develop.

## License

Licensed under GPL 3.0;
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    https://www.gnu.org/licenses/gpl-3.0.en.html