package com.eyeo.privacy

import com.eyeo.privacy.presenter.AnalyticsPresenter
import com.eyeo.privacy.service.StatsService

internal expect class CmpJsInterface internal constructor(
    statsService: StatsService,
    analyticsPresenter: AnalyticsPresenter
)