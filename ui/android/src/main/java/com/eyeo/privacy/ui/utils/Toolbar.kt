package com.eyeo.privacy.ui.utils

import androidx.appcompat.app.AppCompatActivity

fun AppCompatActivity.setToolbarTitle(title: String) {
    this.supportActionBar?.apply {
        this.title = title
    }
}