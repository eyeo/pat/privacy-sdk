import org.gradle.api.DefaultTask
import org.gradle.api.Task
import org.gradle.api.tasks.InputFile
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.TaskAction
import java.io.File
import java.net.IDN

/**
 * Prepare the public suffix list file to be embedded in the sdk
 */
abstract class PrepareSuffixTask : DefaultTask() {

    fun from(task: Task) {
        this.inputFile = task.outputs.files.singleFile
        dependsOn += task
    }

    fun to(outputFile: File) {
        this.outputFile = outputFile
    }

    @InputFile
    lateinit var inputFile: File

    @OutputFile
    lateinit var outputFile: File

    @ExperimentalStdlibApi
    @TaskAction
    fun prepareSuffixFile() {
        outputFile.parentFile.mkdirs()
        outputFile.createNewFile()
        inputFile.readLines().forEach { line ->
            val cleanLine = line.trim()
            // remove empty string and comments
            if (cleanLine.isNotEmpty() && !cleanLine.startsWith("//")) {
                val ascii = IDN.toASCII(cleanLine)
                // check if an unicode version should exist
                if (ascii != cleanLine) {
                    outputFile.appendText(ascii + "\n")
                }
                outputFile.appendText(line + "\n")
            }
        }
    }
}
