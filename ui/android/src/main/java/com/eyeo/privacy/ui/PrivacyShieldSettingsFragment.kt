package com.eyeo.privacy.ui

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.Choreographer
import androidx.preference.*
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.eyeo.privacy.PrivacyShieldAndroid
import com.eyeo.privacy.PrivacyShieldProvider
import com.eyeo.privacy.analytics.Event
import com.eyeo.privacy.models.FilterLevel
import com.eyeo.privacy.ui.PrivacyShieldCoreExtension.ui
import com.eyeo.privacy.ui.PrivacyShieldSettingsActivity.Companion.ARGS_HIGHLIGHTED_FEATURE
import com.eyeo.privacy.ui.analytics.UiEvent.PrivacySettings
import com.eyeo.privacy.ui.view.PrivacyShieldVersionPreference

class PrivacyShieldSettingsFragment : PreferenceFragmentCompat(), PrivacyShieldProvider {

    private lateinit var showAdvancedFeatures: CheckBoxPreference
    private lateinit var cookiesCategory: PreferenceCategory
    private lateinit var privacyCategory: PreferenceCategory
    private lateinit var enablePrivacyShieldSwitch: SwitchPreferenceCompat
    private lateinit var blockTrackersSwitch: SwitchPreferenceCompat
    private lateinit var thirdPartyCookiesSwitch: SwitchPreferenceCompat
    private lateinit var hideCookiesPopupSwitch: SwitchPreferenceCompat
    private lateinit var cookiesDropDown: ListPreference
    private lateinit var hideReferrerSwitch: SwitchPreferenceCompat
    private lateinit var dntSwitch: SwitchPreferenceCompat
    private lateinit var cnameSwitch: SwitchPreferenceCompat
    private lateinit var gpcSwitch: SwitchPreferenceCompat
    private lateinit var removeMarketingParamsSwitch: SwitchPreferenceCompat
    private lateinit var blockHyperlinkAuditingSwitch: SwitchPreferenceCompat
    private lateinit var fingerprintShieldSwitch: SwitchPreferenceCompat
    private lateinit var blockSocialMediaIconsSwitch: SwitchPreferenceCompat
    private lateinit var deAMPSwitch: SwitchPreferenceCompat

    private lateinit var versionPref: PrivacyShieldVersionPreference
    private var versionPrefClickCount = 0

    private var highlightPref: Preference? = null

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        if (!PrivacyShieldAndroid.isInitialized()) return

        setPreferencesFromResource(R.xml.privacy_shield_preferences, rootKey)

        setupPrivacyFeatures()
        setupHiddenFeatures()
        setupHiddenDialog()

        savedInstanceState?.let {
            val visible = it.getBoolean(ARGS_ADVANCED_FEATURES_VISIBLE, false)
            setAdvancedFeaturesVisibility(visible)
        }
        if (savedInstanceState == null) {
            requireAnalytics.notifyEvent(Event.PrivacySettings.OPEN_SCREEN)
        }
    }

    private fun setupPrivacyFeatures() {
        cookiesCategory = findPreference(getString(R.string.ps_key_cookies_section))!!
        privacyCategory = findPreference(getString(R.string.ps_key_privacy_section))!!

        enablePrivacyShieldSwitch = findPreference(getString(R.string.ps_key_privacy_shield))!!
        enablePrivacyShieldSwitch.setOnPreferenceChangeListener { _, newValue ->
            val enabled = newValue == true
            return@setOnPreferenceChangeListener requirePrivacyShield.ui()
                .enablePrivacyShieldWithTnCDialog(
                    childFragmentManager,
                    enabled
                )
        }

        blockTrackersSwitch = findPreference(getString(R.string.ps_key_block_trackers))!!
        blockTrackersSwitch.setOnPreferenceChangeListener { _, newValue ->
            val blockTrackers = newValue as Boolean
            requirePrivacy.editSettings().blockTrackers(blockTrackers).apply()
            requireAnalytics.notifyEvent(
                Event.PrivacySettings.TOGGLE_BLOCK_TRACKERS, blockTrackers
            )
            true
        }

        thirdPartyCookiesSwitch =
            findPreference(getString(R.string.ps_key_block_third_party_cookies))!!
        thirdPartyCookiesSwitch.setOnPreferenceChangeListener { _, newValue ->
            val blockThirdPartyCookies = newValue as Boolean
            requirePrivacy.editSettings().blockThirdPartyCookies(blockThirdPartyCookies).apply()
            requireAnalytics.notifyEvent(
                Event.PrivacySettings.TOGGLE_BLOCK_THIRD_PARTY_COOKIES, blockThirdPartyCookies
            )
            true
        }
        cookiesDropDown =
            findPreference(getString(R.string.ps_key_block_third_party_cookies_dropdown))!!
        cookiesDropDown.entryValues =
            resources.getStringArray(R.array.ps_block_third_party_cookies_array)
        cookiesDropDown.entries =
            resources.getStringArray(R.array.ps_block_third_party_cookies_array)
        cookiesDropDown.setOnPreferenceChangeListener { _, newValue ->
            val index = cookiesDropDown.findIndexOfValue(newValue as String)
            val cookiesFilterLevel = FilterLevel.values()[index]
            requirePrivacy.editSettings().cookiesFilterLevel(cookiesFilterLevel).apply()
            requireAnalytics.notifyEvent(
                Event.PrivacySettings.SELECT_COOKIES_FILTER_LEVEL, cookiesFilterLevel
            )
            true
        }
        hideCookiesPopupSwitch = findPreference(getString(R.string.ps_key_hide_cookie_popups))!!
        hideCookiesPopupSwitch.setOnPreferenceChangeListener { _, newValue ->
            val enable = newValue as Boolean
            if (enable) {
                MaterialAlertDialogBuilder(requireContext()).setMessage(R.string.ps_hide_cookie_popups_dialog_message)
                    .setPositiveButton(R.string.ps_hide_cookie_popups_dialog_validate) { _, _ ->
                        requirePrivacy.editSettings().hideCookieConsentPopups(true).apply()
                        requireAnalytics.notifyEvent(
                            Event.PrivacySettings.TOGGLE_HIDE_COOKIE_CONSENT_POPUPS, true
                        )
                    }.setNegativeButton(android.R.string.cancel) { _, _ -> }.show()
                false
            } else {
                requirePrivacy.editSettings().hideCookieConsentPopups(false).apply()
                requireAnalytics.notifyEvent(
                    Event.PrivacySettings.TOGGLE_HIDE_COOKIE_CONSENT_POPUPS, false
                )
                true
            }
        }
        hideReferrerSwitch = findPreference(getString(R.string.ps_key_hide_referrer_header))!!
        hideReferrerSwitch.setOnPreferenceChangeListener { _, newValue ->
            val hideReferrerHeader = newValue as Boolean
            requirePrivacy.editSettings().hideReferrerHeader(hideReferrerHeader).apply()
            requireAnalytics.notifyEvent(
                Event.PrivacySettings.TOGGLE_HIDE_REFERRER_HEADER, hideReferrerHeader
            )

            true
        }
        cnameSwitch = findPreference(getString(R.string.ps_key_cname_cloaking_protection))!!
        cnameSwitch.setOnPreferenceChangeListener { _, newValue ->
            val cnameProtection = newValue as Boolean
            requirePrivacy.editSettings().enableCNameCloakingProtection(cnameProtection).apply()
            requireAnalytics.notifyEvent(
                Event.PrivacySettings.TOGGLE_CNAME_CLOAKING_PROTECTION, cnameProtection
            )
            true
        }
        dntSwitch = findPreference(getString(R.string.ps_key_send_no_track_signal))!!
        dntSwitch.setOnPreferenceChangeListener { _, newValue ->
            val enableDoNotTrack = newValue as Boolean
            requirePrivacy.editSettings().enableDoNotTrack(enableDoNotTrack).apply()
            requireAnalytics.notifyEvent(
                Event.PrivacySettings.TOGGLE_DO_NOT_TRACK,
                enableDoNotTrack
            )
            true
        }
        gpcSwitch = findPreference(getString(R.string.ps_key_gpc))!!
        gpcSwitch.setOnPreferenceChangeListener { _, newValue ->
            val enableGPC = newValue as Boolean
            requirePrivacy.editSettings().enableGPC(enableGPC).apply()
            requireAnalytics.notifyEvent(Event.PrivacySettings.TOGGLE_ENABLE_GPC, enableGPC)
            true
        }
        removeMarketingParamsSwitch =
            findPreference(getString(R.string.ps_key_remove_marketing_tracking_parameters))!!
        removeMarketingParamsSwitch.setOnPreferenceChangeListener { _, newValue ->
            val removeMarketingTrackingParameters = newValue as Boolean
            requirePrivacy.editSettings().removeMarketingTrackingParameters(
                removeMarketingTrackingParameters
            ).apply()
            requireAnalytics.notifyEvent(
                Event.PrivacySettings.TOGGLE_REMOVE_MARKETING_TRACKING_PARAMETERS,
                removeMarketingTrackingParameters
            )
            true
        }
        blockHyperlinkAuditingSwitch =
            findPreference(getString(R.string.ps_key_block_hyperlink_auditing))!!
        blockHyperlinkAuditingSwitch.setOnPreferenceChangeListener { _, newValue ->
            val blockHyperlinkAuditing = newValue as Boolean
            requirePrivacy.editSettings().blockHyperlinkAuditing(
                blockHyperlinkAuditing
            ).apply()
            requireAnalytics.notifyEvent(
                Event.PrivacySettings.TOGGLE_BLOCK_HYPERLINK_AUDITING, blockHyperlinkAuditing
            )
            true
        }
        fingerprintShieldSwitch =
            findPreference(getString(R.string.ps_key_fingerprint_shield))!!
        fingerprintShieldSwitch.setOnPreferenceChangeListener { _, newValue ->
            val fingerprintShield = newValue as Boolean
            requirePrivacy.editSettings().enableFingerprintShield(fingerprintShield).apply()
            requireAnalytics.notifyEvent(
                Event.PrivacySettings.TOGGLE_ANTI_FINGERPRINTING, fingerprintShield
            )
            true
        }

        blockSocialMediaIconsSwitch =
            findPreference(getString(R.string.ps_key_social_media_blocking))!!
        blockSocialMediaIconsSwitch.setOnPreferenceChangeListener { _, newValue ->
            val blockSocialMediaIcons = newValue as Boolean
            requirePrivacy.editSettings().blockSocialMediaIconsTracking(blockSocialMediaIcons)
                .apply()
            requireAnalytics.notifyEvent(
                Event.PrivacySettings.TOGGLE_BLOCK_SOCIAL_MEDIA_ICONS,
                blockSocialMediaIcons
            )
            true
        }

        deAMPSwitch = findPreference(getString(R.string.ps_key_de_amp))!!
        deAMPSwitch.setOnPreferenceChangeListener { _, newValue ->
            val deAMP = newValue as Boolean
            requirePrivacy.editSettings().enableDeAMP(deAMP).apply()
            requireAnalytics.notifyEvent(
                Event.PrivacySettings.TOGGLE_DE_AMP,
                deAMP
            )
            true
        }
        showAdvancedFeatures =
            findPreference(getString(R.string.ps_key_show_advanced_features))!!
        showAdvancedFeatures.setOnPreferenceChangeListener { _, newValue ->
            setAdvancedFeaturesVisibility(newValue == true)
            true
        }
    }

    private fun setAdvancedFeaturesVisibility(visible: Boolean) {
        cookiesCategory.isVisible = visible
        privacyCategory.isVisible = visible
    }

    private fun setupHiddenFeatures() {
        arguments?.getStringArray(PrivacyShieldSettingsActivity.ARGS_HIDDEN_FEATURES)
            ?.mapNotNull { findPreference(it) }
            ?.forEach { pref -> pref.isVisible = false }
    }

    private fun setupHiddenDialog() {
        versionPref = findPreference(getString(R.string.ps_key_version))!!
        versionPref.setOnPreferenceClickListener {
            ++versionPrefClickCount
            if (versionPrefClickCount == SHOW_HIDDEN_STATS_TAP_COUNT) {
                versionPrefClickCount = 0
                val globalStats = requireStats.getGlobal()
                MaterialAlertDialogBuilder(requireContext())
                    .setTitle("Privacy stats")
                    .setMessage("Trackers blocked: ${globalStats.trackersBlocked}\n" +
                                "Cookies blocked: ${globalStats.cookiesBlocked}\n" +
                                "Popups blocked: ${globalStats.popupsBlocked}\n" +
                                "CMP popups shown: ${globalStats.popupsShown}"
                    )
                    .show()
            }
            true
        }
    }

    override fun onResume() {
        super.onResume()
        privacy?.listenSettings {
            enablePrivacyShieldSwitch.isChecked = it.enabled
            cookiesCategory.isEnabled = it.enabled
            privacyCategory.isEnabled = it.enabled
            blockTrackersSwitch.isChecked = it.blockTrackers
            thirdPartyCookiesSwitch.isChecked = it.blockThirdPartyCookies
            hideCookiesPopupSwitch.isChecked = it.hideCookieConsentPopups
            cookiesDropDown.isEnabled = it.blockThirdPartyCookies
            cookiesDropDown.value =
                cookiesDropDown.entries[it.cookiesFilterLevel.ordinal].toString()
            cookiesDropDown.summary = cookiesDropDown.value
            hideReferrerSwitch.isChecked = it.hideReferrerHeader
            dntSwitch.isChecked = it.enableDoNotTrack
            cnameSwitch.isChecked = it.enableCNameCloakingProtection
            gpcSwitch.isChecked = it.enableGPC
            removeMarketingParamsSwitch.isChecked = it.removeMarketingTrackingParameters
            blockHyperlinkAuditingSwitch.isChecked = it.blockHyperlinkAuditing
            fingerprintShieldSwitch.isChecked = it.enableFingerprintShield
            blockSocialMediaIconsSwitch.isChecked = it.blockSocialMediaIconsTracking
            deAMPSwitch.isChecked = it.deAMP
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putBoolean(ARGS_ADVANCED_FEATURES_VISIBLE, showAdvancedFeatures.isChecked)
        super.onSaveInstanceState(outState)
    }

    @SuppressLint("RestrictedApi")
    override fun onCreateAdapter(preferenceScreen: PreferenceScreen): RecyclerView.Adapter<*> {
        setupHighlight()
        return PrivacyShieldPreferenceGroupAdapter(preferenceScreen, highlightPref?.order)
    }

    private fun setupHighlight() {
        val highlightKey = arguments?.getString(ARGS_HIGHLIGHTED_FEATURE)
        highlightPref = findPreference(highlightKey.toString())

        highlightPref?.let { pref ->
            pref.parent?.let { parent ->
                val isAdvancedFeature =
                    parent.key == getString(R.string.ps_key_privacy_section) || parent.key == getString(
                        R.string.ps_key_cookies_section
                    )
                if (isAdvancedFeature) setAdvancedSettings(true)
            }
            Choreographer.getInstance().postFrameCallback {
                scrollToPreference(pref)
            }
        }
    }

    @Suppress("SameParameterValue")
    private fun setAdvancedSettings(enabled: Boolean) {
        showAdvancedFeatures.isChecked = enabled
        showAdvancedFeatures.onPreferenceChangeListener?.onPreferenceChange(
            showAdvancedFeatures, enabled
        )
    }

    companion object {
        private const val ARGS_ADVANCED_FEATURES_VISIBLE = "advanced_features_visible"
        private const val SHOW_HIDDEN_STATS_TAP_COUNT = 10
    }
}