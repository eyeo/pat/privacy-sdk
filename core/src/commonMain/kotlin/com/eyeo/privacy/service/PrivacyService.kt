package com.eyeo.privacy.service

import com.eyeo.pat.browser.plugins.JavascriptPlugin
import com.eyeo.pat.models.RequestType
import com.eyeo.pat.provider.ResourcesProvider
import com.eyeo.pat.provider.SettingsProvider
import com.eyeo.pat.utils.JsUtils.isolateJavascript
import com.eyeo.privacy.models.FilterLevel
import com.eyeo.privacy.models.PrivacySettings
import com.eyeo.privacy.models.PrivacyShieldHeaders
import io.ktor.http.*

/**
 * This service deals with privacy features
 */
@Suppress("unused")
internal class PrivacyService(
    private val privacySettings: SettingsProvider<PrivacySettings>,
    private val resourcesProvider: ResourcesProvider,
    private val domainService: DomainService,
    private val supportChips: Boolean,
    private val supportStoragePartitioning: Boolean
) {

    init {
        resetAllowedDomainsThisSession()
    }

    fun isProtectionEnabled(url: Url?): Boolean {
        return privacySettings.isEnabled() &&
                url?.let {
                    val settings = privacySettings.get()
                    val host = url.host
                    !settings.allowedDomains.contains(host) &&
                            !settings.allowedDomainsThisSession.contains(host)
                } ?: true
    }

    fun getSettings(): PrivacySettings = privacySettings.get()

    fun listenSettings() = privacySettings.listen()

    fun removeTrackingParameters(reqUrlBuilder: URLBuilder): URLBuilder {
        if (getSettings().removeMarketingTrackingParameters) {
            val keys = reqUrlBuilder.parameters.entries().map { it.key }
            keys.forEach { key ->
                if (TRACKING_URL_PARAMS.contains(key) ||
                    CONDITIONAL_TRACKING_URL_PARAMS[key]?.invoke(reqUrlBuilder) == true
                ) {
                    reqUrlBuilder.parameters.remove(key)
                }
            }
        }
        return reqUrlBuilder
    }

    /**
     * In case of AMP(Accelerated Mobile Pages) URL detection, extract the canonical URL
     * @param urlBuilder holder of the input URL
     * @return the canonical URL(corresponding to the AMP URL) or the input URL in case AMP is not detected,
     * inside of an URLBuilder
     */
    fun ampToCanonical(urlBuilder: URLBuilder): URLBuilder {
        if (getSettings().deAMP) {
            val isAMPCandidate =
                urlBuilder.host.contains(".cdn.ampproject.org") ||
                        urlBuilder.host.contains("google.com") &&
                        urlBuilder.encodedPathSegments.size > 1 &&
                        urlBuilder.encodedPathSegments[1] == "amp"
            if (!isAMPCandidate) return urlBuilder

            urlBuilder.apply {
                val indicators = setOf("amp", "google")
                encodedPathSegments = encodedPathSegments.filterIndexed { index, s ->
                    s.isNotEmpty() && index > 2 && !indicators.contains(s)
                }.toMutableList().takeIf { it.isNotEmpty() }?.apply {
                    this[0] = this[0].removePrefix("amp.")
                }.orEmpty()
                return URLBuilder(
                    "https://${encodedPathSegments.joinToString("/")}".replace(
                        ".amp",
                        ""
                    )
                )
            }
        }
        return urlBuilder
    }

    fun shouldBlockHyperlinkAuditing(
        docUrl: Url?,
        requestType: RequestType,
        headers: List<Pair<String, String>>
    ): Boolean {
        if (!isProtectionEnabled(docUrl) || !getSettings().blockHyperlinkAuditing || requestType.isFrameRequest()) {
            return false
        }

        if (headers.find {
                PrivacyShieldHeaders.ContentType.equals(
                    it.first,
                    ignoreCase = true
                ) && "text/ping".equals(it.second, ignoreCase = true)
            } != null) {
            return true
        }
        return false
    }

    fun getSettingsProvider(): SettingsProvider<PrivacySettings> {
        return privacySettings
    }

    private fun resetAllowedDomainsThisSession() {
        privacySettings.edit { settings -> settings.copy(allowedDomainsThisSession = setOf()) }
    }

    internal suspend fun createJsBuilder(fingerprintShieldScript: String?): JavascriptPlugin {
        return PrivacyJSBuilder(
            privacySnippet = resourcesProvider.loadResource(PRIVACY_SNIPPET_FILE)
                .isolateJavascript(),
            definePropertySnippet = resourcesProvider.loadResource(DEFINE_PROPERTY_SNIPPET_FILE)
                .isolateJavascript(),
            sandboxSnippet = resourcesProvider.loadResource(SANDBOX_SNIPPET_FILE)
                .isolateJavascript(),
            antiFingerprintingSnippet = fingerprintShieldScript ?: resourcesProvider.loadResource(
                FINGERPRINT_SHIELD_SNIPPET_FILE
            ),
        )
    }

    fun isThirdPartyRequest(documentUrl: Url, requestUrl: Url) =
        domainService.isThirdPartyRequest(documentUrl, requestUrl, false)

    fun getMainDomain(documentUrl: Url) = domainService.getMainDomain(documentUrl)

    inner class PrivacyJSBuilder(
        private val privacySnippet: String,
        private val definePropertySnippet: String,
        private val sandboxSnippet: String,
        private val antiFingerprintingSnippet: String,
    ) : JavascriptPlugin {

        override fun createJavaScriptSnippet(
            tabId: String,
            documentUrl: Url,
            requestUrl: Url,
            requestType: RequestType
        ): String {
            if (isProtectionEnabled(documentUrl)) {
                val settings = getSettings()

                val snippetBuilder = StringBuilder()
                if (settings.enableGPC) {
                    snippetBuilder.append(createPropertySnippet("globalPrivacyControl", "true"))
                }
                if (settings.enableDoNotTrack) {
                    snippetBuilder.append(createPropertySnippet("doNotTrack", "'1'"))
                }

                if (settings.blockThirdPartyCookies &&
                    requestType == RequestType.I_FRAME &&
                    isThirdPartyRequest(documentUrl, requestUrl)
                ) {
                    snippetBuilder.append(
                        sandboxSnippet.replace(
                            "%BLOCK_COOKIES%",
                            (settings.cookiesFilterLevel == FilterLevel.ALL).toString()
                        ).replace(
                            "%SANDBOX_PREFIX%",
                            domainService.getMainDomain(documentUrl)
                        ).replace(
                            "%SUPPORT_CHIPS%",
                            supportChips.toString()
                        ).replace(
                            "%SUPPORT_STORAGE_PARTITIONING%",
                            supportStoragePartitioning.toString()
                        )
                    )
                }
                snippetBuilder.append(privacySnippet)

                if (settings.enableFingerprintShield) {
                    snippetBuilder.append(antiFingerprintingSnippet)
                }
                return snippetBuilder.toString()
            }
            return ""
        }

        override fun createJsInterface(): Pair<String, Any>? = null

        private fun createPropertySnippet(key: String, value: String): String {
            return definePropertySnippet
                .replace("%PROPERTY_KEY%", key)
                .replace("%PROPERTY_VALUE%", value)
        }
    }

    companion object {
        private const val PRIVACY_SNIPPET_FILE = "data/privacy_snippet.js"
        private const val DEFINE_PROPERTY_SNIPPET_FILE = "data/define_property_snippet.js"
        private const val SANDBOX_SNIPPET_FILE = "data/sandbox_snippet.js"
        private const val FINGERPRINT_SHIELD_SNIPPET_FILE = "data/fingerprint_shield.js"

        private val CONDITIONAL_TRACKING_URL_PARAMS = mapOf<String, (URLBuilder) -> Boolean>(
            "mkt_tok" to { url -> !url.buildString().lowercase().contains("unsubscribe") },
        )
        internal val TRACKING_URL_PARAMS = setOf(
            "__hsfp",
            "__hssc",
            "__hstc",
            "__s",
            "_hsenc",
            "_openstat",
            "dclid",
            "fb_action_ids",
            "fb_action_types",
            "fb_ref",
            "fb_source",
            "fbclid",
            "gbraid",
            "gclid",
            "hsCtaTracking",
            "igshid",
            "mc_eid",
            "ml_subscriber",
            "ml_subscriber_hash",
            "msclkid",
            "oft_c",
            "oft_ck",
            "oft_d",
            "oft_id",
            "oft_ids",
            "oft_k",
            "oft_lk",
            "oft_sk",
            "oly_anon_id",
            "oly_enc_id",
            "rb_clickid",
            "s_cid",
            "twclid",
            "utm_campaign",
            "utm_channel",
            "utm_cid",
            "utm_content",
            "utm_medium",
            "utm_name",
            "utm_place",
            "utm_pubreferrer",
            "utm_reader",
            "utm_referrer",
            "utm_social",
            "utm_social-type",
            "utm_source",
            "utm_swu",
            "utm_term",
            "utm_userid",
            "utm_viz_id",
            "vero_conv",
            "vero_id",
            "wbraid",
            "wickedid",
            "yclid",
        )
    }
}