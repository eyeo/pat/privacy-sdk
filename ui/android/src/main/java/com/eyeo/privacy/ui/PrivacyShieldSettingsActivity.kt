package com.eyeo.privacy.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.add
import androidx.fragment.app.commit
import com.eyeo.pat.PatLogger
import com.eyeo.pat.utils.CompatExtension.parcelable
import com.eyeo.privacy.PrivacyShieldProvider
import com.eyeo.privacy.PrivacyShieldAndroid
import com.eyeo.privacy.analytics.Event
import com.eyeo.privacy.ui.analytics.UiEvent.Settings
import com.eyeo.privacy.ui.databinding.PrivacyShieldActivityBinding
import com.eyeo.privacy.ui.utils.setToolbarTitle

class PrivacyShieldSettingsActivity : AppCompatActivity(), PrivacyShieldProvider {

    private lateinit var toolbar: ActionBar

    override fun onCreate(savedInstanceState: Bundle?) {
        if (intent.hasExtra(ARGS_THEME)) {
            setTheme(intent.getIntExtra(ARGS_THEME, 0))
        }
        super.onCreate(savedInstanceState)

        if (!PrivacyShieldAndroid.isInitialized()) {
            finish()
            PatLogger.w(
                TAG,
                "Privacy Shield is not initialized. PrivacyShieldSettingsActivity can't be open"
            )
            return
        }

        val binding = PrivacyShieldActivityBinding.inflate(layoutInflater)

        toolbar = supportActionBar!!
        toolbar.setDisplayHomeAsUpEnabled(parentActivityIntent != null)

        if (savedInstanceState == null) {
            privacyShield?.analytics()?.notifyEvent(Event.Settings.OPEN_SCREEN)

            val args = Bundle()
            intent.getStringExtra(ARGS_HIGHLIGHTED_FEATURE)?.let {
                args.putString(ARGS_HIGHLIGHTED_FEATURE, it)
            }

            intent.getStringArrayExtra(ARGS_HIDDEN_FEATURES)?.let {
                args.putStringArray(ARGS_HIDDEN_FEATURES, it)
            }

            setToolbarTitle(getString(R.string.ps_privacy_title))


            supportFragmentManager.commit {
                setReorderingAllowed(true)
                add<PrivacyShieldSettingsFragment>(binding.psFragmentContainerView.id, args = args)
            }
        }
        setContentView(binding.root)
    }

    override fun getParentActivityIntent(): Intent? {
        if (intent.hasExtra(ARGS_PARENT)) {
            return intent.parcelable(ARGS_PARENT)
        }
        return super.getParentActivityIntent()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
            return true
        }
        return false
    }

    companion object {

        private const val TAG = "PrivacyShieldSettingsActivity"
        internal const val ARGS_HIGHLIGHTED_FEATURE = "highlighted_feature"
        internal const val ARGS_HIDDEN_FEATURES = "hidden_features"
        private const val ARGS_PARENT = "parent_intent"
        private const val ARGS_THEME = "theme"

        @JvmStatic
        @JvmOverloads
        fun createIntent(
            context: Context,
            parentIntent: Intent? = null,
            highlightedFeature: String? = null,
            hiddenFeatures: Array<String>? = null,
            theme: Int? = null
        ) =
            Intent(context, PrivacyShieldSettingsActivity::class.java).apply {
                parentIntent?.let {
                    putExtra(ARGS_PARENT, parentIntent)
                }
                highlightedFeature?.let {
                    putExtra(ARGS_HIGHLIGHTED_FEATURE, highlightedFeature)
                }
                hiddenFeatures?.let {
                    putExtra(ARGS_HIDDEN_FEATURES, hiddenFeatures)
                }
                theme?.let {
                    putExtra(ARGS_THEME, it)
                }
            }
    }
}