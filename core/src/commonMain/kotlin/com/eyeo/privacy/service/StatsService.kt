package com.eyeo.privacy.service

import com.eyeo.pat.provider.StorageProvider
import com.eyeo.pat.provider.StoredValue
import com.eyeo.privacy.models.PrivacyShieldStats
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import com.eyeo.privacy.models.PrivacyShieldStatsData

internal class StatsService(storageProvider: StorageProvider) {

    private val stats = StoredValue(
        key = STATS_KEY,
        storageProvider = storageProvider,
        serializer = PrivacyShieldStatsData.serializer(),
        defaultBuilder = { PrivacyShieldStatsData() }
    )
    private var tabsData = hashMapOf<String, PrivacyShieldStatsData>()

    fun listenStats(tabId: String): Flow<PrivacyShieldStats> {
        return stats.listen().map {
            PrivacyShieldStats(getTabData(tabId), it)
        }
    }

    fun getCurrentStats(tabId: String): PrivacyShieldStats {
        return PrivacyShieldStats(getTabData(tabId), stats.value)
    }

    fun getGlobalStats(): PrivacyShieldStatsData {
        return stats.value
    }

    fun onPageChange(tabId: String) {
        setTabData(tabId, PrivacyShieldStatsData())
        stats.notifyChange()
    }

    fun onTrackerBlocked(tabId: String) {
        stats.store(stats.value.let {
            val tabData = getTabData(tabId)
            setTabData(tabId, tabData.copy(trackersBlocked = tabData.trackersBlocked + 1))
            it.copy(trackersBlocked = it.trackersBlocked + 1)
        })
    }

    fun onCookiesBlocked(tabId: String) {
        stats.store(stats.value.let {
            val tabData = getTabData(tabId)
            setTabData(tabId, tabData.copy(cookiesBlocked = tabData.cookiesBlocked + 1))
            it.copy(cookiesBlocked = it.cookiesBlocked + 1)
        })
    }

    fun onPopupsBlocked(tabId: String) {
        stats.store(stats.value.let {
            val tabData = getTabData(tabId)
            setTabData(tabId, tabData.copy(popupsBlocked = tabData.popupsBlocked + 1))
            it.copy(popupsBlocked = it.popupsBlocked + 1)
        })
    }

    fun onPopupShown() {
        stats.store(stats.value.let {
            it.copy(popupsShown = it.popupsShown + 1)
        })
    }

    private fun getTabData(tabId: String): PrivacyShieldStatsData {
        return tabsData.getOrPut(tabId) { PrivacyShieldStatsData() }
    }

    private fun setTabData(tabId: String, data: PrivacyShieldStatsData) {
        tabsData[tabId] = data
    }

    companion object {
        private const val STATS_KEY = "ps_global_stats"
    }

}