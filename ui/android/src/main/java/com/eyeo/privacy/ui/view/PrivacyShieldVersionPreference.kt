package com.eyeo.privacy.ui.view

import android.content.Context
import android.util.AttributeSet
import android.widget.TextView
import androidx.preference.Preference
import androidx.preference.PreferenceViewHolder
import com.eyeo.privacy.PrivacyShieldAndroid
import com.eyeo.privacy.ui.R

class PrivacyShieldVersionPreference @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
    defStyleRes: Int = 0
) : Preference(context, attrs, defStyleAttr, defStyleRes) {

    init {
        layoutResource = R.layout.privacy_shield_preference_version
    }

    override fun onBindViewHolder(holder: PreferenceViewHolder) {
        super.onBindViewHolder(holder)
        val text =
            "${context.resources.getString(R.string.ps_powered_by)} v${PrivacyShieldAndroid.get().version}"
        (holder.findViewById(R.id.ps_preference_version_tv) as TextView).text = text
    }
}